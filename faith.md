---
layout: default
title:  Faith
---

<h2 class="faith">Faith archives</h2>
<table>
  <tbody>
  {% for post in site.categories.faith %}
  <tr>
    <td class="archive-post faith"><a href="{{ post.url }}">{{ post.title }}</a></td>
    <td class="archive-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%B %-d, %Y" }}</time></td>
  </tr>
  {% endfor %}
  </tbody>
</table>

