---
title: "church in africa"
date:   2009-10-13 12:00:00 +0300
layout: post
---

It's different. I'll just go ahead and put it out there. That's the gist of it.

The service length, as you might suspect, is a bit longer than the short-and-sweet, just-under-an-hour American variety. I don't object to this personally, but I'm not going to lie and tell you I haven't looked at my watch. I doubt that many people would want to spend 2+ hours listening to a sermon they  can't understand, and then stand awkwardly with the congregation and observe while everyone else sings songs. That's pretty much what church has been like for me since being here. I try my best to listen for words I know, and then when I get tired of doing that, I have my devotional book and Bible with me so I can at least stay focused on God.

I do get excited when I understand little phrases, though. During one song, I caught part of a verse that says "...na Wewe, Bwana" ("...with You, God"). It was stuck in my head the rest of the day. And during a sermon, I heard the pastor referring to "Yoshua."

Small sidenote::

> When Tanzanians pronounce the "J" sound, it sounds like a "Y" to us, even
> though we've been told over and over by our LCFs that it's a hard "J" after
> we've pronounced it as a soft "J." I have resolved to simply hammer every
> "J" in my Kiswahili, even though it sounds like I'm butchering the language.
> I've been told by my LCF many times that I sound like a native speaker,
> which I think is why Tanzanians love to talk so fast to me. They think I
> know a lot more of the language than I actually do, so I have to tell them
> to "sema polepole, tafadhali!" ("speak slowly, please!")

::End of sidenote

I am currently reading the book of Joshua, for a few reasons. Reason 1, God showed me Joshua 21:45 the second day of my homestay, which I texted to my dad. A couple days later, my dad informed me that pastor Mike at PC3 had mentioned Joshua's crossing the Jordan in his sermon (reason 2). Then I found out the Levites were in Joshua, of which I remember a friend telling me about and comparing me to before I left (reason 3). On Sunday and Monday night, "Yoshua" was the scripture of our family devotional (reason 4). I have deduced that this is God's voice calling me to check into Joshua.

Ex Officio Update: I promised some of you back home to keep you informed of my underwear situation, which you are interested in because I wrote about it in one of the first posts on this blog. If you need a refresher, all you need to know is I only brought 3 pairs of underwear with me for two years. Ex Officio status is good! No problems whatsoever with any pairs so far, they have all washed clean and dried quickly just as advertised. Generally speaking, I've been washing underwear every week, so I 'll wear two pairs for about 6 days and then switch to the third on Sunday, freeing the other two to be washed. I actually think I could've gotten away with two pairs at this point, although I've only been here long enough for two or three wash cycles. In case you're wondering, my 2 undershirts are also doing well, although the collars have changed colors a bit, from white to slightly brownish. It doesn't really matter, since you aren't supposed to see the undershirt, just the effects of it.

The next update should include something about Nyerere Day, which is on October 14th (tomorrow at the time of this writing). Hope you all are enjoying the fall back in the US, because I'm definitely jonesing for it over here in the heat...while drinking my hot chai...
