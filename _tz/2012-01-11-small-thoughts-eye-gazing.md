---
layout: post
title: "small thoughts: eye-gazing"
date:   2012-01-11 12:00:00 +0300
---

> The eyes are the window to the soul.

No one really knows where this proverb originated. In any case, I believe there is some inherent truth to this, that simply gazing into someone's eyes can tell you a lot about them. Given how much time I spend staring *through* people, or with my head pointed at the ground in front of me, I've realized I should be intentional about changing this. Whether it's someone just walking by, or I'm deep in a one-on-one conversation, I'd like to spend a good deal of 2012 looking into people's eyes.

That's not creepy. Is it?
