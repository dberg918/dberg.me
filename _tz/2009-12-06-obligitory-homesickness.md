---
layout: post
title: "obligitory homesickness"
date:   2009-12-06 19:00:00 +0300
---

If there's one thing I've learned over the past week, it's that Peace Corps knows best.

My first week at site has been full of struggle and frustration. Which seems ironic, looking back at site announcements. I remember when I received my placement, I felt like I had drawn a lucky straw. Something I was actually praying against. But I learned that my site had electricity 24/7, the school was fairly recently established and growing, with a computer lab and a comprehensive science laboratory. It seemed like a sweet deal to me. But my perspective has shifted massively in the past week. There really is no lucky straw. Every site has its challenges, and I've confronted many already. Herein lies the truth of why Peace Corps knows best.

After being sick just the second day since arriving, I was harbouring resentment, towards what in particular I wasn't sure. At first I assumed it was the wildlife dwelling in my roof, but then I thought it might extend towards this town I'm in. Upon thinking more about it, I discovered it was simply towards the way I had to live my life here. It hit me when I was washing some of my clothes in a bucket on my back stoop, and organically a thought came out of me: "In America I could just throw these in a washing machine and be done with it..."

Naturally, I was worried by this, since I'm going to be living this way for the next two years. It's one thing to be frustrated by something, completely another to be utterly bitter. I immediately sought a book I had received during training, one we all had deemed was a waste of paper, Culture Matters. My CBT had done a skit about the process of adjusting to a new culture, so I looked for the section we had read over just a few weeks before. Lo and behold, many of the feelings I had over the past few days were scribed in the book, either as a guide of "how you might feel during your service," or as quotes from actual volunteers that resonated deeply with how I felt. So yes, if you're feeling something during the course of your service, chances are that not only does the Peace Corps already know about it, but they've written extensively on it and given the literature to you during training, while you tossed it aside and thought blissfully to yourself about how awesome and fantastical the next two years of your life were going to play out.

It doesn't matter if the difference is large, like living without a water tap, or seemingly trivial, like washing clothes in a bucket, it is different from what you are accustomed and there are consequences. Just from these last 7 days, I think the more trivial differences have done more damage to my mental health than the nontrivial, mostly because I am expecting an adjustment to being without a tap, whereas hand-washing clothes seems easy enough.

How ironic is it that, after travelling thousands of miles to be here for two years, it's the little things (familiar even), washing clothes, eating meals, living alone, that create in me a longing to go home...
