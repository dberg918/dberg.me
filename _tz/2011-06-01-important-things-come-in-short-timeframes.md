---
layout: post
title: 'important things come in "short" timeframes'
date:   2011-06-01 12:00:00 +0300
---

<a href="http://3.bp.blogspot.com/-SRP3KfuNQyw/TeZX2Xdlv8I/AAAAAAAAASA/3RKzBGKINWc/s1600/time-flies.png" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" height="166" src="http://3.bp.blogspot.com/-SRP3KfuNQyw/TeZX2Xdlv8I/AAAAAAAAASA/3RKzBGKINWc/s200/time-flies.png" width="200" /></a>

We're in the waning days of terminal exams here at school. Hard to believe I may not be here much longer. Between my vacation later this month to Zanzibar and COS conference at the beginning of August (and assuming I land the extension down south), I have only a precious pair of months left here at site.

If my service has taught me anything, it's how fleeting and insignificant 2 years feels. Not in the sense that the time I've spent here has been insignificant; it's been the most enlightening 2 years of my life. But I don't know why I haven't felt this already, like when graduating from college or even graduating from high school. And it's contradictory to what I've heard most westerners say, which is that time stands still in Africa. This is a misstatement. It's not that time stands still here; *people* stand still here. And when people stand still, that's when you can feel the time passing. It's a humbling thing, coming to the realization that life is much shorter than you could ever understand.

Even so, I'm reminded by the title of this blog that I'm still only beginning. Beginning to find my purpose, beginning to awaken my sense of the vast planet I live on, and beginning to understand how little time I have to make a name for the Guy that made it all happen.
