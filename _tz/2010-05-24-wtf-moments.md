---
layout: post
title: "wtf moments"
date:   2010-05-24 12:00:00 +0300
---

It's been a while since I've had one of these, or at least had one and then had the good sense to write an entry for you guys.

Last night was just too weird NOT to type something up about it. As I was walking home from town in the evening yesterday, I happened to walk by the social centre (just a big hall for hosting weddings and other events), and there just *happened* to be a bunch of white people walking in at the time.

Given how rare it is for most Peace Corps Volunteers to see other white people at their sites, let alone a fairly large group of them, I was quite shocked and strangely excited. On top of that, I heard them speaking an American-ish dialect of English, so I was even more curious and excited (it was probably visible at this point). I continued walking past, intending to go on my merry way, but luckily a local stopped to greet me before I got too far. I asked him what was going on, and he told me it was some kind of church event (Roman Catholic variety). He pushed me towards the door, which I was resisting with words like "Nipo sawa" (I'm ok) and "siruhusi" (I'm probably not invited), but I did very little physically to stop my advancing. When I got to the door, I saw tons of students, many from my own school! They were very surprised to see me, but quite excited to jump outside to drag me in.

So here I was, in a huge room full of Tanzanian students and a handful of "wazungu" (white people), not knowing what the event was or why it was happening in my town. I think that was the biggest "WTF??" for me; why are all these Americans at *my site?* Of all the places to be in Tanzania, you're here?? Why not Dar, or Arusha, or Moshi? Not that my site is a bad place to be, but when there's Mount Kilimanjaro and the Serengeti and Dar es Salaam...it seems very unlikely to make a sighting of so many white people at once in an area like mine, so devoid of what most white people are looking for.

In any case, I made awkward eye contact with many of them throughout the hour or so I was there, and when it was finally time to head back to the school (we had to print out exams for this week), I felt the urge to talk to at least a couple of them. They were probably wondering why their number had grown by one since arriving, and I felt it would be rude to leave with no explanation. I met a guy and a girl, Logan and Kaitlyn (sp?), and they had just arrived in TZ the day before. Since I was in a hurry to get back, I didn't have time to ask them the flurry of questions swimming through my brain, but I managed to tell them I was a Peace Corps Volunteer who had been living in the area for the past 6 months. That was about the extent of our conversation, unfortunately.

But perhaps they'll be curious enough to search for this blog and find it. If that's the case, Logan and Kaitlyn, tell the rest of your group that I'm sorry I didn't get to chat with them. And if you're ever travelling through my neck of the woods again, you have a place to crash!
