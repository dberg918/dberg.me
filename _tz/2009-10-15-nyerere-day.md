---
title: "nyerere day"
date:   2009-10-15 12:00:00 +0300
layout: post
---

October 14th is the day that Tanzania's first president, Julius Nyerere, died back in 1999. It is now a national holiday, and people observe it by staying home from school, attending festivals of sorts, and having parades. I only did one of those, the first namely, although I watched what looked like some sort of festival or parade on TV. I've also been watching Nyerere's speeches all day today, and apparently he's hilarious. I don't understand a lot of the things he says unfortunately (except when he starts speaking English), but I look forward to the day when I can finally understand Kiswahili enough to be able to go back and hear all of his jokes.

This year happens to be the 10th year anniversary of his death, as you could've guessed given the year I gave in the previous paragraph. Since this is the only Nyerere day I've experienced, I don't know if this one has been particularly special, but I do know he is held in very high esteem here in Tanzania. While my family didn't really do anything out of the ordinary today, they all have a healthy respect for Nyerere and the things he's done for the country. I can certainly understand why, considering his legacy is still fairly modern. And a president that makes you laugh every time he gives a speech is a winner in my book.
