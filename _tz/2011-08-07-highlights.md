---
layout: post
title: "highlights"
date:   2011-08-07 12:00:00 +0300
---

July was a low key month for me. Mostly because I had to live on less than $40 after my vacation on Zanzibar (totally worth it). We started back at school around the time of my last post, and I've been busy teaching Form II and Form III Physics ever since. I also started a computer class for interested students after school. We've only met one week so far because of the electricity situation (don't ask), but I'm hoping to have them word processing and filling out spreadsheets with practical applications. My idea right now is to have them create a budget for the school's Form IV graduation ceremony, and then write a cover letter for it to the Headmaster who will "approve" it and release the funds (all hypothetical of course, but it may be cool to have the headmaster's stamp on their hard-copy budgets). After that, I'd really just like to teach them touch-typing; they pretty much know how to navigate the applications of a computer, but none of them can type without staring at the keyboard.

I've also been turned on to another extension opportunity with a group called Creative Associates. They're being funded by USAID to bring Basic Education into the 21st century. Interesting that they're focusing on primary schools, though I suppose the earlier you introduce it, the faster it will be adopted by the population at large. I'm hoping to set up a meeting with them during my time at COS conference (just a week away now!), which will give me a clearer picture of what I might be doing, as well as where I may end up. It could be Mtwara region or on the islands of Zanzibar!

Though July was pretty low key in general, it sure went out with a bang! We had a highlighter party at my house on the 30th to send off our two Health Volunteers (one has already completed COS, the other will COS mid-August). The pictures I took, however, were from a birthday party three of us crashed the day before. Totally unexpected, but we had a blast, and invited everyone back to my place afterwards to test out the blacklights I bought. We do have pictures of the "highlights," just not on my camera, which died soon after the birthday festivities.

This week is Shadow Week, and though I don't have any of my own (my house is still recovering from the highlighter party, as well as undergoing renovation for the incoming volunteer), I'm meeting a couple in town today to send them off to Itigi, and we're all meeting up in Dodoma before I go to COScon to grab some pizza together. Exciting times!

[comment]: # (Also exciting is one of my friends beginning his Peace Corps training this past Thursday! Follow <a href="http://mccurdyca.wordpress.com/">his blog</a> to keep up on his adventures in Uganda, though he may not post for the next week or two while he does his "survival training" to get prepared for homestay.)

That's all the highlights I have for now. Watch the blog for updates on extension prospects; hopefully I'll have a job description by the end of next week. And as for videos...well, don't get your hopes up, but kicks to the pants in the comments section are certainly welcomed, given my current lack of motivation.
