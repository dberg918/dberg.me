---
layout: post
title: "small thoughts"
date:   2010-05-09 12:00:00 +0300
---

While I was walking back from the market today after a long morning stroll, a rickety old motorcycle (which I will henceforth refer to as a pikipiki in my blog, because I like to say it that way) passed by and I caught a good whiff of exhaust fumes. Strangely, it smelled like a golf cart, and I was immediately longing for rolling fairways and manicured greens. I suppose it doesn't help that I've been watching Caddyshack and Happy Gilmore non-stop this past week. nnnnnnnNOONAN!

Mom and Dad, make sure you put in some tee times at St. James for me the next time I come home, whenever that may be.
