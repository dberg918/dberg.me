---
layout: post
title: "record time"
date:   2010-05-16 12:00:00 +0300
---

I just got back from a small weekend trip up to Singida town, and I think I've suffered whiplash.

The road between my site and Singida is partially dirt road, and in normal driving conditions, you can make the drive in about 2 and a half or 3 hours in a large bus, as was the case on the way up yesterday. Today, I left Singida town at about 3:30pm, and arrived at my site 4 minutes before 5. I'm convinced the driver of the Bunda Express (which certainly deserves its name) was a rally cross driver in a previous life, because I'm fairly certain we were doing 80 km/hr around the turns on this dirt road. I sat in the back, so part of this is my fault, but there weren't many other seats open. In any case, at one point during the bus ride, I went airborne and landed in the seat next to me, about 3 feet to the left. I tried to retain good humor about the whole experience, thinking eventually he would have a heart for the poor folks in the back of the bus, but I think he actually increased speed intentionally over some of the bumpier parts, maybe having the logic that he was lessening the impact on everyone.

If this was America, this guy would get sued 6 ways to Sunday and have his license revoked forever, and probably get thrown in jail. But in Africa, all I can say is we made good time...and maybe the guy's an idiot.

And thank God I survived.
