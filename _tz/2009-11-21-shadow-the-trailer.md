---
title: "shadow: the trailer"
date:   2009-11-21 12:00:00 +0300
layout: post
---

Last week, from Wednesday to Sunday, I shadowed a current volunteer in Tanzania to see what life as an actual PCV is like. I have been writing up the story of shadow for a few days now, and I'm fairly close to finishing it (it's a 6-part series, or thereabouts). But because I took lots of video last week, I want to wait to post the story until I can cut together some clips of the journey. Unfortunately, I'm running low on disk space on my netbook, and the hard drive my parents sent me hasn't arrived yet. So, it might be a little while until the story shows up on the blog. But rest assured, I will work feverishly in the first few weeks at site after swear-in to get it all done, considering I won't have anything better to do (at least that's what I hear about the first weeks at site).

In the meantime, I have uploaded pictures from shadow on Flickr. They probably tell the story better than I can describe it with words.

Sidenote: Swear-in is just 4 short days away! We head back to Dar on Tuesday, and swear-in is on Wednesday the 25th. I'm almost an official PCV!!
