---
layout: post
title: "unaweza kunichukua?"
date:   2012-11-21 12:00:00 +0300
---

Even a Peace Corps Volunteer fresh out of Pre-Service Training can understand the meaning of this question. And it's a loaded one when it comes from the mouth of a kid.

Since the beginning of November, I've been away from home supporting the TZ21 field office in town. It's hard to be away that long, so I've tried to assuage the sting of living out of a suitcase by staying at the beach house, where we PCVs have most of our holiday gatherings. Two days ago, I went out for an evening swim after work, and I met a kid who was already out in the ocean practicing his doggy paddle. As is customary among the children you meet on the beach, he wanted to show me all of his swimming techniques and the neat tricks he could do underwater. We did this exchange for a while, until I got concerned about how dark it was. This kid was alone, so I figured he had a long walk back home and I didn't want him to stay late on my account.

> "Where do you live?"
> "I live near a hotel in town."
> "It's getting dark now. Aren't your parents worried?"
> I don't have any parents."

I had a feeling I wasn't going to like where this was going.

> "What? How were you born, then?"
> "I was born like normal, but then my parents left me."

I find it strange to hear this kind of thing in another language. You get so used to translating things in your head that they just become words, losing their emotional impact. This kid's story, however, was heart-breaking. He told me, in just a few words, how his parents had abandoned him on the street one day; how he spends his nights outside a guest house near the bus stand, where someone is gracious enough to feed him each night; how he has no friends, only people he knows. It's not surprising he feels that way -- who needs friends when your mom and dad abandon you?

He asked me for many things after that conversation. Everything from money, to food, to sunglasses. How do you respond to a kid who needs everything? Then came his last question:

> *"Unaweza kunichukua?"* (Can you take me?)

Life is not fair. God taught me that on the Cross. But it's also not fair that stuff like this happens to kids. Join with me in praying for Abdallah that, despite his desire to simply live day-to-day, he might be given the things he really needs. Things like loving care-takers, friends that invest in him, and a chance at an education.

One thing I can be thankful for is that God is already looking after him.
