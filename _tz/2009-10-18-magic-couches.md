---
title: "magic couches"
date:   2009-10-18 12:00:00 +0300
layout: post
---

The couches in my host family's living room have a magical power. It almost never fails. Within about 10 minutes of sitting on these couches, I start to lose consciousness. It doesn't matter if it's nighttime, in the morning 20 minutes after I wake up, or in the afternoon while we watch poorly acted kung fu movies. I don't know what it is, maybe it's the posture they force me into because of how firm the cushions are. And seriously, these couches have some firm cushions. But it's quite strange that I can fall asleep on them considering how uncomfortably I have to sit on them. I suppose I still haven't adjusted to life here in Africa and my body's natural response to this is to shut down. Hopefully I'll start adjusting soon, because I'd really like to listen to the Kiswahili Bible readings we have every night in our living room sometime.
