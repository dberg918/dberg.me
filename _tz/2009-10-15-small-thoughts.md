---
title: "small thoughts"
date:   2009-10-15 13:00:00 +0300
layout: post
---

There are birds nesting up in the roof of our CBT classroom, and within the past two or three days, we've seen two dead baby birds on the floor. Today, one of them fell on one of my fellow trainees before it hit the ground. A bit traumatizing, really. But my question is, what's the deal with the parents of these baby birds?? Either you need to find a better place to nest, or keep your babies in check. We would prefer not to clean up your dead babies anymore.
