---
layout: post
title: "the sweeter side of life"
date:   2011-05-01 12:00:00 +0300
---

Putting a video together with the monstrous amounts of footage I have has proven to be quite difficult. It's also much easier to edit when you're closer to it in the timeline (it's been a while since all the stuff I filmed was actually filmed). So alas, no video...yet.

I've been keeping busy regardless. School is back to normal since the craziness of Easter weekend, and I'm struggling to get back into the swing of teaching. There's also the website a bunch of us are working on, the teaching manuals we made in Moro that still aren't finished (oh, the DVD, too), and I'm trying to wrap my head around extending: is it going to happen, what do I do if it falls through, is it worth it to stay a whole 3rd year? So many things, so little time :)

<a href="http://1.bp.blogspot.com/-32yrCjvXYpc/Tb1dIAbQcII/AAAAAAAAARs/jSx0zcY072Q/s1600/cake.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" height="160" src="http://1.bp.blogspot.com/-32yrCjvXYpc/Tb1dIAbQcII/AAAAAAAAARs/jSx0zcY072Q/s200/cake.jpg" width="200" /></a>

I've also taken some time to enjoy the perks of being at home. What you see on the right is my first attempt at baking (by myself) since...well, I didn't do much baking in America, so this might be my first time. It might not look all that great, but it is uhhhhhh-MAZINGly good! I baked it just this morning so I haven't really eaten any of it yet, but I scraped the pan I baked it in and you can bet I'm itching to leave this internet cafe to get home. I have to say, my culinary skills are one thing that have drastically improved since I moved to Tanzania. It was kind of expected given the circumstances, but still thrilling for me in any case.

It's also funny how excited I get about this kind of stuff now. For example, I found brown sugar in my town yesterday and nearly broke out in a cold sweat I was so giddy. I bought it without hesitation, not realizing I had no idea what to do with it. I know brown sugar is featured in a lot of awesome recipes...can you guys help me out? I don't want to use 1 kilo of brown sugar as chai sweetener for the next month and a half, I want to dazzle my taste buds!

Keep in mind that cookies might be difficult for me; my oven is approximately the size of the cake you see pictured above (it was nested inside another pot to keep it from burning on the bottom).
