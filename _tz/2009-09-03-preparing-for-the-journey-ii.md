---
title: "preparing for the journey: part ii"
date:   2009-09-03 12:00:00 -0500
layout: post
---

I just about bought a new wardrobe today.

Most of the important clothes have been purchased now, possibly including a hand-me-down rain jacket from my dad. The next big thing is getting the pack. For the past few days, I was looking for something large enough to fit everything, but after rereading the luggage guidelines this morning, I realized I overlooked the 50lb weight limit per one checked bag. After this very quaint shift in perspective, I'm quite confident in my choice of an [internal frame pack][rei]. If I can't fit everything into this, the plan is to add a shoulder-slingable duffel into the mix.

Packing isn't the only thing I have to worry about. I also need to finish up some paperwork for staging, get my immunization history, consider a 3-month supply of allergy medication, and continue to weave these bracelets I'm making for all my friends. And how many weeks are left now? Just three?

Huh boy. Maybe this would've seemed less crazy if I had had a job these past 11 months.

[rei]: https://www.rei.com/product/757777
