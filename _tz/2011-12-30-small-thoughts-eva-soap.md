---
layout: post
title: "small thoughts: eva soap"
date:   2011-12-30 12:00:00 +0300
---

Yes, I'm writing about soap. The reason being it does such remarkable things, seemingly defying laws of common sense. Also because I've been in a hotel for the past two weeks using it.

<table cellpadding="0" cellspacing="0" class="tr-caption-container" style="float: left; margin-right: 1em; text-align: left;"><tbody><tr><td style="text-align: center;"><a href="http://1.bp.blogspot.com/-DxyNQsDAO0E/Tv4FLvizlLI/AAAAAAAAAbo/4nIIYItbibU/s1600/PIMG-0068-e1.jpg" imageanchor="1" style="clear: left; margin-bottom: 1em; margin-left: auto; margin-right: auto;"><img border="0" height="150" src="http://1.bp.blogspot.com/-DxyNQsDAO0E/Tv4FLvizlLI/AAAAAAAAAbo/4nIIYItbibU/s200/PIMG-0068-e1.jpg" width="200" /></a></td></tr><tr><td class="tr-caption" style="text-align: center;"><span style="font-size: xx-small;">When does soap become "Luxury" soap?</span></td></tr></tbody></table>

A little background: when you stay in hotels in Tanzania, they usually provide you with a towel and soap (already in your room if you're lucky!), and more often than not, the soap they stock is Eva brand. It usually comes in the cute travel size you see in the picture, though I have foggy memories of bigger bars being available when I first started my service. As a Peace Corps Volunteer, this makes it a good item to take with you when you check out; it's small, it's light, and best of all, it's free! Most well-traveled PCVs have a stockpile in their house for those moments you find yourself without laundry detergent or a decent bar for bucket baths.

But why is it so remarkable? Well, for one thing, once you get it wet, it is absolutely impossible to keep it in your hand. You might think this is an ordinary quality of all soaps, but I've noticed Eva is extraordinarily slippery. On average, I probably drop it 3-4 times more than most other soaps I've used. Which is aggravating when you consider how difficult it is to get a lather out of it.

The other peculiar thing about Eva is its staying power. That is to say, once you do get it on your skin, it's quite hard to remove. Lots of cheap soaps leave residue, and Eva is definitely no exception. This goes doubly if you've decided to use it for washing clothes in the sink. My advice to PCVs is don't do this. Just *don't*. I did it a few days ago and it didn't go well. I rinsed each garment for a good 25 minutes and the soap was still coming off. I finally gave up and ended up with undershirts that had that awful gritty feeling when you use too much soap.

So there you have it; soap that's impossible to hold, impossible to lather, and impossible to wash off if you ever manage to overcome the first two. Pretty remarkable, don't you think?  
