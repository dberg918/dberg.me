---
layout: post
title: "posts from the past: reclaiming joy"
date:   2012-11-30 12:00:00 +0300
published: false
---

I've been going through my backlog in recent weeks, and thought you guys might enjoy rereading -- or maybe reading for the first time if you're a newbie -- posts from earlier in my service. One of my favorites is <a href="http://peacecorpsdmb.blogspot.com/2010/11/reclaiming-joy.html">reclaiming joy</a>, because it has great pictures and references some shadowers we had in Singida -- most of whom have just COS'd! My how time flies!
