---
layout: post
title: 'small thoughts: "grory to god"'
date:   2010-01-24 12:00:01 +0300
---

Any volunteer here in Tanzania will tell you one of the more amusing parts of living here is seeing the multitude of slogans plastered across seemingly every commercial bus and transport truck on the road. Surprisingly, many of them are in English. I'm not sure if this is because Tanzanians are looking for a reason to practice their English, but the majority of these English slogans have spelling and grammatical errors. I wouldn't be shocked if I was one of the only volunteers who dies of laughter every time I spot one of these, I can barely understand why myself. One of the slogans I saw on the way to the YMCA from the airport I have put in the post title. I had to contain my laughter somewhat since I was in the car with someone I didn't know, but I definitely let out a chuckle.

It's good to be back in Tanzania.
