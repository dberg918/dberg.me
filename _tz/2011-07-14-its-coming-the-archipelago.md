---
layout: post
title: "it's coming: the archipelago"
date:   2011-07-14 12:00:00 +0300
published: false
---

<div style="text-align: center;"><object height="303" width="480"><param name="movie" value="http://www.youtube-nocookie.com/v/iy6Gc8GpUGg?version=3&amp;hl=en_US&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube-nocookie.com/v/iy6Gc8GpUGg?version=3&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="480" height="303" allowscriptaccess="always" allowfullscreen="true"></embed></object></div>

I was going to wait a few more days before I posted this trailer, but I'm just too excited to share! I know it's been a while since I promised footage from my vacation, but editing and sifting through 8 days worth of HD video on an under-powered 10" netbook is actually quite difficult. Hopefully this will tide you over until I can put something long-form together. Share and enjoy!
