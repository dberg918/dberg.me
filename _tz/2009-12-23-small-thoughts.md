---
layout: post
title: "small thoughts"
date:   2009-12-23 15:00:00 +0300
---

I have to say, after spending nearly three solid months with a choo, I was getting used to the idea of suspending myself freely above a hole to do my business. But after two nights and two appointments at a western toilet, I have decided that the choo just isn't right. The western toilet is simply how the bathroom is supposed to be. And it isn't any kind of bitterness from my circumstances and my condition at the moment speaking on my behalf, I think it's just the culture that I've grown up in. It's what I call home. The western toilet is like home to me. It won't make the trips to the choo any harder, but I don't think I'll change my mind after this visit to Dar.

I hope all of you in America know you have the greatest bathrooms in the world.
