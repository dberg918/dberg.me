---
layout: post
title: "it's that time of year"
date:   2010-10-11 12:00:00 +0300
---

<div class="separator" style="clear: both; text-align: center;"><a href="http://3.bp.blogspot.com/_WM5vLMqkVOk/TLK4BgvPR-I/AAAAAAAAAOU/nMcxXaX80gk/s1600/PIMG-0022_e1.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" height="240" src="http://3.bp.blogspot.com/_WM5vLMqkVOk/TLK4BgvPR-I/AAAAAAAAAOU/nMcxXaX80gk/s320/PIMG-0022_e1.jpg" width="320" /></a></div>

Yep. The seasons are changing. Leaves are shifting their color, the air is crisp and cool, and trees are stripping down to their bare branches.

If you're in the northern part of the northern hemisphere, that is. If you're in my neck of the woods, the temperature is going from sauna hot to broiler hot, the sun is attempting to push the UV index beyond its current limit, and everything is just as dusty as it always was, perhaps a bit more so. And I guess technically, this is like springtime for the local vegetation.

And yet, we have this picture. How is it even remotely possible that my body knows it's that time of year? When I look around, everything is still dead! It makes me wonder if the passage of time is some kind of placebo effect that my allergies fall for; "well, it's been about 5 months, guess it's time to get started." I mean, of all places to have allergy issues...the desert? Really??

I suppose it's only fitting that in the time it took me to write this entry, I went through the rest of that toilet paper roll. Looks like I have an errand to run...
