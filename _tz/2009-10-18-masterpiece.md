---
title: "masterpiece"
date:   2009-10-18 13:00:00 +0300
layout: post
---

Deep down, I think we all have the desire to create a masterpiece.

Of course, not all of us are painters like Picasso was, but I'm not talking in the artistic sense. There is a more general definition of "masterpiece" that I learned in my winter session art history class, way back in that time when I was in college. The general definition of a masterpiece is a revelation or a theory that articulates or reveals something about the human condition or consciousness that was previously unknown (this is my own paraphrase, so please correct me if you have a more accurate definition). Essentially, you do something new, something that hasn't been done before. And it's good.

We usually talk about "masterpieces" in terms of art, but it can be applied to other things as well. Albert Einstein's Theory of General Relativity is a pretty good example. It articulated something that was previously unknown, the properties of mass and space. It blew minds. It still blows minds today. And I think everyone longs to contribute something like this to our human culture. I know I do. How many people can say they've figured something out that no one else has?

I think this is what I'm finding a bit daunting about teaching at the moment. Essentially, my charge as a teacher is to create a masterpiece every day. To clarify or bring to light something that, to the audience, was previously unclarified or, in many cases, completely unknown. Now throw a language barrier into the mix. As if teaching wasn't already challenging enough!

Yes, of course teaching is hard. You're attempting to open up worlds to young minds. And I'm convinced this is why there's such polarization when it comes to teachers. There are many teachers that love their jobs, because they see their students discover the unknown every day. And then there's teachers that...well, they're awful to say the least. These teachers not only miss opportunities to make an impact, but they can even hamper the chances of future success. While I'm fairly confident in my ability not to become one of these bitter teachers that poison their students, I'm a bit scared of being average. Average in many cases is worse than being good or bad. It's even biblical (see Revelation 3:15-16).

So amidst the flurry of prayers that casually flow through my mind each day, I lift up my hope to create a masterpiece every day for those students that will be in my class come January, whatever it is I happen to be teaching.
