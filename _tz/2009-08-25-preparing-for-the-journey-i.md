---
title: "preparing for the journey: part i"
date:   2009-08-25 12:00:00 -0500
layout: post
---

It's already been over a month since I got the invitation, and it feels like I've done nothing to get ready. Partially because it's actually true. I still don't know what kind of luggage I'm going to use, I haven't drafted a packing list yet, and I've been spending a lot of time at the beach. What you're looking at right now is the first fruit of my labor to prepare for this trip. A blog. I don't exactly have my priorities in order at the moment. Regardless of my situation right now, I know things will work out. Procrastination runs deep in my immediate family, but we also have a knack for pulling things together quickly when the pressure's on. Mostly out of necessity because of the procrastination.

I've started looking into luggage and rainwear first and foremost, as I sense these will be decisions that have a major impact on my comfort overseas. Although given the recommendation for luggage with some kind of shoulder straps because of the awful roads in Tanzania, I don't anticipate carrying 80 lbs on my back to be comfortable in any situation. I'm also researching solar chargers, hoping to find something under $100 that's powerful enough to trickle charge the little netbook I'm typing this post on.

Entering &lt;geek mode&gt;: (if you don't understand the next paragraph at all, don't feel bad)

> On that note, I've been finding ways to reduce the power consumption of my
> laptop to squeeze every bit of battery life out of it that I can. The model
> I have, the EeePC 1005HA, is advertised to get 10.5 hours of battery life
> (ridiculous), which means they turn off all functionality (wifi, bluetooth,
> webcam, etc.) and leave it idle until the battery dies. So far, I think the
> most I've gotten on a single charge of normal use is a little over 6 hours,
> and that was with the screen backlight almost all the way on, playing
> videos, browsing the web and such. The other day I found a nice little
> utility that lets me monitor the wattage of the machine on battery power,
> and it gives suggestions to help save battery life. It's called powertop,
> and it runs in a command line. Yesterday I managed to get the average power
> consumption down to about 8 to 10 watts, which should serve me pretty well.
> When I'm using it outdoors, I can turn the backlight completely off, which
> is pretty cool, and that should save me some power. I can also turn the wifi
> off when I'm not in a hot spot (which will be often in Tanzania), which will
> save even more. I think if I can get it down to about 6 or 7 watts, the
> battery life should get pretty close to 9 hours. Not too shabby for running
> KDE4 Arch Linux on a netbook!</geek mode>

In terms of getting to staging, which is in Philadelphia (20 minutes from my hometown), I've pretty much decided to drive instead of fly. I think it will be easier than scheduling a flight with SATO Travel. If I flew, I'd have to go through security and all that jazz one more time, and then figure out a way to get to the hotel, whereas driving eliminates all of that. Driving also allows opportunities to see friends before I leave, in Maryland and in Delaware. We're planning to catch a MD football game on the way up at the moment.

A little something about the blog before I finish this first entry. If you're wondering about the stripe pattern in the background, those are the [colors of the Tanzanian flag][ctz]. I've seen two or three variations of the green and blue colors of the flag, and I'm interested to see how they really look when I finally get there.

That's pretty much it. I've got 27 days until staging! Once I make a decision on luggage, a packing list is soon to follow. Thanks for reading, hopefully these blog updates will get more interesting over time.

[ctz]: https://en.wikipedia.org/wiki/Flag_of_Tanzania
