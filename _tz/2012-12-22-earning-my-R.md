---
layout: post
title: "earning my 'R'"
date:   2012-12-22 12:00:00 +0300
---

It was recently brought to my attention that I haven't made my "farewell" post yet. For those not in the loop, November was my last month at work, and December 19th was my COS date. So yeah, I'm done. Despite having internet access the whole week before then, I neglected to write anything here. Leave it to me to wait until I'm in the airport getting ready to leave, dead-tired because it's nearly 1 in the morning.

The past 3 years have changed my life. Deeply. So deeply, in fact, that I can't bring myself to write about it. Maybe after Christmas, when I'm home and I'm caught up on sleep. You'd think 2 days in a 5-star beach resort in Dubai would revitalize you, but I was there on Friday night. December 21st. Which was notoriously marked as an "End of the World!!!" date. So yeah, I went out to an "End of the World!!!" party. Was it worth the 120 AED (about $32 USD) I spent on 3 beers? Probably not. But who stays in their hotel room on a Friday night in Dubai? Nobody does that.
