---
title: "title post"
date:   2009-10-03 12:00:00 +0300
layout: post
---

Like many music albums have a title track, or a song for which an album is named, I am dubbing this entry the "title post," as the freshness has now started.

The first week in Tanzania has been full of blessings and challenges. Maybe overflowing is a better word. Some of the challenges:
 - passport troubles at the airport
 - learning to sleep under a net
 - figuring out how to properly set up said net
 - dealing with jet lag and malaria drug side-effects
 - adjusting to a very starchy diet
 - the malaria self-test (story to follow)
 - staying properly hydrated
 - trusting the Steripen
 - greeting strangers in Kiswahili
 - understanding Kiswahili being spoken to me

Some of the blessings:
 - everyone made it to Tanzania!
 - no major health problems for anyone yet
 - the desire to learn Kiswahili (many of us are picking it up very quickly!)
 - the overall friendliness of Tanzanians
 - abundant friendships
 - having two current TZ PCVs help us with questions
 - the entire PST staff

Both lists could certainly go on. Spirits have generally been good, although everyone has had their ordeal with something. For many it was having trouble sleeping. For me, it was the malaria self-test.

On Friday, we had our session "All About Malaria." Our PCMO (Peace Corps Medical Officer) gave a lecture on what it is, why we should take it seriously, and how to prevent getting infected. In the case that we do actually contract malaria, we need to be able to confirm this with a malaria self-test. It looks very much like a pregnancy test, in that it will show you one or two lines depending on whether the test is positive or negative. To take the test, a drop of blood is required, which is where this story begins. At the end of the lecture, we were given the self-test "kit" to try it out. Along with the testing stick, there is a pin, with which you prick your own finger. Many of us, including me, did not know the proper procedure for extracting a drop of blood from a finger, which resulted in a few bloody messes and some failed malaria tests. I'm not sure if it was the jet lag, the change in diet causing an empty stomach, the medication, or maybe the 3 vaccinations I had gotten eariler that morning, but my self-test in particular did not go very well. My first attempt at pricking only produced enough blood to fill the small crevases in my finger. At this point, I should've realized that my blood was looking a bit thin, but since everyone else was having trouble, I thought I'd give it another go. Upon pricking a second time in the same finger, going a bit deeper this time, I produced quite a bit more. My strategy of squeezing the finger and trying to let gravity create a drop proved a grave mistake, as the blood simply stuck to my finger via friction (van der Waal's forces maybe?). Trying to smear it into the little hole the blood is supposed to go into didn't work, and by this time I noticed my hands shaking a bit. Before I knew it, I was getting tunnel vision and I started to lose my hearing. I'm not sure how much time passed before the PCMO walked by to check on me, but when my head came out from between my legs, she let me know that I should contact the local office to get the test done by someone else in the future. Quite embarrassing really, but the cold sweat from nearly blacking out actually did a wonderful job of keeping me cool for the next few hours.

Currently, I just got back from a tour at the Peace Corps Office in Dar es Salaam, which is quite nice. We had a few learning sessions there, and were treated to fresh veggies at lunch, something that's missing in the diet at the hostel we're currently staying in. Our first week is almost over, and from what I hear from the staff and trainers, the first week is a little bit like la-la land (which is funny, because "lala" in Kiswahili means "sleep"). PST really begins with the homestay, which will be the first major challenge we all face. There's one more day to prepare us before we leave the city and meet our host families. Everybody is buzzing with excitement, and all of us are eager to begin our service in what we've heard is the gem of the Peace Corps!
