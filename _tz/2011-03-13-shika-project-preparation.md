---
layout: post
title: "shika project: preparation"
date:   2011-03-13 12:00:00 +0300
---

If you follow my Twitter account, you may have caught a tweet that went out a week or two ago about a science conference. It is sponsored by the Ministry of Education and Vocational Training here in Tanzania, and it's all about creating hands-on science activities (practicals) using locally available materials. The time has flown by since then and we're now making preparations for our first day!

While this post is titled "shika," the conference isn't necessarily about our Shika project. The outcomes of the conference will most likely end up in the Shika manual, however. Our greater hope is that these activities will spread beyond Shika into the textbooks and literature of Tanzania's education system so that they will have an impact outside of the schools Peace Corps Volunteers are placed in.

Tomorrow is mostly about getting things organized for the rest of our time together; figuring out what we need/want to accomplish, organizing that work into a schedule, and buying materials for the practicals we need to perform. While I'm certainly here to contribute ideas (especially in the field of physics), my primary assignment at the conference will be documenting and taking footage of what occurs while we're here. Given we'll be doing lots of running around in town tomorrow buying materials, it should be a fun day :)
