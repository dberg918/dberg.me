---
layout: post
title: "life post-peace corps & pink eye"
date:   2011-03-04 12:00:00 +0300
---

It's that time of year. Second-year PCVs are considering their options; to stay or not to stay.I have been looking at a wide variety of things in the recent days and weeks, from Americorps' VISTA program (props to my buddy Casey for bringing it to my attention), to grad school in South Africa, and most recently, the [JET program][jet].

Granted, I've decided I'm not all that interested in teaching another year in my village. But I'm looking at the JET program as more than just teaching. Much like the Peace Corps, JET understands the importance of cultural exchange, so much so in fact that the 'E' in 'JET' actually stands for (Cultural) "Exchange." And to be perfectly honest, I've had a personal interest in Eastern culture since I was in junior high school. Yeah, maybe I watched some Japanese cartoons when I was younger (I was, and still am, an unabashed Pokémaster), and maybe I attempted to learn some Japanese on my own at one point. I even took a Chinese Calligraphy class in college because their writing system has always fascinated me; the kanji itself are like beautiful little works of art.

As for teaching in Japan, I think learning about another education system (which will undoubtedly be better organized and equipped) would be a blast, and my role in the classroom would actually be to tag-team with an already established English teacher. Add to that the fact I won't be living below the poverty line anymore (even if I end up in a rural village, which I would ironically love and be familiar with), and there's no way around it. I'm actively excited about this opportunity.

As for the other headline, I woke up with conjunctivitis this morning. I know. Gross. My allergies have been quite active the past few weeks, and given that my average sneeze series has grown from the normal 2 to 3 or 4 at a time, the increase in irritation and wateriness of my eyes has led to excessive rubbing, which has undoubtedly led to this case of pink eye. It's already starting to feel better, as I've been pretty good in the past 24 hours about not touching my face.

But yeah, mostly the other thing :)

[jet]: https://www.jetprogramme.org
