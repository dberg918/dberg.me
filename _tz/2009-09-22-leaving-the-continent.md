---
title: "leaving the continent"
date:   2009-09-22 12:00:00 -0400
layout: post
---

Given that I probably won't be able to update for the next week, I thought I'd post an update here to try and let as many people know as possible.

I'm boarding the first flight tomorrow evening at 6pm out of New York, and after a couple of stops, I should be in Tanzania by Wednesday evening. The first week we're there, all trainees will essentially be isolated in one area while we learn some basic survival language, safety precautions, etc. After that, we travel to our training hub, where we'll meet each week together as a big group during technical training. This is when we meet our host families! Hopefully, by that time I will be able to find a hotspot to post another update on the progress I'm making, whether or not I've purchased a phone yet, and general communication avenues.

Until then, no news is good news!

I just want to take one last moment to say thank you for all of the prayer and support. I'm blown away and so grateful to God for the friends and family that I  have! Even though I can already start to feel some mental (and certainly physical) fatigue beginning to set in, I know that God has purpose for me here. He wants to show me a new way to live, a new way to give, and a new way to love. I just need to keep myself humble and say "yes" to Him.

*God, prepare the way! I'm giving up control to You. Thank you for Your promise to be by my side, Your plans for my life, and Your glorious unfailing love. You know what I need and where I need to be, so put your plan in motion! Draw near to me as I take this first step into unknown lands and unknown challenges. Keep me (and the rest of my new friends) safe this coming week as we travel and learn new things (whether expected or unexpected). Keep us refreshed and energized, and keep us in good spirits. Hold us in the palm of Your hand! You are the reason we're here, and I praise You for choosing us! We're ready to go, so send us in the name of Your gracious son Jesus! Amen.*
