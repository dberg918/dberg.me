---
title: "wtf moments"
date:   2009-10-18 14:00:00 +0300
layout: post
---

We've all had those twilight zone moments in our lives, where you think "what in the world is going on???" My night last night was like this.

Yesterday, I cooked with my CBT in the morning before we took a trip to a school near CCT to play some sports. When I told my mama about the sports day we were having, she informed me that my brother was going to a celebration after dark, and that I would be joining him. So she inquired what time I would be home, and I informed her it wouldn't be later than 6 (the "celebration" started at 7).

After the sports day, I came home and greeted my baba, and then I asked him about the party I was supposed to go to. He told me it was a wedding ceremony, and my mind started to wander. There's already been a few stories about weddings floating around our training class, all of them generally not good. But I was ready to try something new, and I thought it would be fun to see another part of the culture. Given the title of this post, you can see where this is going.

One thing I wasn't expecting...it got cold. And I had a long sleeve dress shirt on. The reception was outdoors, and the night winds were whipping. It even threatened to rain a few times, which would have been quite awful had the sky opened up. Of course, that isn't the wtf moment. First, we need to bring some alcohol into the mix. So when we entered the reception, we receieved two "coupons" for drinks, which my kaka exchanged for two Castle beers for me. For all of you that are unaware, Tanzanian beers are almost twice as big as American beers and contain higher concentrations of alcohol. The Castle beers we had at the wedding were 12 proof. It wasn't long before I realized I was in trouble. I'm well aware of my lightweight status when it comes to consuming alcohol, so I was fairly sure my second beer wasn't even going to be opened. After finishing half of my first beer, I felt light-headed and quite sleepy (remember sports day earlier today?).

Then the hunger set in. My CBT ate our brunch at 12 noon, but it was brunch. We ate fruit, eggs, and bread. Not exactly enough to sustain me for the next 10 hours. But it had to, because we didn't get food at this wedding until 10pm. And here's the wtf moment. Standing in line for food, I noticed the girl at the end of the serving line looked awfully familiar. I thought, "what??? That doesn't make any sense, why would she be here for one, and why in the world would she be serving food???" I was convinced it was someone else, and left it to my imagination that she was in my training class. But sure enough, when I got to the end of the line...

> "Hey Dave."  
> "What are you doing here???"  
> "Serving you food, what does it look like?"  
> "What in the world is going on...there's a story here, isn't there..."  
> "Oh, there is..."

To reiterate, wtf?

What's funny is this would've made the perfect mefloquin-induced vivid dream (mefloquin is my anti-malarial drug). Why on earth it actually happened, I haven't the slightest, but I will definitely get the story from my friend about serving food at the wedding and share it. My mind has been buzzing all night with ideas about what the Peace Corps really does behind the scenes. I was telling my kaka on the walk home that it felt like the Peace Corps was orchestrating these weird situations on purpose to see how we react. Almost like a big brother reality show. But of course, delirium and exhaustion can make you think and say crazy things.

I still can't get over that "wtf???" feeling, though...

**EDIT:** *I know this is long overdue, but I don't want to leave future readers  hanging. The resolution of this story is that my friend, Charlotte, was living with one of the families involved in the wedding. As a "family member," she had to serve food at the reception. Certainly not the kind of experience you would expect to have during your Pre-Service Training in the Peace Corps.*
