---
title: "processing homestay experience"
date:   2009-10-10 12:00:00 +0300
layout: post
---

This is what is on the schedule every morning we have sessions with our CBT, and I found it quite funny the first time I saw it last week. Not that it was actually funny, but because I knew it would be very necessary and that there would be stories.

I've been in the homestay for about 2 weeks, and it's been quite the experience. What's weird is that I had this dual-sided attitude going into it, thinking "I need to stay humble and be prepared for challenges," but really in the back of my mind thinking "this isn't really much of an adjustment, it's not a big deal." I've found out after only 2 weeks that even the small adjustments have the capacity to wear you down over time. Things like bedtimes, eating times, and even chai breaks have had their effect.

Chai is something that was very foreign to us at the beginning of our training. It isn't breakfast, because that was 2 hours ago, and it isn't quite lunch, because that isn't for another 2 hours. It's 10 in the morning and there's practically another meal occurring. Many of us did not eat at chai during the first few days. After a week, however, we have come to love chai very much. To the point that we miss it when there is no chai. Which brings me to another point.

Hot beverages. I don't know why people drink nothing but hot beverages here, but they do. It seems counter-intuitive given the climate; hot and hotter. The only drinks you can get cold here are soda and, on occasion, water (as far as I know). But it's another adjustment many of us have eased into over the past few weeks. While I drank no hot beverages almost at all in America, I have at least two every day now. When we missed chai last Saturday at our school because it was the weekend, we really *missed* chai. We actually paid for it today because we wanted it so badly. I'm fairly certain this is something many of us will be bringing back to the United States. It may even get us fired at our new government jobs when we get back because of our refusal to work at 10am.
