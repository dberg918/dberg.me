---
layout: post
title: "another beginning"
date:   2011-09-05 12:00:00 +0300
---

Apologies for the lack of updates and tweets in the past 2 weeks; it's been hectic with preparations for my new leg of service, and my phone battery is another story. I got a replacement today after showing the broken one to a sales clerk. She commented "imevimba," to which I blindly replied "yes, imevimba," not knowing at all what "vimba" meant. Turns out it means to become inflamed, or in this case, to bulge or swell. It wouldn't fit in the battery compartment anymore, to the point where I could barely close the casing on the back. Anywho, new battery, good times.

Expect some tweets this week; I'll be in Dar at a beach-side resort for a team building retreat with my new family of co-workers! Should be informative and fun.
