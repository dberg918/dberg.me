---
layout: post
title: "there's a bug in my bathroom"
date:   2010-05-31 12:00:00 +0300
---

![Bug](/assets/tz/20100531T084954 - IMG_1483.jpg)

Don't worry, he's taking a nap.

I found him next to my choo last night, and like most bug sightings I make, I froze for a few seconds in sheer terror of his size. He was on his feet at that time (I took the picture this morning), and I could see a bright yellow nocturnal glow coming from his left eye. Yeah, this guy has legit eyeballs.

When I woke up this morning, I was hoping he found his way out of my bathroom, but as you can see, I suppose he had decided to take a nap or something. In any case, I took it upon myself to show him the way out with my broom. He didn't put up a fight as I swept him to the door, which led me to believe the poor guy chose my bathroom as his place to die. Or maybe he arrived in my bathroom and the smell basically killed him (or put him in a daze).

Later that day when I left to go into town, I saw him outside on the ground, very close to the place he landed when I swept him out the door. He appeared to be curled up, trying to get into the fetal position, and a funny thought popped into my brain: "so *that's* why he was in the bathroom; he must've been having digestive distress."

I feel bad for the bugs of Africa. Even more so for the ones that make it into my house.
