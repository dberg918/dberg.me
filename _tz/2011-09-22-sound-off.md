---
layout: post
title: "sound off"
date:   2011-09-22 12:00:00 +0300
---

It's been a slow two weeks.

Not really a whole lot going on in my new line of work just yet, but I'm getting paid to stay in a 3-star hotel on Zanzibar...hard to complain about not having much to do. It is a bit unnerving to think that I've been living out of hotels since the beginning of September though, with no end in sight. I'm still in the dark about my placement since I never got a response about my scope of work from the Chief of Party. That also means I have no job outline, but I've been keeping myself busy preparing for a presentation to persuade our team to try some outside-the-box techniques for developing training manuals. As long as this goes through, I'll have plenty to keep me busy for the next year. If it doesn't for some reason...well, I simply refuse to think in that direction.

That's pretty much the end of personal news, besides getting food poisoning on Sunday night. Turned out pretty well, actually, since I got to stay in Monday and see A Few Good Men on the Dubai One channel at my hotel.

Since it's been so slow, I've been taking time to keep up with world news, partially with the help of estranged acquaintances on Facebook who flooded my news feed with Troy Davis postings. I've only read a handful of news articles, so I can't express an opinion on the matter, but it does make me wonder how others form their own; did they do any research into the trial and witness statements, or did they read some slanted post on their personal liberal web hub of choice and take it on good faith?

The other story out there right now is Palestine's bid for Statehood in the UN. I can honestly say I have no idea what this means, though I hope to do some reading now that I have an internet connection. Obama has already stated his position on the matter, and I can tell this is going to blow up in my Facebook feed in the coming days. Say whatever you want about Obama and his butt-smooching posture towards Israel; I think he understands how little political posturing has influence over that situation. If Obama supported statehood for Palestine, would that stop the conflict? No. It would just make some people happy and other people angry. In essence, it would do absolutely nothing, besides changing the attitudes amongst a handful of people. What's the solution, then? Figuring out a way to make people not angry. Life would be a whole lot simpler without pride, wouldn't it?

Expect me to sound off like this on the blog every once in a while. Otherwise, I think it might go deafly silent in the wake of how commonplace it feels to live in Tanzania with 2 years under my belt.
