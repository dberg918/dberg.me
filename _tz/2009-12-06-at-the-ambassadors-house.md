---
layout: post
title: "at the ambassador's house"
date:   2009-12-06 18:00:00 +0300
---

I can scratch two things off my bucket list now:
 - Give a speech in a foreign language
 - Play guitar at a U.S. Ambassabor's house

Those would look pretty good on a resume, wouldn't they?

Our swear-in ceremony took place the day before Thanksgiving at the U.S. Ambassabor's house in Tanzania. We had the privilege of performing a skit to mark the occasion, which was a short 10-minute musical dreamed up by two of our brilliant trainees, who developed a script for the "coming-of-age" play about a volunteer reflecting on how her service in the Peace Corps changed her world. They also rewrote lyrics to The Boxer by Simon and Garfunkel and With A Little Help From My Friends by The Beatles to fit the musical, and I learned the chords to play them live.

I also had the honor of giving the "comments" for our training class in Kiswahili. I actually enjoyed the moment, speaking some words I didn't even know the meaning of. It was a very unique and exciting experience. After speeches by some of the guests and by the U.S. Ambassador, we took the oath on national television to swear-in officially as Peace Corps Volunteers. Cake and refreshments followed shortly thereafter.

That night, we got to attend a dinner at our Country Director's house. Given that we would be travelling on buses all day the next day, Thanksgiving, she wanted to give us a proper meal before we headed out on our journeys. We got to meet her husband and her children, all absolutely delightful people, and all well spoken and educated. It turned out to be more than a dinner in the end, when the Michael Jackson started cranking in the living room. There, in a modest space normally suited to hosts and guests chatting and drinking hot teas and coffee, a handful of newly sworn-in Volunteers, the Country Director, and some of her children held a veritable dance party to the 70's mega-hit Thriller.

I guess that should go on the list too, shouldn't it?
