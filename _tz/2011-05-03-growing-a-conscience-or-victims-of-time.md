---
layout: post
title: "growing a conscience, or victims of time?"
date:   2011-05-03 12:00:00 +0300
---

Given that you've made it to my lowly blog to read this, I can only assume you've seen the MLK-quote phenomenon sweep across your web browser, either on Facebook or Twitter or some news site scavenging for hits. It's probably even landed in newspapers all over America. If you haven't (maybe you've been unconscious the past few days?), here's the quote for your reference:

> I will mourn the loss of thousands of precious lives, but I will not rejoice in the death of one, not even an enemy. Returning hate for hate multiplies hate, adding deeper darkness to a night already devoid of stars. Darkness cannot drive out darkness; only light can do that. Hate cannot drive out hate; only love can do that.

It has since been identified as <a href="http://www.theatlantic.com/national/archive/2011/05/anatomy-of-a-fake-quotation/238257/">slightly mangled</a> (even as people continue to spread it), but that's not what fascinates me. What has piqued my interest is whether or not people truly feel this way, because it's almost precisely how I felt when I first heard the news. In my own heart, the jury's still out as to why there's conflicting feelings, but if there's more than just a handful of us that have reacted this way, perhaps there's a common denominator. Maybe, like Ryan from The Office confesses as an excuse for his negligent behavior as a VP, we "never really processed 9/11." Maybe the past 10 years were enough to sufficiently fog our memory and, to a larger extent, our hatred. Do you think you'd feel any different had Osama been killed back in 2001 or 2002? We were different people back then, weren't we?

There's no doubt that I think the world is better off without a guy like bin Laden in it, but it's best that we not forget one thing, which I think is well embodied in this statement from a friend of mine on Facebook: "God is...just, and the death of one man is God's justice fulfilled. However, we shouldn't forget that we too deserve this same justice." (my thanks to Adam for this insight)
