---
layout: post
title: "gearing up and winding down"
date:   2010-11-09 12:00:00 +0300
---

My first year of teaching has unofficially come to a close. The Form IVs finished their NECTAs back in mid-October and the Form IIs have now started their national exams. Over the next few weeks, perhaps I'll reflect. Right now, I'm getting ready to host 8 visitors on Friday. While I didn't receive any shadowers personally (for reasons unknown to me), I invited everyone in the Singida region to my site for a huge party. I think we're celebrating 3 birthdays, Halloween, Election Day...maybe we'll throw pre-Thanksgiving into the mix.

The other big headline is my trip back to America for Christmas and New Years. I'm waiting to get the extra vacation days approved by the office, otherwise I have my flight picked out and ready to book. It's a strange feeling pondering all the things that have happened since I left home, the anticipation of being overwhelmed by my own culture (my own family, even), and predicting the lack of gracefulness that will be on full display throughout my visit. But more than anything I think this trip will confirm deep in my being that I've managed to unboring my life, and I think there will be an unspeakable joy found within that realization.

T-minus 41 days...
