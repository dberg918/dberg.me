---
layout: post
title: "let there be light!"
date:   2010-07-18 12:00:00 +0300
---

...among other things.

It hasn't been very long since I created my Twitter account, but I already feel like I'm getting Twitter Brain. This is my name for the condition where you find yourself wanting to share every mundane detail of your life with the world, but can't for the life of you generate an interesting thought of any substance worthy of a blog entry. So I apologize for the following paragraphs, because they're basically just a collection of tweets.

The title of this post is an allusion to the electricity which is now flowing through the power lines in my house. Perhaps now I'll start cooking again, or better yet, heat some water and take a bath!

School's back in session, but I spent most of this past week in Dodoma with a focus group reviewing the education program here in TZ. I'm so unbelievably far behind I've resolved to simply teach whatever I can before November rolls around. There's no use in stressing myself out teaching a billion hours a week to catch up, since that wouldn't be fun for anyone, I doubt my students would benefit from it.

And today, I almost witnessed a pig slaughter! Just a few minutes late, the head had already been removed. They were taking out the innards, however, which was interesting to watch. I took video on my phone! If you'd like to see it, leave a comment and I'll put it on Youtube. I suppose I don't have to give you the warning about it being graphic, but it certainly isn't for the faint of heart (although you could probably handle it, dad).

[comment]: # (Again, I apologize for the quality of this entry. If you want a good read, check out Caty's latest post in the sidebar below. It's heart-breaking, but a good commentary on Tanzanian culture regarding an all-too-familiar issue that many teenagers face worldwide.)

[comment]: # (Happy reading!)
