---
layout: post
title: "mahitaji ya wengi (the needs of many)"
date:   2012-07-20 12:00:00 +0300
---

When was the last time you gave food to someone who was hungry?

Before last week, it had been a long time for me. Sadly, I had sunk into the attitude most Volunteers don as their service wears on -- I'm just as poor as they are, why do I need to give anything? I've already given up enough just coming here, right? While it appears to be true on the surface, it is a dirty, vile lie.

If you had followed me around since I got here, you would've discovered that I was being very generous to my friends. Whether it be buying food or drinks for them, paying for daladala rides or what-have-you, about one-tenth (probably more) of my monthly income was being invested in other people. And it was done, for the most part anyway, without any expectation of reciprocation. I often got strange looks from them, but they were usually very grateful. You would think I'd feel pretty good about that, but after last week, I realized how terrible it is.

I'm investing in people that don't need my help.

Last week, God presented me with a number of opportunities to be charitable which I acted upon, usually by giving someone food that was in my hand. In every case, it was a small sacrifice, maybe a fraction of a sandwich or some chicken and chips that, had it remained in my possession, certainly would've been consumed, but I was okay with letting it go. I won't lie to you, I felt like Mother Theresa when I did it. Actually, let me correct myself -- I don't know how Mother Theresa<i></i>felt, but I was definitely on the receiving end of how Ifelt about her. I did all the right things; I tried to be as discrete as possible, I did it with a smile, and I moved on. Despite the fact I just told you about it, which can be construed as me gloating of my unbelievably big heart (totally true, just ask my parents).

The point is, things changed after that. All of a sudden, I had this hyper-awareness of need that was all around me. I remembered acutely every person I passed by that was asking me for something. Over the course of 20 minutes when walking to the bank one day, I was asked for money twice, food once, and for "help" once. Where were all these needy people yesterday? I came up with a couple of theories.

The first is that they could sense the generosity in me, and that's why they were asking. God was telling them "ask that guy for help, I sent him here. He's working for me." This was the first thought I had about what was going on. I was suspicious before, but now that I've written it down, I can plainly see how arrogant it is. People sensing that I'm generous? Yeesh, what a douche.

The other, probably much closer to true, theory that I don't want to believe is that they were always there asking -- the difference was either my ears were closed or my heart was.

It's easy to be generous when we're doing it for friends, or when it's convenient, or when there's a chance we'll be reciprocated. But sometimes God wants us to make bigger sacrifices for the sake of his kids, and it's our responsibility to respond when the time comes.
