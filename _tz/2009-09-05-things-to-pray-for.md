---
title: "things to pray for"
date: 2009-09-05 12:00:00 -0500
layout: post
---

Just so I don't forget...
 - My training class and their preparation
 - That everyone gets their passport/visa
 - Safe travel overseas
 - For my host family and my trainers
 - That I find Christ-centered community in Tanzania
 - That God uses my future home in whatever way He sees fit
 - Humility, every moment of every day
 - Boldness
 - An open mind
