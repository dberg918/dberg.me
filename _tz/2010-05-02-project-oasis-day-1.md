---
layout: post
title: "project oasis: day 1"
date:   2010-05-02 12:00:00 +0300
---

I have declared war on my house.

Last Sunday night was the first time I slept in my house in about 23 days.Granted, about 16 of those days were because of IST. It's hard to pinpoint the reason why I don't want to sleep in it, but generally it has to do with how dirty it is. It isn't clean by any sense of the word, and I had become a bit pessimistic about the process of cleaning it. Because I have a colony of bats taking refuge in my ceiling (they've been here longer than I have), the house doesn't stay clean very long after it's been swept and mopped.After a few months, I had essentially decided to give up. Instead of fighting back, I simply avoided the problem; for a month, I literally did nothing in my house besides sleep. Then, about a week before IST, I didn't even do that. I took shelter in my second master's room until I had to leave for Dodoma.

But now, after IST, I've realized the importance of having my own space and keeping it clean. I'm fairly certain that having a home I feel comfortable in will go a long way in helping me do my job better, and that the length of my stay here is almost entirely dependent on my house at this point. While I had found a way to survive without it for some time, I don't think that life was in any way sustainable for 2 years. It's now time for me to fight, and take back what's mine!

<iframe width="560" height="315"
src="https://www.youtube.com/embed/rpu4RTNhgqg" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
allowfullscreen></iframe>

And for those of you who are artists in the audience, I invite you to help me pimp my house! When I do get things back to an inhabitable state, I'm looking to start decorating the walls, preferably with African-inspired art. If any of you have ideas, leave a comment or send me an email. If you can upload drawings or sketches of layouts or designs, even better! In return, I will brew you some wine when I get home; I just started my first batch of Orange-Pineapple last Tuesday and I'm very excited to see how it turns out!
