---
layout: post
title: "kkk: big problems in tanzania"
date:   2012-05-10 12:00:00 +0300
---

First off, no. It's not what you think. It is indeed an acronym, but it stands for something different in the Education sector of Tanzania. The order varies depending on who you ask. In fact, you could ask the same person 6 times and get six different combinations of the same three things:

*Kuandika. Kusoma. Kuhesabu.*

For the Swahili-challenged out there, these words refer to three essential skills that students should learn in their early formative years; Writing, Reading, and Math. It's a topic we're addressing with the TZ21 Project. My counterpart estimates that approximately 20-25% (!!) of primary school students in our district struggle with "KKK." But that's not why I'm writing this entry.

I'm writing this entry because of the irony and absurdity of the above acronym. It's pretty hilarious to hear a bunch of education officials deliberating in a meeting about how big a problem "KKK" is in Tanzania. Yes, that's exactly how they refer to it. The real reason why this acronym is so absurd is because it's built with the infinitive form of each verb. And in Swahili, *every* infinitive verb begins with a "K." The English equivalent would be using "TTT" as the acronym for "To write. To read. To do math."

I'm brainstorming some new meanings that I can use as bad Swahili jokes with my colleagues. If you PCVs have any good ones (shouldn't be too hard, right?), leave'em in the comments.

[comment]: # (*Afternote: accept my apologies for trolling you with the title. It was a shameless way to get hits on my blog, and I've probably just invited myself to get plastered with hate mail and death threats from the members of the American organization with the same three letters.*)
