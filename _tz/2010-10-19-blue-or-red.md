---
layout: post
title: "blue or red?"
date:   2010-10-19 12:00:00 +0300
---

I've been contemplating my future in recent days. The reasons behind this vary, from the book I've been absorbed in since last week, to the incomprehensible groanings and mumblings within my heart.

President Obama's vision for a revolution in the way we think about government in the 21st century has my head spinning (it's quite detailed and technical), but it also has me wondering where I fit into his picture. He talks constantly about training and raising a new generation of engineers and scientists which, if you look at my college degree, I am a part of. Unfortunately, I get the sense that, perhaps not my opportunity for a engineering-based career, but rather my desire for one is beginning to slip. The plight of Africa is hard to ignore, especially when you're steeped right smack in the middle of it, and its gravity is starting to tug and pull. Just last Saturday, on my way home from a short visit with a nearby friend, crazy thoughts about spending the rest of my life here were cascading through me, the sheer implications of them weighing heavy within me. "It doesn't matter what you do, really, just being here and suffering alongside is the important part." I've been praying about these groanings, especially now that I'm distanced from them by a few days, because I'm not entirely sure where they originated.

At times, I find it a bit alarming how swiftly and gracefully I made the transition from science-centric college grad to impoverished social worker. I know the vast majority of fresh, young, undergraduate degree holders don't necessarily go into the fields they majored in, at least not right away, but in something as specialized as aerospace engineering (with an emphasis on the "space"), I feel like there's generally an expiration date on how long you can hold out on your future, and the longer you wait, the more your prospective employers will wonder where your allegiance, or your heart, truly aligns.

One thing I'm sure of is that this tension which has surfaced has far-reaching consequences. It could determine how and where I spend the prime years of my life. And something I'm beginning to ponder is whether or not I'm currently receiving what Oswald Chambers dubs the irresistible and "baffling call of God." When I read that phrase, I used to think of God simply directing my career path. Now I can see it's a little more involved than that.
