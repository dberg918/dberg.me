---
layout: post
title: "a loooong weekend off"
date:   2011-04-21 12:00:00 +0300
---

Traditionally, Easter weekend is a long weekend here in Tanzania because we have Good Friday and the following Monday (called "Easter Monday") off. This year it's even longer because Union Day falls on Tuesday of next week, so it's become a nice little 5-day break. As if that wasn't enough, I found out yesterday that my school is hosting this year's Catholic student Easter Conference. I'm not sure exactly what it is, but I've donated quite a bit of money to students so they can go; they walk around town with these donation forms, asking people for money so they can attend the conference. I usually give around 500 /= to each student (a little less than $0.40), since it's normally the case that around 10-12 students ask me for a donation.

Anyway, we're hosting the conference at our school this year, so we haven't had classes since Tuesday. Add to that my normal Tuesday schedule of zero periods, and I find myself not working for over a week. And this is just a week after we finished our week-and-a-half break after mid-terms. Funny how things work out sometimes.

This is good because it gives me a chance to breathe after the madness that was my month of March. It also gives me time to get really sick (Tuesday night); the first time I've been legit sick in quite a while. It was almost nostalgic how much it reminded me of when I got foot drop that crazy second week of my service. Don't fret! I'm all better. I'm not nearly as clueless now as I was then about taking care of myself, not to mention all that sickness last year has beefed up my immune system.

[comment]: # (I'm also excited because I think I'll finally have some time to process this past month in the form of a video !!!! :D !!!!!, so be on the lookout these next few days. Should be good given the relatively ginormous amount of footage I've taken since I put together that last one. Man, that was all the way back in February...how time flies!)

Enjoy the home stretch of Lent, everybody!
