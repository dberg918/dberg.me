---
layout: post
title: "april is blog about malaria month!"
date:   2012-04-27 12:00:00 +0300
published: false
---

<a href="http://media.tumblr.com/tumblr_m1ue8fKTZp1qkomze.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="http://media.tumblr.com/tumblr_m1ue8fKTZp1qkomze.png" /></a>

I know I'm late to the party, especially since I wasn't in Tanzania for most of April, but this month is all about Malaria awareness in Peace Corps land. Not only are we spreading the word throughout our Volunteer community and our village communities, we're also <a href="http://katie-tumaini.blogspot.com/2012/04/every-child-deserves-5th-birthday.html">blogging</a> <a href="http://sevintanzania.blogspot.com/2012/04/every-child-deserves-5th-birthday.html">about it</a> to send some shock waves through our communities back home!

Admittedly, my personal experience with Malaria has been quite limited. In my first two years, I lived in a semi-arid climate where even insects like mosquitoes had trouble surviving. I also took my prophylaxis religiously and slept under a bed net every night just to be safe. There was only one occasion where I was sick with symptoms that hinted at Malaria, but it turned out to be some sort of auxiliary infection.

Nevertheless, Malaria is no joke. It has evolved and developed resistance to drugs. Even though I was State-side this month, I still had to take my anti-malaria drug to keep my immunity up. And even if you're good about taking it, some people still get it anyway. From what I've seen, Tanzanians fear it more than HIV. And for good reason; in Tanzania, it kills 1 in 5 children that are younger than 5. Given that the birth rate here is close to 5 children per mother, (statistically speaking) nearly every mother experiences the death of a child because of Malaria!

[comment]: # (If you want to help Stomp Out Malaria in 2012, go to <a href="http://stompoutmalaria.org/">stompoutmalaria.org</a>, check out the people involved, and send encouraging notes or blog comments to Volunteers doing Malaria work on the ground. There are <a href="http://stompoutmalaria.org/country-by-country/">23 countries</a> included in the project <a href="http://sevintanzania.blogspot.com/">Carol</a> leads our intervention in TZ, and I'm sure they would all appreciate your support!)
