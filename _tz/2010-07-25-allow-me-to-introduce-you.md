---
layout: post
title: "allow me to introduce you..."
date:   2010-07-25 12:00:00 +0300
---

<a href="http://4.bp.blogspot.com/_WM5vLMqkVOk/TEv-ChdgHsI/AAAAAAAAAMI/_0Q5OHpQf7g/s1600/PIMG0021.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom:1em; margin-left: 1em;"><img border="0" height="200" src="http://4.bp.blogspot.com/_WM5vLMqkVOk/TEv-ChdgHsI/AAAAAAAAAMI/_0Q5OHpQf7g/s200/PIMG0021.jpg" width="150" /></a>

This is my friend Sam. He's been my wake-up call this weekend. Yesterday and today, I heard a knock on the door that shook me from slumber. 8:38am, both days. Like clockwork. The reason he's been knocking on my door is a bit heart-breaking. For the past few days, he's been coming over to retrieve bandages from my med kit because he has a gaping hole where his ankle used to be. It was a farm accident, from what I can understand of the conversations that occur between the children when they come over. Sam is one of the kids who has been transforming my heart over the past 8 months. I remember having a bad first impression of him because of how recklessly he rooted through my property, but now he's one of my guardian angels here. He will keep the other kids in check if they do something I don't like. He also likes to help me with house chores, sometimes without any kind of provocation from me. Like today, as I was washing some of my clothes, he picked up the mop and bucket and started mopping the living room. Then he said the cutest thing after he started: "Wait, the floor hasn't been swept first..." So he picked up the broom :)

<a href="http://4.bp.blogspot.com/_WM5vLMqkVOk/TEv_70JfgfI/AAAAAAAAAMQ/pJ2MwiSMnOE/s1600/PIMG-0022.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" height="200" src="http://4.bp.blogspot.com/_WM5vLMqkVOk/TEv_70JfgfI/AAAAAAAAAMQ/pJ2MwiSMnOE/s200/PIMG-0022.jpg" width="150" /></a>

This is my friend Elise. She is usually knocking on the door shortly after Sam comes over (I think they're siblings, but I'm not positive). She wins the superlative of Child I Always Want To Pick Up And Hold The Most. I don't know why. She's just so adorable! I can't even remember how it all started. I think I just started tickling her one day and fell in love with her. Maybe it's because she likes hugs as much as I do. Whenever my little friends are over playing with all my stuff and I'm getting ready to leave, Elise always wants me to carry her outside (a total of about 7 feet). How cute is that??

I don't know what it is about African kids, they're just so full of personality, even at such tender ages. Without a doubt, the children of Africa have been the biggest blessing and the biggest challenge to my heart in the past 10 months.

Jesus called them to *Him* and said, "<span style="color: #e06666;">Let the little children come to Me, and do **not** forbid them; for of such is the kingdom of God. Assuredly, I say to you, whoever does not receive the kingdom of God as a little child will by no means enter it.</span>" - Luke 18:16-17
