---
layout: post
title: "a movie theater in africa?"
date:   2012-01-08 12:00:00 +0300
published: false
---

<table cellpadding="0" cellspacing="0" class="tr-caption-container" style="float: right; margin-left: 1em; text-align: right;"><tbody><tr><td style="text-align: center;"><a href="http://4.bp.blogspot.com/-6RGiem9V1Ao/Twk5mjElDWI/AAAAAAAAAcU/PelTtwnIj-s/s1600/PIMG-0149.jpg" imageanchor="1" style="clear: right; margin-bottom: 1em; margin-left: auto; margin-right: auto;"><img border="0" height="150" src="http://4.bp.blogspot.com/-6RGiem9V1Ao/Twk5mjElDWI/AAAAAAAAAcU/PelTtwnIj-s/s200/PIMG-0149.jpg" width="200" /></a></td></tr><tr><td class="tr-caption" style="text-align: center;">Blackout shutters!</td></tr></tbody></table>

As you may remember, in one of my previous entries I <a href="http://peacecorpsdmb.blogspot.com/2011/12/renovatingor-rebuilding.html">hinted at big plans</a> I had for my living area (check the last paragraph). That was referring to a hasty decision to, as a Christmas present to myself, rig up a home theater system in it, complete with surround sound system and ceiling-mounted projector. Crazy idea, right? I'm still only in the planning stages though, and as it stands there are some rather large obstacles I'll have to confront before the dream becomes reality.

First is that of power. After my first week in the new house, I've noticed that this is going to be an issue. Unlike in Singida, where the power outages were actually predictable and you could plan around them, the cuts here are not. They also vary in length from a few minutes to almost an entire day.

Second is the sound system. The projector and the PC to drive the content are things I plan to buy while at home in March, but the sound system is rather large and would be difficult to take on a plane. This means it's better to just get one here, and the closer to home it already is, the better. Unfortunately, all the shops in my town (while they surprisingly have good brands like Sony) stock systems that have no inputs (or rather, non-HDMI inputs that don't support surround sound), meaning that the only (surround) sound you can get from it is the sound from the included DVD player. They're also about 3-4x more expensive than their retail price, though that's to be expected given the circumstances. Looks like I'll have to shop around Dar for this.

The remaining issues are a bit more abstract; how much money do I want to spend, and is it really worth it when I'll only have it here for about 6 months? And things break in Africa. Maybe it's better to wait.

<a href="http://3.bp.blogspot.com/-2j9jT7sGaGk/TwlEfHcITYI/AAAAAAAAAcc/Dq0oqLB5BRo/s1600/PIMG-0150.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="360" src="http://3.bp.blogspot.com/-2j9jT7sGaGk/TwlEfHcITYI/AAAAAAAAAcc/Dq0oqLB5BRo/s400/PIMG-0150.jpg" width="480" /></a>

But then I look at this room. Gosh darnit, it's just too perfect for this. It would be a crime *not* to do something this crazy.
