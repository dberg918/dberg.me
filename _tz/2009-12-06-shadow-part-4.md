---
layout: post
title: "shadow: part 4"
date:   2009-12-06 15:00:00 +0300
---

We awoke quickly Friday morning to accompany our hostess to her school where, because it was getting close to exams, she didn't teach formal classes, but let the students ask questions about the material. Since we were there, it quickly turned into a question session about America. In the 4 classes we visited, we were asked about Michael Jackson twice, to which I was happy to quickly reply "Michael Jackson is dead."

Later in the afternoon, we decided to take a hike out to a town near our shadow site, which is essentially a town up on top of a cliff. The walk took about an hour, and much like the scenery everywhere in Lushoto, it was BEAUTIFUL! The pictures hardly do it justice. We ate lunch in town, bought some groceries for our dinner that night, then headed back.

Our evening was fairly quiet, consisting of reading and listening to the radio. A heavy rainstorm rolled in about the time we started cooking our dinner and we saw a full-arch rainbow over the mountains! Our dinner that night was sloppy joes with homemade rolls and chocolate bundt cake. A satisfying ending to a delightful day. Almost immediately after finishing our food, we had to hit the hay because the next day had an early wake-up call; 4:15am. The reason being the bus we were going to catch back into Lushoto passed by our neck of the woods at about 5:00am. A shame, considering that's one of the latest buses you can board to go back south. I suppose there's a price to the landscape.

Nearing the end of our story, in the next installment of "shadow," we meet up with some other volunteers for a party.
