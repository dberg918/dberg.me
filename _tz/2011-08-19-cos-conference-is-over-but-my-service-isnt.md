---
layout: post
title: "cos conference is over, but my service isn't"
date:   2011-08-19 12:00:00 +0300
---

It's official: I'm extending for a 3rd year!

I'm still exhausted from last night's "closing ceremonies" and subsequent 4-hour dance party, not to mention the interview this morning that sealed the deal, so I'm not going into detail, but I'm working with a project called TZ21 Basic Education. It's essentially an initiative to incorporate computers and ICT frameworks into primary schools in Mtwara region and on Zanzibar. From the meeting I had with the Chief of Party, it sounds like I'm headed for Mtwara. I am, however, attending a team building retreat in about 2 weeks here in Dar, and participating in a 2-week-long needs assessment on Zanzibar right after. So most of my time in September will be spent away from site. Perfect timing really, since that's when my replacement will arrive. He'll get to spend a good amount of time getting to know the lay of the land on his own, which is what I was hoping for.

I'm not sure what will happen after that; it all depends on when my accommodations down south are ready. I wasn't given a time-frame for that, but I'd imagine end of September or sometime in October would certainly be reasonable.

So yeah. Exciting times. You can stop worrying now, Mom and Dad!
