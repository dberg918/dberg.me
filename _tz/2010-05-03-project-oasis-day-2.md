---
layout: post
title: "project oasis: day 2"
date:   2010-05-03 12:00:00 +0300
---

Things have been going well on the OASIS front. I've cooked more in the last week than I have in the past few months! I consider that in itself a major victory. I'm still not very good at lighting fires though; I nearly run through an entire box of matches each time I attempt to start up the charcoal jiko.

<iframe width="560" height="315"
src="https://www.youtube.com/embed/9xatLTesO4I" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
allowfullscreen></iframe>

Also, one of the walls in my house has seen a paint job, and a rather unorthodox one at that. It was inspired by one of the volunteers I shadowed during training; one of her walls was painted like this. If you can figure out why I painted the wall the way I did, leave your best guess in a comment. I'll reveal the answer next week, hopefully with another video.
