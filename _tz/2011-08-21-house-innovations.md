---
layout: post
title: 'house "innovations"'
date:   2011-08-21 12:00:00 +0300
---

I'm back at my house as of yesterday evening, and it looks like a dust devil has wrecked havoc on its insides. Mostly because of the ongoing renovations that started while I was gone.

Two weeks ago, my headmaster grew worried about what he had to do to get ready for my replacement; Peace Corps had sent him a letter about coming to Dar to pick up the new Volunteer, but they hadn't told him how to prepare for his arrival. To quell his fears, I gave him a tour of my house and provided a list of repairs he should make before the end of August. He repeatedly called these repairs "innovations," which I find so hilarious and strangely heart-warming that I haven't corrected him yet. As a matter of fact, I might start using it that way myself just for the hell of it.

<iframe allowfullscreen="" frameborder="0" height="300" src="https://www.youtube-nocookie.com/embed/1WZ8bj4xKwk?rel=0" width="480"></iframe>

So now I've returned, and much to my delight, something actually got done while I was gone! Granted, it would've been nice if they'd run a broom across the floor after they finished working, but the back room finally looks livable! I'd actually like to move my bed in there now (see video). Also amazing is that they patched the hole where the bats were getting in (and more recently a swarm of bees, don't ask), so I think that problem is finally taken care of.

They still have some ceiling boards to put in above the kitchen, but I consider the house already substantially improved from its condition throughout the past 2 years. Hopefully the next volunteer will keep it this way, and the bats won't find another way in...
