---
layout: post
title: "oven at last, oven at last!"
date:   2012-01-12 12:00:00 +0300
---

<a href="http://2.bp.blogspot.com/-3Ox4UHEGW3w/Tw7wlQmBJJI/AAAAAAAAAck/jO0rQ33AkZs/s1600/PIMG-0155.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="360" src="http://2.bp.blogspot.com/-3Ox4UHEGW3w/Tw7wlQmBJJI/AAAAAAAAAck/jO0rQ33AkZs/s320/PIMG-0155.jpg" width="480" /></a>

To celebrate Zanzibar Revolutionary Day (Zanzibar's "Independence Day"), I've purchased this giant cooking apparatus. It's been nearly two weeks since I moved in and I've had no way to cook for myself. While I went almost 6 months in Singida without cooking anything at home, the circumstances are now quite different. First of all, I'm not scared of poisoning myself anymore. Second, quick access to local *mgahawas* (small shops that serve food) is quite limited here, and they're a bit more expensive. Lastly, I actually have a decent grip on some culinary skills now, and would like to continue extending them.

As you can see in the picture above, the cooking unit I've selected has a small oven (!!). What you aren't able to see is the auxiliary rotisserie function it also has (!!!). Yeah, that means I can make my own rotisserie chicken. Although that also means I need to learn how to slaughter a chicken; you can't buy pre-slaughtered chickens outside of Dar (not necessarily a bad thing). I'll let you know if I make any headway on that skill.

But I'm really excited about having an oven. I can actually bake things for real! I'm using this as an opportunity to get into bread-making. While I've seen bread around town (even some that says it's made in Masasi), I haven't found a bakery, which is where I prefer to buy my bread products. Ipso facto, I'll be my own bakery.

I'll post pictures when I succeed, either in slaughtering a chicken or making a loaf of bread. Graphic images (most likely of the former) will be accompanied with an appropriate warning if necessary.
