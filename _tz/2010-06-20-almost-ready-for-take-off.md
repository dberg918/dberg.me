---
layout: post
title: "almost ready for take-off"
date:   2010-06-20 12:00:00 +0300
---

Life has been pretty unexciting here the past few weeks, besides all the World Cup hysteria. Hence the mixed-bag nature of my recent posts (including this one).

Despite the unexcitement, I'm growing quite eager for my Zambia trip next week. I leave my site on Thursday for Dar to spend a few days getting some admin stuff taken care of, and then I fly out on Monday!

I'm visiting a friend I knew in a previous American life, before we both turned into PCVs. I'll be there for about 9 days, doing...well, I don't really know. She's done all the planning and told me nothing, so I'll be taking it day by day. I do have some surprises planned for her though, since it'll be her birthday on the 1st.

Besides that, I've been living without power for the past week and a half. It makes me laugh how easy it is to negotiate here in Tanzania, especially when I think about the time in DE when our power got knocked out by one of those hurricanes for, what, 3 days, and we almost went insane!

I also finished my second batch of wine this past week, and just today I helped the academic master at my school start his first batch (most productive day I've had in a few weeks, sadly). I've started giving my wines silly names, this one being "Honey, it's Bananas!" because I used unripened bananas and honey. The first batch I've dubbed "Orange You Glad I Used Pineapple?"

Please don't make fun of me.
