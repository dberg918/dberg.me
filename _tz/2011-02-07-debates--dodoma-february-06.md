---
layout: post
title: "debates & dodoma: february 06"
date:   2011-02-07 12:00:00 +0300
---

<div style="text-align: center;"><iframe frameborder="0" height="270" src="https://player.vimeo.com/video/19658881?byline=0&amp;portrait=0" width="480"></iframe></div>

Just to elaborate a bit on the above, I hope to have two special updates in the coming weeks; one with some rock climbing and another about the Shika project. Both are currently tentative. Cross your fingers that I'll be able to get them in! And pray that I'll be a good teacher in the meantime...

[comment]: # (**UPDATE:** It's been brought to my attention that the Shika site link in the video doesn't work. As it turns out, the site hasn't gone public yet. My bad. You can, however, view our code page, which currently has a PDF draft of the International Shika manual in the Downloads section. Check it out <a href="http://shika-na-mikono.googlecode.com/">here</a>.)
