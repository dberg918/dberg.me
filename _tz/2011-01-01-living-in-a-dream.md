---
layout: post
title: "living [in] a dream"
date:   2011-01-01 12:00:00 +0300
---

I debated whether or not I would blog at the dawn of 2011, and I suppose you can see where I ended up. The debate existing because I'm currently State-side, and blogging would be unnecessary since my life in Tanzania is now on hold. Turns out a post might do me some good in expressing the things I'm experiencing in the US after spending a year abroad.

<div style="text-align: center;"><iframe class="youtube-player" frameborder="0" height="278" src="https://www.youtube.com/embed/mxfdE_LySpw?hd=1" title="YouTube video player" type="text/html" width="480"></iframe></div>

Thoughts about this trip have been bouncing around in my brain for 5 months. That's when I first started talking to my parents about coming home for the holidays, and I had been dreaming about America ever since. As December approached, and as I saw my friends take their leave across the Atlantic and come back, I began to wonder what my encounter with American culture would be like. I've heard that reverse culture shock can be even more profound and dizzying than the shock of visiting another country, so I began to think that being back in the US may not be the relaxing dream vacation I was thinking it would be. Would I feel out of place? Would I be overwhelmed? I was preparing myself mentally as I boarded the plane at Nyerere International Airport.

After a solid week and a half back on this side of the globe, I'm baffled. And not in the way I expected. Coming back to America was easy. Suspiciously easy. I'm actually kind of disappointed in how unaffected I've been by my time in Africa. I've made this remark to almost all the friends and family I've seen here: this past year feels a lot like a dream right now, and I've finally woken up. But now there's all these strange pictures and videos of me in some other country, with people I didn't know in America, and I have 3 or 4 different kinds of currency in my wallet. Perhaps if I was back in the United States for the indefinite future, having to look for a job/career, pay bills and such, I might feel a bit different. After all, the best kinds of vacations are the ones where you can forget about all that stuff.

And maybe I'm being a bit melodramatic. I do dread the thought of my return, though. If my mind is trying to trick me into thinking all this didn't really happen, what happens when it's time to come back to reality?
