---
layout: post
title: "big ideas, little time"
date:   2011-04-11 12:00:00 +0300
---

Today marked the end of our O-level mid-term break. It also marked the end of A level break at our school, so our boarding students returned yesterday. The result: utter chaos.

We don't have much room in our dorms for boarding students, so when the government decided to give us 70 new Form Vs on top of the 58 we had last year, we had to improvise to make room for them all. Unfortunately, this meant sacrificing a Form II classroom and converting it into a dorm. Of course, we had to do all of this at the last minute, so when everyone showed up all at once this morning, some of our students had no classroom to go to. As you can imagine, things didn't go very well today. Alas, I hold out hope things will return to normal before the end of the week.

That wasn't all of the excitement today brought, however. I also got 5 phone calls from other volunteers, 5 more than I normally get, with a big idea to create a volunteer-run website. It would host all of our training manuals, our cookbook, include a wiki where people can share ideas and comment on them, a space to share funny anecdotes and stories, pictures, videos, basically everything a volunteer wants and needs in one place. Of course, being the newly-appointed tech guru amongst the PCV community in Tanzania, they turned to me thinking I could implement something like this.

Just to be clear, I'm in unload mode at the moment. Things were already stressful enough given that I'm set with stuff I'll be doing until November (the teacher manuals we made in Morogoro still need pictures that I have to scan into a digital format, there's a DVD that has to be made at some point with the 80-some activities we filmed), not to mention I'll be attending a workshop with the Peace Corps staff next Monday to develop the new training manuals for the incoming Ed class in June, which I have to prepare a presentation for about why we're switching from Microsoft Word to LaTeX and Mercurial (just Google them if you're curious, I don't have time to explain what they are). While there, I'll have to train the entire office staff, a handful of volunteers, and talk with the office IT guy about how to take care of administrative issues that go along with the switch. What else is there...oh yeah, I'm supposed to be *teaching at school* on top of this, no biggie...just my primary assignment in the Peace Corps, that's all. That has also brought some stress, by the way, given that I've been ousted from the Form I classroom and thrust into a Physics-only schedule, yet again. At least I got a few solid months of Math in. Now I'm back to figuring out how to teach a subject that can't be taught without English to students who don't know English.

I don't want to complain. PCVs are usually starving for something to do. When it comes down to it, being busy is a good thing. It keeps you focused, keeps you active, and you appreciate your time off that much more when you get it. So while I'm whining right now, I'll feel much better when I actually get some stuff done.

Come to think of it, even in the midst of the chaos at school this morning, I managed to perform two cool experiments with a group of students sitting outside doing nothing. We made a water drop microscope out of some clothespins and plastic trash, and I demonstrated the concept of static electricity with the plastic covers from a bunch of old Peace Corps training manuals in my house. That felt pretty good, considering it was all stuff we developed at our workshop last month.

Life is stressful right now, but it's good :)
