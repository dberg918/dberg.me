---
layout: post
title: "merry christmas: a new gait"
date:   2009-12-22 12:00:00 +0300
---

I suppose now would be a good time for an update on my status.

Last week, I was sick again, this time with a bacterial infection. Needless to say, it wasn't a fun three days. The bout began on Monday evening, when I was walking home from town and felt quite warm. I was coming down with a fever and feeling very weak, to the point where I nearly collapsed when I got to my door. After sleeping for a few hours on the couch in my living room, I had my first spell of diarrhea, an omen of the days to come. The issues continued throughout the night, to the point where I was squatting for longer than I should have.

I'm still not positive if it was the squatting that did it, or the awkward sleeping position I was in for long periods of time, but I managed to create problems for the peroneal nerve in my left leg. I noticed this the next morning when I was walking kind of funny, much like my stroke-addled uncle. I couldn't flex my left foot upward, causing my toe to drag across the floor unless I lifted at the knee enough to clear the foot off the ground.

Once I overcame the sickness (not until a stool sample was delivered and antibiotics were administered two days later), I talked to the PCMO about my foot and its condition. She told me to monitor it for the next few days, and if it didn't improve it would require a trip into Dar for evaluation.

Sure enough, the foot didn't improve, and here I am in Dar at the Peace Corps Office typing out this blog entry. I just had the evaluation this morning, quite short as I expected. I've been diagnosed with something cutely named "foot drop," which is a nickname for paralysis of the peroneal nerve in the leg. Parts of my foot and leg are numb because of it. A report has been sent to the DC hq, and they will make the decision on what the next course of action is. Medevac is looking very likely. I can either be sent to the US or South Africa for treatment. Unfortunately, DC is in the midst of a snowstorm, and all of the doctors in SA are gone for holiday, so the medevac may not occur for a week or two. In all likelyhood, I'll be flown to SA in early January to begin treatment/therapy.

The PCMO told me a volunteer in Western Africa had a very similar issue, and was medically separated because he went over the allotted 45 days for treatment and recovery. He appealed and was reinstated, however, so even if med sep occurs, it isn't the end of the road. I've been informed that the staff here at PC/Tanzania would have no problem welcoming me back if I do get med sep'd because of this, which is comforting to know.

I know this is probably crazy for you guys to hear, but it's the reality I'm facing at the moment. I had no idea this foot numbness was going to be such a huge issue. But to be perfectly honest, if there's anything that's going to get me medevac'd to SA, this is a pretty sweet deal. I'm not experiencing any kind of pain at all. It's the opposite of pain, in fact, it's numbness! It isn't the most ideal way to travel, but I am excited by the thought of getting to see a little bit of South Africa, and getting to fly again!

Okay, now back to the somber thoughts...
