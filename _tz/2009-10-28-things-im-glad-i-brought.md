---
title: "things i'm glad i brought"
date:   2009-10-28 13:00:00 +0300
layout: post
---

**My Headlamp**  
This thing has seen some serious use in the past few weeks. I would be quite lost without it. Well, not necessarily, but it has contributed leaps and bounds to not only my own personal comfort, but also to my host family. Even in a homestay that has electricity like mine, a headlamp can be used every day. I use it at night when I turn out the lights in my room so I can properly tuck my mosquito net under my bed. When the electricity goes out in the evening (which is usually 2 or 3 times a week), it's usefulness quadruples. It's hands-free, so it's easy to use while cooking food, carrying things, washing pots, or just general business at night (choo visits).

**Baby Wipes**  
Tanzanians don't use toilet paper like Americans do (they use water and their left hand), although it is sold in many dukas (shops) in my area. Unfortunately, it's not the Charmin Ultra quintuple-ply super soft paper that my bottom is used to, so this can cause problems. Luckily, my parents thought to have me pack some baby wipes, and boy am I happy I did. I'm actually so happy I did, I'm having my parents send me more.
