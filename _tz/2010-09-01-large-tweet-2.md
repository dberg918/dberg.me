---
layout: post
title: "large tweet #2"
date:   2010-09-01 12:00:00 +0300
published: false
---

There's been a flurry of activity upstairs the past few days.

About things past, things happening now, upcoming things, things to plan, and things that could be. Certainly a lot of "things" going on. Most of my attention has been drawn to an upcoming collaborative project, a girl's conference with a few other volunteers. I'm co-hosting a 4-day fun-festival for girls in Singida town in a few weeks, but I think I'll hold off on the details for now, as I'd like to devote an entry to it later when all these "things" are a little more fleshed out.

In recreational news, I just returned from a trip to Dodoma. A large gathering of volunteers occurred on Saturday, and given I'm only a 2-hour dala ride on a tarmac road, I was more than happy to make an appearance. Hot showers, authentic Italian cuisine, and good American company are always a welcome distraction from the usual drone of rice'n'beans, dust-ridden computers, and a constant battle within my house and within myself to stay clean. Some days you win, some days you lose :)

And in health news, I was sick for about 4 days last week. Not a huge concern, though it was a bacterial digestive issue like the one that was indirectly responsible for sending me to South Africa. Strangely, things cleared up by Saturday for my Dodoma trip (note a hint of sarcasm, I still had a minor stomach ache Saturday morning).

[comment]: # (What's been happening in America? I've been on Facebook this past week, but my friends were unusually soft-spoken about current events. I did hear about Glenn Becke's "non-political" stunt in D.C. You'll have to pardon my French, but what a douche. Way to douche up a sacred day of America's treasured past. Man, I need some Bri-Wi in my life...)
