---
layout: post
title: "umoja wanawake"
date:   2011-05-08 12:00:00 +0300
published: false
---

Happy Mother's Day :)

<a href="http://3.bp.blogspot.com/-_8u18GkDJeA/TcajIUFcSdI/AAAAAAAAARw/wVUSpybKsUs/s1600/CIMG5147.JPG" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="265" src="http://3.bp.blogspot.com/-_8u18GkDJeA/TcajIUFcSdI/AAAAAAAAARw/wVUSpybKsUs/s400/CIMG5147.JPG" width="400" /></a>

Though I'm unable to deliver flowers physically, I thought I'd gather what flowers I could (not many considering this is the desert) and "deliver" them this way. They're pretty interesting, especially the spiny and pokey orange one. Anybody want to find out what these actually are?

<a href="http://1.bp.blogspot.com/-xFM-V40D6AU/TcajVQKvxWI/AAAAAAAAAR0/RXTw-CcfHoM/s1600/CIMG5150.JPG" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="242" src="http://1.bp.blogspot.com/-xFM-V40D6AU/TcajVQKvxWI/AAAAAAAAAR0/RXTw-CcfHoM/s400/CIMG5150.JPG" width="400" /></a>
