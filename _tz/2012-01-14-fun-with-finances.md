---
layout: post
title: "fun with finances"
date:   2012-01-14 12:00:00 +0300
---

In perhaps the surest sign of madness since I've started preparations for a monthly budget, I have been doing a fairly extensive analysis on food prices in the *soko* (market) in town. Shown below is a sample of the table I created with the data I collected this past Thursday, just before I bought my new oven (quite a successful day, I admit).

[comment]: # (You can get the whole table <a href="http://pctanzania.org/sites/default/files/food-prices.pdf">here</a>.)

<a href="http://3.bp.blogspot.com/-WcdyMQ26Oqk/TxFYtnZZWCI/AAAAAAAAAcs/4trMgUUWflo/s1600/food-prices.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="306" src="http://3.bp.blogspot.com/-WcdyMQ26Oqk/TxFYtnZZWCI/AAAAAAAAAcs/4trMgUUWflo/s400/food-prices.png" width="480" /></a>

Yeah, maybe I went a little overboard. Questions of sanity aside, this will certainly help me out in the long run. Shopping lists should be much easier to write up, and I'll be able to calculate almost precisely how much money to bring with me, which cuts down on impulse spending and keeps me from carrying unnecessarily large sums for no reason (something to avoid in places like the market, with its narrow, crowded passages).

This is also going to be tremendously helpful next month, which marks Peace Corps/Tanzania's annual Volunteer Allowance Survey. Every February, PCVs are asked to fill out an Excel Spreadsheet detailing their spending for the month, including minuscule things like how much a tomato costs locally and how many were bought throughout the month. While it isn't mandatory, the data collected is the sole determining factor for whether or not our pay gets raised (or in certain rare circumstances, lowered), and HQ needs at least a 70-75% response rate in order to approve it. Ask any Volunteer if they could use a pay raise, and the answer is an unequivocal yes.

While I'd readily admit a raise wasn't necessary living in Singida, prices are quite different down south. The data I now have further proves it. I have no doubt that I can make it work and even save up a bit in the process, but it will take considerably more planning and discretion with regard to spending.
