---
title: "preparing for the journey: part iii"
date: 2009-09-08 12:00:00 -0500
layout: post
---

Major success this Saturday. I now have a good chunk of the critical items for my trip! We drove up to the REI stores in Cary and Raleigh and did a bit of a shopping spree. Things that got checked off the list:
 - internal frame backpack
 - underwear
 - socks
 - sandals
 - compass
 - whistle
 - aLOKSAK bags
 - Steripen (water purifier)
 - nalgene
 - headlamp
 - undershirts

Not a complete list of what I got, but these were some of the things I was looking to get.

So first off, we got the backpack I had been researching: the Gregory Baltoro 70L. It's gotten rave reviews and won at least one award last year. I'm hoping it will be able to hold everything that isn't going in my carry-on, which would make things at the airport a little less stressful. As for the underwear, I've decided to do something a little crazy. The Peace Corps recommends bringing a two-year supply of underwear, which upon first hearing it, is quite vague and daunting to figure out. Instead of packing tons of cotton underwear, I have decided to put Ex Officio's specialized nylon underwear to the test. I'm only taking three pairs. Yes, it sounds ridiculous. How could three pairs of underwear last me two years in Africa? I read reviews, and read recommendations from seasoned travellers. They say synthetic fabrics are the way to go. It wicks moisture, it dries quickly, and it's durable. Three essentials for your essentials in a foreign (and hot) land. This is also why I opted for two polyester undershirts and just three pairs of Coolmax polyester socks (two pair quarter length gray and one crew length dark blue). Granted, I'm not sure how I'll feel about laundering undergarments every other day or so, but I think I will appreciate the saved space, not to mention it greatly simplifies things.

I also went out to Wal-Mart today to get some of the other, not so specialized, items on the list. On top of that, I put in an order with an online retailer for some travel accessories (inflatable hangers, clothesline, packets of laundry detergent). At this point, there aren't many things left on the list. It's getting close to sorting time, where I'll figure out what's going in which bag. Ironically, my current plan is to try and pack as many of the essentials into my carry-on as I can. Sounds counter-intuitive, but I'd like to keep the important things as close to my person as possible. In the event that the checked luggage gets lost, having the essential things (undergarments, Steripen, first aid kit...) will ensure the least amount of discomfort in such a situation.

The countdown continues: departure is just two short weeks away!
