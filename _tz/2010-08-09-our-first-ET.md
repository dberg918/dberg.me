---
layout: post
title: "our first ET"
date:   2010-08-09 12:00:00 +0300
published: false
---

I'm saddened to announce that last Friday, our September 2009 intake suffered its first ET after swear-in. A good friend of mine and most everyone else in our group, Zach from the Manyara region decided to call it quits to pursue an opportunity back in America.

Good luck to you, friend, if you're reading this! His blogged was linked below, but I've already replaced it with another, a Health volunteer from our very own Sin City (nickname for Singida region)! Her name is Cessie, and I just happened to spend a weekend in Dodoma with her (it was a holiday this past Sunday). Check out her blog below and be sure to comment on how adorable her dog Nyika is.

You can also check out some vids I took at the holiday celebration in Dodoma. It's called "nanenane," "nane" being the Kiswahili word for the number eight. Thus, it means "eight-eight," or August 8th. July 7th is also a holiday here, "sabasaba." Just like Cinco de Mayo in Mexico :)
