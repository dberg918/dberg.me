---
layout: post
title: "shadow: part 3"
date:   2009-12-06 14:00:00 +0300
---

It is now Thursday morning, and Charlotte and I have a bus to catch down the mountain. Naturally, we decided to return the way we came, which, as I had predicted for myself, was worse than the trip up. By the time we got to the road, our legs were shaking nervously at every step, and we simply hoped they would function long enough to get us back to the bus stop. Now allow me to return to our pineapple.

Our poor pineapple had to survive a trip up the mountain in our first hostess' bag and a trip down in our hands, which it did graciously enough. After descending, Charlotte and I got the idea to make pineapple upside-down cake for our hostess. We weren't sure if she had some of the ingredients, so after calling our friend Owen for the list, we made our way through the soko (market) searching for the essentials. While most people can speak Kiswahili in Tanzania, locals sometimes prefer to use the native tribal language. This is the case where we were, which created yet another language barrier. Luckily, Kiswahili is the fallback language for most, and some even knew a little English.

After picking up some cake ingredients, we headed to the bus stop to wait for our ride. As we waited, we spoke some Kiswahili to some locals waiting with us, and one of them bought us bananas! Certainly a welcome treat during our wait. Finally our bus arrived, with "Picnic Class" painted across the front. Not sure what that means. It was about a 45 minute drive into Lushoto, which was fairly uneventful. The next 3 hours, however, could be described in a number of different ways. After Lushoto, the road we rode upon was no longer paved, and it winded around the Usambara mountains, occasionally dipping into the valley to pass through small towns. Needless to say, it wasn't the most comfortable 3 hours I've spent on a bus, especially considering the complimentary vomit bags that are supplied to passengers. Luckily for me, I was so transfixed on the vistas outside my window, I didn't notice all the people throwing up around me.

We arrived safely at our destination in the evening, stomachs intact for the most part, and met our hostess right off the bus. After a quick tour of the school and the surrounding area, we cooked dinner and the most delicious cake I've ever taken part in making, which brings be back to the pineapple.

Our pineapple, which survived the grueling 3 hour trip out to our shadow site, was made into one of the greatest cakes I've ever eaten. We only needed a few slices for our upside-down cake, and the rest went into a bowl that was snacked upon for the next two days. Not only was it our dessert, but it was also a bedtime treat and breakfast the next morning! I know it's silly that I'm talking about this pineapple so much, but it was really that good.

In part 4 of "shadow," I will describe our Friday morning question session with our hostess' students and our afternoon hike to one of the most beautiful places I've seen in my life.
