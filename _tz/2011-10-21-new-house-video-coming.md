---
layout: post
title: "new house, video coming"
date:   2011-10-21 12:00:00 +0300
---

Lots of things have happened since my last post. And unfortunately, lots of other things *haven't* happened.

One thing that has happened is that I've paid a visit to my future house! Super exciting! Of course, given that I'm referring to it as my "future house," I'm implying that it isn't my current house. No. I'm still a nomad. And I'm closing in on two months since my departure from Singida. Yeah. It's getting a little old living out of a duffel bag. I refuse to unpack anything, so I've been stuck wearing the same work clothes since the beginning of September. It'll be nice to wear a short-sleeve collared shirt after this is all over.

Anyway, the real reason I'm posting is because I managed to splice a video together showing off the new digs! I don't know how I managed it, as I've long ago lost my patience for video editing on a netbook. But I did it, and it's ready for upload. If the internet in our satellite office down here in the deep 'n dirty south ever decides to cooperate long enough for me to get it posted, I will send out e-blasts in all the normal outlets (Facebook, Twitter, carrier pigeons, etc.) to let you know it's ready.

[comment]: # (Oh, and I heard Gadhafi got murdered. 'Bout damn time that bastard got what was coming to him.)
