---
layout: post
title: "blogger refresh"
date:   2012-11-12 12:00:00 +0300
published: false
---

Admittedly, I've been more engaged in reading blogs than writing them recently. The only word I can think to describe the feeling I get when I read about the adventures of others, the things I experienced early in my service...I feel refreshed. Which is fitting, because part of that word, "fresh", is what it felt like when I was in it myself. It's precisely the feeling I wanted to convey in the name of my own blog.

I'm very proud of our new group, and I'm thankful that many of them are taking their time to share their struggles and joys with the rest of us! If you'd like to get in on the action, check out the blogroll on <a href="http://pctanzania.org/">pctanzania.org</a>. All the blogs in my sidebar might be a little outdated (although some RPCVs that are home now are still posting!), but I'll update them soon!
