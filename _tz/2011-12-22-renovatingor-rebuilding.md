---
layout: post
title: "renovating...or rebuilding?"
date:   2011-12-22 12:00:00 +0300
---

Repairs are underway on my house, and I've been pondering the above title since we started yesterday; some of the things we're doing (like hammering iron bars into the aging brick and mortar infrastructure) make me think there's something more substantial going on. Despite my doubts of its structural integrity, I'm quite confident the fixes we're making now will keep the house standing at least until I leave, if not for the duration of the TZ21 project (which is the ultimate goal). There's also nothing stopping the utter excitement I feel about beginning my second life in Peace Corps, in a brand new setting with new people, and (hopefully) with a fresh perspective. My house has been a big part of this so far, acting as the gateway at which this new life starts.

<iframe allowfullscreen="" frameborder="0" height="274" src="https://www.youtube.com/embed/OlpSlhdL9Sc" width="480"></iframe>

The video above is just a sampling of the various things that have happened over the past two days. In it, you can see the roof being patched and painted, the interior wood frame being coated with used motor oil (natural termite repellent, apparently), cracks in the concrete being fixed inside and outside, ceiling boards finished in the living area, etc. In addition (not in the video), mosquito nets are in the windows, the electricity has been rewired, and the light fixtures are working as of this evening!

I'm pretty amazed at how fast things are getting done, but there's still plenty left to do. More ceiling boards, more concrete work, stuff in the bathroom and the kitchen, the other side of the roof needs to be painted, among other things. I honestly think moving in by Christmas is a real possibility; as long as I get a bed with a mosquito net in my bedroom, locks on the doors, and we make sure the *choo* is functional, I would consider it safe to spend the night inside.

A house would be a nice Christmas present, I have to say.

[comment]: # (But I've already got a plan for the Christmas present I want to give to myself. It has something to do with my living area. I'll share more details in another entry in relatively short order, just be patient!)
