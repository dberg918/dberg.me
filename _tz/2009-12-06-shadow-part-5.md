---
layout: post
title: "shadow: part 5"
date:   2009-12-06 16:00:00 +0300
---

You would think I would be tired at 4 in the morning, but I went to bed at about 8:30, so I felt fresh and energized, ready for the day. This turned out to be a good thing because we had some hiking planned for Saturday, how much hiking exactly, Charlotte and I did not fully understand.

Our first charge was to survive the bus ride back through the mountains. While the first bus ride for me was a pleasant experience, the return trip was a slightly different story. It's hard to be blissfully ignorant of the people around you when an old lady sitting in the row in front of you is making loud moaning noises, hanging her head outside the window like a seasick sailor. This was about 20 minutes into our 3 hour trip, so I wasn't holding out much hope for my own stomach's well-being. But then a stroke of luck! She got off the bus after her little episode. Of course, that just leaves an open seat for the bus's next victim, at least so I thought. But in reality it wasn't the seat in front of me that would host the next bag-holder. It was the seat immediately to my left.

An older man in some kind of track jacket was sitting with his head between his legs. The other half of the time he spent leaning, pushing all of his body weight into my left shoulder, squishing me into my shadow friend on my right. Thankfully, he never actually vomited, but he threatened to on a number of occasions.

After 3 hours graciously passed, we deboarded a few miles north of Lushoto so that we could walk to the house of a volunteer couple living in the area. The plan was to hike from their house all the way to the volunteer we spent the night with on Wednesday. The walk to the couple was about 5k, a very scenic 5k. But the hike to the other site near Lushoto was about 14 or 15k. Not exactly your average stroll through the mountains. When we arrived in town, we picked up some ingredients for our main course that night; pizza! Our volunteer friend that was playing host had a brick oven, so we were going to attempt homemade brick oven pizzas. After buying the veggies and crust ingredients, we split up into two groups. One group took a taxi up the mountain to save their legs, and to carry all the luggage. The rest of us (only three of us including me), made the trek up the mountain by foot. Yes, after hiking 19k in the morning, I decided to repeat the mountain climb I did just a few days before. This time, I scaled it in about half the time, bouncing up the steepest parts with relative ease.

And as luck would have it, we arrived just as the rest of the group arrived with their escort. We unpacked and got started right away on the pizzas, since it was the evening by this point. There were about 14 of us altogether, including 6 trainees. Multiply by cooking time for pizzas, and it was readily obvious it might be a while before some people got to eat. Everyone did eventually get a pizza, though most of them turned out doughy. The oven just wasn't hot enough. The bread we baked after the pizzas did very well however, and we ate that for breakfast the next morning.

It was a great party overall, given all the different levels of experiences represented in the people there. Some were on their way out, others in their second year of service, and the 6 of us staring down the assignments we would be taking up in the coming weeks.

That night, 14 of us slept cramped in a house built for no more than 4, splayed out on the concrete floor in the living room. It looked like a jigsaw puzzle the way everyone's legs tangled together, but we all fit...barely. We awoke the next day stiff and sleep deprived, at least those of us who haven't trained ourselves in the art of claustrophobic communal resting on concrete floors. A few, not including me, announced to the rest of us how well they slept.

In the last installment of "shadow" are details of our pilgramage to Lushoto town proper and the last day of our shadowing experience.
