---
layout: post
title: "shika project: conference over"
date:   2011-03-28 12:00:00 +0300
---

The conference is no more. We finished on Friday. Sorry I didn't share much in the past couple of weeks.

I was hoping to update during the conference, but I greatly underestimated how much work I would be responsible for. Turns out I played a critical role in many aspects of the conference, something that would've been hard to plan for even if I had known in advance. We all ended up with a lot more work than we bargained for, but I think everyone took it in stride and coped quite well given the circumstances.

Our goal for the conference was to output three teacher's manuals; one for each science subject in O-level secondary school (Bio, Chemistry and Physics). These manuals would contain information on setting up a laboratory in a school without one, with the main focus being on science activities that could be performed with locally available materials. Each subject had a team of about 9 teachers, and our over-arching goal was to parse the entire syllabus in each grade level, find the practicals (experiments) students need to know how to perform, and design activities for all of them. The grand total came to about 400 activities, so needless to say everyone stayed busy.

We somehow managed to get activities for every single practical, so the conference was most certainly a huge success. We're still burning the midnight oil to prepare the manuals for the Ministry of Education, but we're hoping to finish them in the next week or two.

The other output that's still pending is a DVD of step-by-step instructions on how to perform some of the more difficult practicals. This has basically been given to me as my own personal pet project. While I certainly didn't film every single activity at the conference, we did film over 80 experiments, and I have over 55 GB of raw footage sitting on my external hard drive. Unfortunately, we haven't really figured out a way to divide this work yet. It's all on me at this point, and I'm dreading the thought of attempting to put the DVD together by myself. It's also been put on the back-burner while we rush to put the books together. I get the feeling that I may not finish the DVD until the June/July break, and even then maybe not. I do have another job to do, after all.

In any case, we've all been pretty pleased with the outcome. We are hosting the LaTeX code of the books on our Google Code site for Shika na Mikono (which we recently learned is grammatically incorrect, it should be Shika KWA Mikono). If you have a TeX distribution installed on your machine, as well as Mercurial, you can pull the code and take a look. Otherwise, you can wait until we upload PDFs in the Downloads section.

Okay, my brain is now officially off. Can't type anymore. I'm going to Iringa on Tuesday to veg out for a bit, then I'll be heading back to site to see the end of mid-terms at school. Should be fun. Take care everyone :)
