---
layout: post
title: "reclaiming joy"
date:   2010-11-15 12:00:00 +0300
---

<a href="http://www.flickr.com/photos/23688234@N07/5177780259/" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;" title="For Amy's mom by dberg918, on Flickr"><img alt="For Amy's mom" height="180" src="http://farm2.static.flickr.com/1336/5177780259_6c9e628f12_m.jpg" width="240" /></a>

Despite not receiving any of my own shadowers from the new intake of education volunteers, I took it upon myself to host all of the shadows that came out to the Singida region at the end of last week. So, on Friday afternoon, 7 people de-boarded in my town and took their places in my house. I have a fairly large house by Peace Corps standards, but 7 extra people will almost always require some amount of adjustment.

While they were here, I was lucky enough to introduce them to some of my best friends; one being the guy who runs the pork shop near my house (Wilbroad), and the other being my friend in town who owns one of the local bus lines (Slim). We had a huge pork dinner with Wilbroad on Friday night, and then walked into town to have lunch with Slim on Saturday. We also had the opportunity to climb up on the rocks every evening to watch the sunset, something I hadn't done in nearly a month.

<a href="http://www.flickr.com/photos/23688234@N07/5177780273/" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;" title="Family photo by dberg918, on Flickr"><img alt="Family photo" height="180" src="http://farm2.static.flickr.com/1242/5177780273_3bef533215_m.jpg" width="240" /></a>

One of the things I truly appreciate about spending time with newbies and visitors is that they make it easy to fall in love all over again with the place you call home. Sometimes it takes an outside perspective to help you reclaim that gratitude you felt at the beginning of your service.
