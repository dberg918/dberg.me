---
layout: post
title: "3rd year perspectives"
date:   2011-12-24 12:00:00 +0300
---

Last night, it finally hit me that it's Christmas. It was right around the time I was finishing up a beer in the center of the bus stand in my new town, when I felt the absence of my family. But I realize I've been pretty lucky to be so preoccupied with something else. I don't think it could get much worse than being alone with nothing to do over the holidays. Having a new house to fix up, and organize, and decorate, and be excited about has been a wonderful Christmas present for me.

After feeling a little sad, I started to think about the past 2 holiday seasons. How they've been different and how they've been similar. On the first Christmas I ever spent away from family, I was contemplating my impending medical evacuation to South Africa, due to my being [diagnosed with foot drop][foot]. I was also burdening my counterpart, spending the entire afternoon in his room eating his food and watching war movies (it's the only American movies Tanzanians really enjoy, since the dialogue isn't really important). It sounds really really sad, but that is a fond memory of my early service. I didn't know the teachers at my school very well at that point, but this guy took me under his wing after we hung out, just one afternoon, drinking beers at the local *kitimoto* shop. At the time, December 25th was one of the better days compared to the others that surrounded it, and in hindsight, it will probably be one of the highlights of my life. Not necessarily because I enjoyed it in the moment, but because it was a showcase of God's grace and love for me when I felt I was descending into a valley.

Last year was the Christmas that provides the contrast to the two that book-end it. Thanks to the generosity of my parents and grandparents, I got to fly home for the holidays. And it was different from every other Christmas I'd had in America before because I had spent a year living with the other 3/4 of the world (and was going back after two short weeks). I didn't feel guilty; I didn't preach at my family or friends, that they should think harder about how much they take for granted; on the contrary, [I was having trouble registering][dream] that the past year had actually happened. It was a strange feeling, but I had a new appreciation not just for the things we have in America, but for the people that were a part of my life (Peace Corps Volunteers/Staff and Tanzanians included).

And now, another Christmas is upon us. My contract with the Peace Corps ended before Thanksgiving, but I decided to extend for a year so I'm still here. I could have flown home for Christmas, on the Peace Corps' dime this time, but I've decided to take my leave in March so I'm still here. While it's certainly difficult to be away for the holidays, it's easy to see that God has His hand in how it's all working out.

Had I gone back to the States, I would've had to come back still homeless. The work on my house probably wouldn't have started until mid or late January, getting dangerously close to when we are scheduled to host our "Project Launch" down south (middle of February), featuring honored guests such as the United States Ambassador to Tanzania and the President of Tanzania himself, Jakaya Mrisho Kikwete. In all fairness, my living situation would've been low on the list of priorities.

Had I gone back to the States, I would've missed out on yet another Baseball season (you never know how much you miss something until you go 2 years without it), probably leading me to ET (Early Termination of Service) to avoid watching more incomprehensible Cricket matches. Spring Training can't get here soon enough!

So to my family and friends back in the States, if you're reading these words, know that you're in my thoughts, that I love you all, and that I wish I could be there with you to celebrate the holidays.

*Heri ya Krismas, Mungu Awabariki, Allah Akhbar, na tutaonana hivi karibuni!*

[foot]: /tz/2009-12-22-merry-christmas-a-new-gait
[dream]: /tz/2011-01-01-living-in-a-dream
