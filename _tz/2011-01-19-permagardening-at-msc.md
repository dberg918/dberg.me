---
layout: post
title: "permagardening at msc"
date:   2011-01-19 12:00:00 +0300
---

To prevent dying of sheer boredom at MSC, I shot a short video about our permagardening session on the last day. There's really not much to see; this is more for my own purposes. After improving to an HD video camera during my stay at home, I realize there are other ways to improve my videos besides picture quality. Hopefully they'll become less one-dimensional (montages set to music) in the coming months. Please try to enjoy this first step in that direction :)

<div style="text-align: center;"><iframe frameborder="0" height="270" src="https://player.vimeo.com/video/18915162?byline=0&amp;portrait=0" width="480"></iframe></div>
