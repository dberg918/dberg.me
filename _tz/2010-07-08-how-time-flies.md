---
layout: post
title: "how time flies..."
date:   2010-07-08 12:00:00 +0300
published: false
---

...when you're doing a new thing. Like jumping 20-foot waterfalls or biking 80km of the Zambian countryside.

I'm back from my Zambia excursion already (was that seriously 8 days???) and I just posted some pics on Flickr. I did a pretty horrendous job of documenting my trip, so I apologize for the shortage of media. Expect a couple of short videos to surface here soon, though. I got some good footage of Ashley's favorite kid in her village, and your heart will melt when you see her!

I'll try to post a more elaborate synopsis of the trip in the near future. There's a funny ending that is worthy of being dubbed a "wtf moment."
