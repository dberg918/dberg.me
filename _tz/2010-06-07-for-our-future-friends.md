---
layout: post
title: "for our future friends"
date:   2010-06-07 12:00:00 +0300
---

For those of you that are staging in approximately 7 days (and for any other Future PCV who wants to look at a packing list), I thought I'd post the packing list I used when I left country.

This isn't a list of suggestions, this is a list of the stuff I *actually brought.* Use it however you'd like.

*Last updated: January 3, 2012*

```
..::PACKING LIST FOR PEACE CORPS SERVICE IN TANZANIA::..
                      -- draft #5 --

::CLOTHING::
1 fleece jacket       
1 rain jacket         
3 pair khakis         
1 pair shorts         
1 pair wind pants
1 swimsuit            
4 button-down shirts  
2 white t-shirts      
2 colored t-shirts    
4 pair socks          
3 pair underwear      (your mileage may vary...)
1 hat                 
2 belts               
2 pair glasses/cases
1 pair sunglasses

::SHOES::
1 pair boat shoes    (boat shoes rock, bring a pair)
1 pair sandals       
1 pair sneakers      

::TRAVEL/CAMPING EQUIPMENT::
1 multi-purpose tool
1 travel clock
1 watch/batteries       
1 compass            
1 whistle            
1 travel pillow      
1 set earplugs
1 neck pouch         
1 sleeping bag
1 mosquito net       (a net you can drop over a hat, not necessary)
- Swahili dict.
1 headlamp

::ENTERTAINMENT::
1 netbook/adapter            
1 power converter    
14 AA batteries      
8 AAA batteries      
1 battery charger    
1 mp3 player/cable
- portable speakers
- spare USB cable
- jack splitter
- headphones
- headphone buds
1 bag emb. floss     
1 hackey sack        
- guitar/strings     
1 digital camera     
1 deck of cards      
2 USB drives         
2 SD cards
- Bible
- devotion

::KITCHEN ITEMS::
- aLoksak bags       (awesome for keeping stuff dry)
1 Cutco knife  (if you're lucky enough to own one of these, bring it)
- hand sanitizer     
- safety pins        
- rubber bands       
1 roll duct tape     
1 spork

::HYGEINE::
- deodorant          
4 toothbrushes       
1 tube toothpaste   
- razor/blades       
- shaving cream      
- nail clippers      
1 sink stopper       
- Woolite            
- spot remover       
1 Flexoline          
2 inflatable hangers
- toilet paper       
- squirt bottle      
- wet wipes          
- Steripen           
- Pepto Bismol    (not necessary, it's in your med kit)
- bug spray       (also in your med kit)
- first aid kit   (again, up to you)
1 shamwow      
- lens cloth         
- dental floss    (in your med kit, PC/TZ stocks good stuff now!)
- Claritin-D       

::MISC::
1 keylite          
1 set of sheets      
1 nalgene            
1 notebook & pen    
1 small calculator
1 pair binoculars  
1 bike helmet
- photos of family +
- U.S. stamps      +
- assorted manuals   

::AIRPORT ESSENTIALS::
- passport              =
- visa                  =
- extra passport photos
- driver's license      
- plane tickets         =
- US bank card      
- vaccine history
- wallet                =
```

I highly recommend the Steripen; I had forgotten all about it when I went to site since my host family boiled water for me during training, but I use it all the time now, and it's always in my bag when I travel around country. It's awesome to fill up your Nalgene from a tap, drop in the light-saber, and be drinking free water in 90 seconds!

Check out [OneBag.com][onebag]. Good tips for what luggage to use, and how to pack your stuff. Organization goes a long way! I recommend packing early and carrying around your stuff like you would in the airport. I enjoyed watching everyone else struggle with their 2 huge rolling bags and giant duffels, while I glided through the airport with my Gregory Baltoro and L.L. Bean backpack. I would also emphasize the point OneBag.com makes about carrying your essentials in your carry-on; don't leave things up to chance by putting important items in your checked luggage. Our training class was lucky to end up with all of our bags in Dar when we arrived, you may not be so lucky. Things to consider important: underwear, any technology (laptop, Steripen, etc), batteries (esp. AAA), rain jacket, most hygiene stuff...and whatever you think you can't live without for the next two years.

Also keep in mind you can have things sent from home; I've had pretty good luck with the postal service here, although the rule of thumb seems to be the bigger/heavier the box, the longer it takes to arrive. My parents like to send the soft padded envelopes you can find in local USPS branches, and they usually arrive within two or three weeks. Postal service in Tanzania varies greatly by region. If you're lucky enough to be posted in Singida (where I was '09-'11), the service there is fantastic! Padded envelopes usually arrived within 2-3 weeks. I've heard horror stories about Dodoma's post office, but don't know much beyond that. Mtwara town's posta seems pretty good; I've already received two packages there, and both took about 2 weeks to arrive. I had to retrieve the card from the PO Box in order to get my packages however. If your small town (aka NOT a regional center like Dodoma or Singida) has a post office, befriend the office staff and they will usually just hold your packages for you and not require the slip. This is much more convenient, especially if you are an Ed volunteer and use your school's box instead of renting your own (you won't have to retrieve the key from the person who holds onto it).

Enjoy your last few days in the States, and karibuni sana Tanzania!

[onebag]: http://www.onebag.com
