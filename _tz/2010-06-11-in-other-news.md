---
layout: post
title: "in other news..."
date:   2010-06-11 12:00:00 +0300
---

Don't have any profound thoughts to share, but I thought I'd share some of the recent goings on of the week.

The World Cup starts today, for those of you completely oblivious in America. We've been buzzing with excitement here for the past 2 weeks or so, and a couple of my PCV friends are making their way down soon to catch a few games. It's actually kind of sad how little the US cares about it, considering it's the biggest international sporting competition, save the Olympics. And just for the record, World Cup &gt; Super Bowl (my opinion, of course).

Last night, I didn't have power in my house. Not because the electricity was out, but because Tanesco (the power company here) came and actually cut it. I've been Zima'd! And not the weird carbonated alcohol beverage kind. "Zima" in Kiswahili means to switch or turn off. My electricity situation is a little weird; there's 3 of us that share a bill, rather than having each of our own. You might imagine how this can lead to problems. Anyway, some other guy who happened to be on our bill hasn't paid his share, so now we're all paying the price. Every American bone in me wants to rail into this mystery man, but I realize this is a cultural thing (still trying to understand why the socialism has to extend to power bills, though). When one person has a problem, it's everyone's problem. Well, this guy's problem is 189,000 schillings, so I have to pay an extra 21,000 for the next 3 months to clear the discrepancy. F me, I guess.

One of our new Form V girls came into the computer lab today to type up a farewell poem, announcing her departure from Mwanzi. Our conversation went something like this:

> Me: Why are you leaving?
> Student: I don't like it here.
> Me: Is it because of me?
> Student: Haha, of course not.
> Me: Is it too cold here?
> Student: No, not that...
> Me: Is it too dusty?
> Student: Yes, this is the bush.
> Me: LOLZ

The fact that I can last longer here than a Tanzanian is a giant ego boost.

[comment]: # (And so, to end this post, I thought I'd go ahead and post that first three months comic I promised like...well, it was three months ago, ironically.)

[comment]: # ([![3 Months][comic]][comic])

[comment]: # (Gotta end on a good note, right?)

[comic]: http://lh5.ggpht.com/_WM5vLMqkVOk/TBInLIOqVPI/AAAAAAAAALA/pcp-ZcJhv4I/comic.png
