---
layout: post
title: "religion is crippling tanzania"
date:   2011-07-16 12:00:00 +0300
---

Forgive me for engaging in silly hyperbole, but to some extent I do believe this is occurring in Tanzania. I think even those of use that are Christians, if we observe the situation long enough, notice that it is inhibiting progress. Progress of what, exactly?

Well, as an educator myself, I make the argument that religion is inhibiting intellectual progress. It isn't difficult to see; at my school, we have 2 class periods (80 minutes) on our weekly schedule set aside for "Religion." We also get out 80 minutes early on Fridays for those that practice Islam. Four class periods out of the weekly 40-some doesn't seem like that much of a sacrifice, but I think it does do damage when students are trying to figure out how the two, education and religion, should coincide and fit together. I like the way C.S. Lewis puts it:

> Nowadays most people hardly think of Prudence as one of the "virtues." In fact, because Christ said we could only get into His world by being like children, many Christians have the idea that, provided you are "good," it does not matter being a fool. But that is a misunderstanding. In the first place, most children show plenty of "prudence" about doing the things they are really interested in, and think them out quite sensibly. In the second place, as St. Paul points out, Christ never meant that we were to remain children in intelligence: on the contrary, He told us to be not only "as harmless as doves," but also "as wise as serpents." He wants a child's heart, but a grown-up's head. (Mere Christianity, "The Cardinal Virtues")

In other words, "religion" is not an excuse for being uneducated. In Christianity, it is quite the opposite.

At this time I should point out that this misunderstanding generally stems from the Christian population, not the Muslim, and it is usually Tanzanians of African descent rather than those that immigrated here from the Middle East (there is a large Arab presence in my town, mostly from Oman). Education is not valued as highly as the amount of time you spend in the church. And they spend A LOT of time in church. Practically the entire weekend is devoted to it. I can hear church music quietly and rhythmically thumping through my window as I type.

I can give many more examples of how much Tanzanians value being in church. All last week, there was a "conference" in town that consisted of blaring music from loudspeakers (even though the power was out) and preachers yelling things for hours on end, seemingly without taking a breath. While my Swahili skills still aren't sharp enough to understand everything they say, I do understand the word "Hallelujah," and I'm pretty sure this is about 60-70% of what comes out of their mouth while they yell. I mean, what else can you say once you've hit Day 4?

Forgive me for sidetracking, but I have to note that this is one of my great aversions to "church" here in Tanzania. Yelling is not preaching, nor is it a sign you are "on fire" for Jesus, at least to me. I understand if you want to show your passion and devotion, but if you can't for 10 minutes calm yourself enough to deliver a message I can actually understand, then I'm just going to find another way to connect with God.

Another example is the Easter Conference we hosted. At our school, as in on school grounds, right in the middle of our first term (we quite literally had to cancel classes). It also lasted an entire week, and regularly continued past 1AM each night. They also woke up around 4:30AM to start again. My question is, if you can wake up for church at 4:30AM, why can't you get to school before 8AM?

I don't think religion's impact is necessarily limited to just Education, but this is simply where I've noticed it the most; I am a teacher, after all. I'm also not implying that religion is single-handedly responsible for the state of Education in Tanzania; no, there are far worse problems in Tanzania's education system. I only wish to say that I can't help but notice the effect religion is having on Education.

One of the most frightening moments in my time here was discovering that the man who works in our school store believes the universe is geocentric (it's apparently in the Bible somewhere). This coming from a close friend of mine, who happens to be the husband of our second-master, albeit a man who constantly berates me and asks why I am not attending church.

If the church in my town truly endorses beliefs like these, I think I'll steer clear.
