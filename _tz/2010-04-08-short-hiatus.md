---
layout: post
title: "short hiatus"
date:   2010-04-08 12:00:00 +0300
published: false
---

My apologies for not posting an update last week when I had ample opportunity; I was in Dar for a few days with 24-hour wi-fi access. I did find time to upload the video I mentioned a few entries back, however. So check that out below (First Three Months). The comic will be forthcoming; unfortunately, the program I use to draw my comics broke when I ran a software update in Dar, so that set me a back a bit. Hopefully I'll have it up next week sometime (while I'm at IST, no less).

I will try to write up a few entries during my time at IST, perhaps a commentary on the events of the next two weeks. In the meantime, enjoy the video, leave some comments on it, and eat a block of cheese in my honor!
