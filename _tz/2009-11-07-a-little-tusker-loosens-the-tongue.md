---
title: "a little tusker loosens the tongue..."
date:   2009-11-07 12:00:00 +0300
layout: post
---

I went to a bar north of Kihonda after our CCT day today. I was hoping for a meal, since there was talk the previous day of going to the "American" style bar near the center after our activities. However, since we decided not to go there, we ended up somewhere new (to some of us), but with good friends nonetheless. I was in good spirits, having just received my second care package with plenty of goodies, and gone through a CCT day that included a session on how to maintain a computer lab. Nerd  alert!

Since I had just consumed a soda at CCT before we left, I decided to order a Tusker to balance out the sweetness. I don't know if I drank it too fast or what, but that one Tusker made me a little loopier than normal. Loopy enough that I felt the need to consume a bag or two of Goldfish from my handy care package. Thankfully I stabilized before we caught the daladala back to our village.

Still feeling the effects of the alcohol consumed 30 minutes prior, I walked into my house and greeted my mama in fluent Kiswahili. I then proceeded to use sentence structures I had only practiced in class quite flippantly in conversation. An interesting discovery. Tusker makes me better at Kiswahili. No wonder statistics show that PCVs say they drink more than before by COS. That's their secret to better language!

In no way do I really believe this, but I certainly do think the Tusker loosened the nervous grip of my own perfectionism. Just enough for me to let loose a few quick sentences that impressed even myself after they came out. And for the record, I just got through telling my wazazi (parents) about tomorrow's activities, only to find that the Tusker's magical abilities had worn off. I was back to my old crappy speech. Maybe one beer a day isn't such a bad idea, at least for the first few months while I continue to learn the language...
