---
layout: post
title: "next year's challenge: mwezi wa maskini"
date:   2010-11-26 12:00:00 +0300
---

[Almost half the world (3 billion people) live on less than $2.50 a day.][poverty]

I've been mulling it over the past few weeks, and I think I'm going to give this a try. For one month, I will attempt to live on $2 per day. I won't be able to do it until March, though. I'm coming back to the States for Christmas and New Years (won't be back at site until mid-January), and we fill out our annual living allowance survey in February. March is the earliest I'll be able to do it.

Why do it? Because you'd be surprised how easy it is to distance yourself from poverty, even when you're technically steeped in it yourself. Compared to my community, I live in a huge, secure house, I've managed to keep myself on a fairly balanced diet (at least since I started cooking last month), and even with my humble earnings, I find myself not wanting for anything. To be honest, I've grown a bit tired of being comfortable. Of knowing how my next meal is going to come, knowing that I can afford it, and knowing what's in my pocket can easily take care of my needs.

So, when the end of February rolls around, I will take a trip east to deposit a healthy chunk of my living allowance into my "savings" account, leaving ameasly93,000 =/ to keep me alive for a month.

The Month of the Destitute.

I encourage others to join me if they wish, even you all in the States! You don't have to actually live on $2.50 a day; try living on a healthy subset of what you're currently living on. If you want to use me as a guideline, I'll be living on approximately 40% of my current salary. That might change in 4 month's time, of course. I plan to use the current value of the US dollar to make the conversion into my local currency. As long as its value doesn't spike in the coming quarter, things will remain relatively the same.

Who's game?

[poverty]: https://www.globalissues.org/article/26/poverty-facts-and-stats
