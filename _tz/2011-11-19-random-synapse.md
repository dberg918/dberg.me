---
layout: post
title: "random synapse"
date:   2011-11-19 12:00:00 +0300
---

We just finished a workshop out in a village called Lukeledi. I don't know why, but I really like this place. From what I can tell, or from my very limited perspective of the area around the primary school, the village itself has nothing. It does have *korosho* (cashew nuts), but that's about it. Despite how impoverished it is, I still find myself wanting to stay here. Not forever, but at least a little while.

Why do I feel this way? Is it guilt, for being one of the lucky few born in the richest country on the planet? Is it the desire to help, and to improve the lives of the people here? Nope. I just wish my life was simpler. The people here are simple. They live simple lives. I'd like to think if I was boiled down to my essence, I'd find the same thing within me. But it isn't true. My life is complicated. Really complicated.

I suppose it's ironic then, that the reason I'm here is to make their lives more complicated; having computers in their community certainly won't make them any easier. Well, at least not yet.
