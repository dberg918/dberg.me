---
layout: post
title: "waiting or procrastinating?"
date:   2010-08-15 12:00:00 +0300
---

I'm sitting here in the school computer lab on a Sunday morning trying to update the operating systems on the school computers and things aren't going well already. It's only been about 20 minutes since I got here, but I've spent those 20 minutes waiting for one computer to boot the new Ubuntu Live CD. Eight months ago I would've told you that I wasn't considering getting new computers for the school, that they should concentrate on utilizing the computers they have, that they should focus on the here and now. I'm beginning to think this might change over the course of today.

This also has me thinking about taking another progressive step here in the lab; setting up a Local Area Network, something I talked a lot about when I first got here. Well, I haven't done anything about it yet. I don't really know why, other than the fact that the only way to get the equipment for it is probably a trip to Dar, and that it'd be smart to find a place that sells it and arrange something before actually going. It would also require me putting in a Leave Request Form, even though I personally feel requesting leave to travel to Dar is a little silly, since that's where the Peace Corps Office is. It all comes down to a lack of motivation, which has been spreading like the plague within me recently (though it seems to be affecting just my school activities). I could let myself feel guilty about it, but where would that get me? I think the first step in the right direction is a small one, something like taking some measurements of the room, or figuring out the table arrangement (or both). At least then I'll know approximately how much cable I'll need to get everything connected.

I just spent another 15 minutes typing the above paragraphs, and the computer still hasn't booted into anything useful.
 
*sigh* It's going to be a long day.
