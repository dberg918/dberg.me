---
layout: post
title: "slight still delay[ed]"
date:   2011-02-21 12:00:00 +0300
published: false
---

**EDIT:** Hey, so I haven't even started on this video yet. I'm still searching for the perfect tune, but am coming up empty. Did you know how many genres and subgenres and styles and substyles of electronic and drum 'n bass music there are? There's like, a kajillion. And I get the feeling the music I'm looking for is only in this one specific subgenre/style that's so obscure I won't be able to find it. Maybe I should just break out <a href="http://lmms.sourceforge.net/">LMMS</a> and make my own damn music...in any case, this might be one of those "it'll be ready when it's ready" scenarios. If this takes longer than another week, I'll make sure to post something to keep you all entertained.

Stay sweet.

The new video won't be up until later this week, considering I'm still in search of music for the audio track. If you know of any good drum 'n bass or electronic artists with strong rhythms, let me know.

Get excited though, it's about one of the "special" events I mentioned a couple weeks back :)
