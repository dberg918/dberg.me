---
layout: post
title: "have you SUFFERED lately?"
date:   2010-03-21 12:00:00 +0300
---

Given that nearly a quarter of 2010 has already gone by (yes, it really has been almost three months), I figure this is a good time to reflect on what myoneword has taught me so far. My apologies to the uninformed and the unreligious in the audience; you can check out <a href="http://myoneword.org">myoneword.org</a> to find out what I'm talking about here.


Those of you that are in close contact with my parents may already be aware of how true of a myoneword SUFFER has been for me. Even if you aren't, you can probably sense it in most of the blog posts since the new year began. Yes, there has been plenty of physical suffering for me in the past three months. In recent weeks, I've been experiencing a trend of having stomach troubles once or twice every 7 days. Usually not on the same days, but perhaps in the next few months I'll be able to predict when the distress will occur; at least then I could prepare notes for my poor students. I also had my house broken into a few weeks ago, leading to some slight mental turmoil (nothing important was stolen).

While I could just focus on the fact that I have indeed been SUFFERING this year, it would be silly of me if I didn't examine what God is trying to teach me and tell me through the suffering. I think one of the more profound things I've been hearing - not just during sickness but also during quiet times - is to simply be silent. One of my natural reactions to GI pains is to pray for mercy, which sounds hilarious when considered outside the situation. But I'm sure many of us have that one kind of sickness that cripples us into a beggar's stance: "God, take this from me," or "let it be quick, Lord" are fairly common prayers when I'm in this position. Over the past three months, the prayers have begun to evolve. "I don't want this anymore" has started to turn into "please give me peace." "Father, I'm hurting" is turning into "Father, I'm listening." Sometimes I think God allows us to be afflicted in our least favorite ways to get us to shut up. Wouldn't it be fair to say that the vast majority of the time we spend praying, we're the ones doing all the talking?

I believe one of the misconceptions among Christians when it comes to SUFFERING is that it's simply God's way of punishing us, or as Hebrews puts it, "chastening" us, when we're doing something wrong. Of course there's some truth to that, but that's a very shallow perspective if that's all you think suffering is; a tool used for behavior change. What I'm beginning to find is that when I begin to SUFFER, I begin to get closer to God. And I've found this to be the true purpose behind SUFFERING; it's not to correct behavior, it's to bring unity. I think the scriptures back this up as well. Just consider the passion of Jesus for a second. Some of the most intense SUFFERING any human being has endured, and it served to bridge the gap between God and man, bringing what? Unity.

Perhaps if we can have this perspective during our times of SUFFERING, we can delight in those tribulations the way Paul did...
