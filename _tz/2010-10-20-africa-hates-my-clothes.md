---
layout: post
title: "africa hates my clothes"
date:   2010-10-20 12:00:00 +0300
---

<a href="http://www.flickr.com/photos/23688234@N07/5100048812/" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;" title="Battle Scars! by dberg918, on Flickr"><img alt="Battle Scars!" height="160" src="http://farm2.static.flickr.com/1198/5100048812_56af4aca7c_m.jpg" width="240" /></a>

Now that about 13 months have passed since I arrived in Tanzania, some of my clothes are starting to get a little worn out. They've made a valiant effort to stay intact for so long in such a harsh environment; not that the environment here is that much harsher per say, but the fact that I wear these clothes a LOT compared to my habits of switching clothes every day (sometimes more) back in the States. Not to mention the hand-washing.

I just tore the pants today; I was only lightly tugging them down while sitting, but that particular spot on the knee was already looking a little ragged. It happened while I was tutoring some students in math in the computer lab, but I guess they didn't hear the rip, so it wasn't horribly awkward. The walk home was, however.

<a href="http://4.bp.blogspot.com/_WM5vLMqkVOk/TL8liBb2xOI/AAAAAAAAAPw/THbJaFel9LQ/s1600/PIMG-0029.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" height="200" src="http://4.bp.blogspot.com/_WM5vLMqkVOk/TL8liBb2xOI/AAAAAAAAAPw/THbJaFel9LQ/s1600/PIMG-0029.jpg" width="180" /></a>

The shorts...I'm not totally sure what happened there. I've chalked it up to either bugs eating the cloth, or chemical burns while I was helping some teachers set up the NECTA chemistry practical. I prefer the chemical burns theory.

In other news, the clouds are starting to roll in! I don't think I've ever been this excited for rain before. All I know is that when it finally does come, it will be magical. There will be dancing in the streets, with buckets poised to capture it in all its glory. Even just the thought of smelling it for the first time in 8 months is enough to make my knees buckle. Seriously, this rain can't get here soon enough.
