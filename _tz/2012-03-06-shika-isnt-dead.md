---
layout: post
title: "shika isn't dead"
date:   2012-03-06 12:00:00 +0300
---

No really, Shika hasn't kicked the bucket. But I'll be the first to admit things have stagnated a bit in terms of getting new people to maintain the book and keep it fresh. Though I'm working on a big fancy USAID project now, I'm still a Peace Corps Volunteer, and as a Peace Corps Volunteer, I like to keep my hands in things that other Peace Corps Volunteers are doing (or at least should be doing). Things like hands-on science!

In the past few days, I've been buckling down to get some documentation together for people who want to help us edit and write new content for the manual. My original idea of whipping up a few wiki pages with dry instructions, however, has suddenly blossomed into the desire to write a veritable handbook, complete with diagrams, graphs (I love graphs!), and of course...

<table align="center" cellpadding="0" cellspacing="0" class="tr-caption-container" style="margin-left: auto; margin-right: auto; text-align: center;"><tbody><tr><td style="text-align: center;"><a href="http://1.bp.blogspot.com/-1wBIfNyaSVs/T1WtF61WmPI/AAAAAAAAAe0/HK2ltZggxKw/s1600/nyan-digging.png" imageanchor="1" style="margin-left: auto; margin-right: auto;"><img border="0" height="171" src="http://1.bp.blogspot.com/-1wBIfNyaSVs/T1WtF61WmPI/AAAAAAAAAe0/HK2ltZggxKw/s640/nyan-digging.png" width="480" /></a></td></tr><tr><td class="tr-caption" style="text-align: center;"><span style="font-family: Georgia, 'Times New Roman', serif;">"The width of the planting bed should be approximately one yaNYANYANYANYA!"</span></td></tr></tbody></table>

One of the things I'm writing in the Preamble is an explanation of why we're not writing *Shika* in something like Word. Since many of our colleagues have switched to Ubuntu to save themselves from the rampant infection of annoying viruses here in Tanzania (not talking about HIV in this case), MS Office isn't really a good solution because OpenOffice doesn't work all that well with DOC files (or DOCX files for that matter). As you can see in the above drawing, crazy stuff can happen when OpenOffice tries to convert Word documents, especially big ones that have math and chemical formulas in them.

For my fellow volunteers reading this, I hope to have a decent draft finished before the end of next week. If you're interested in helping me proof-read, you know how to reach me. Also, expect to see many more internet memes in the literature. My goal is to hit a page-to-meme density of about two. You're welcome to help me with this as well.
