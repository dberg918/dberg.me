<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type='text/javascript'>
		$(document).bind("mobileinit", function(){
			$.mobile.ajaxEnabled = false;
		});
</script>
<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css" />
<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script type='text/javascript'>
  $(document).ready(function() {
	  $("div.jump-link a").attr("data-ajax", "false");
	  $("h3.post-title a").attr("data-ajax", "false");
	  $("div.post-footer a").attr("data-ajax", "false");
	  $("div#blog-pager a").attr("data-ajax", "false");
	  $("div#BlogArchive1 a").attr("data-ajax", "false");
	  $("div#PageList1 a").attr("data-ajax", "false");
	  $("div#Header1 a").attr("data-ajax", "false");
    $("a.translation").attr("title", "");
    $("div.popup-translation").attr("style", "display: block;");
  });
</script>
