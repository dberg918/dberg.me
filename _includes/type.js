<script type="text/javascript">
var nt_element = document.getElementById("finish");
var nt_text = document.getElementById("finish").getAttribute("text");
var nt_post = document.getElementById("there");
var nt_array = nt_text.split("");
var nt_timer;
var nt_speed = 50;
var nt_break = document.createElement("BR");

function defaultSet() {
  nt_post.setAttribute("id","hide");
  nt_element.setAttribute("id","wait");
}

function nerdText() {
  nt_element.setAttribute("id","type");
  if (nt_array.length > 0) {
    nt_element.innerHTML += nt_array.shift();
  } else {
    clearTimeout(nt_timer);
    nt_element.appendChild(nt_break);
    nt_element.setAttribute("id","wait");
    setTimeout(eraseCursor, 1100);
    return;
  }
  nt_timer = setTimeout(nerdText, nt_speed);
}

function eraseCursor() {
  nt_element.setAttribute("id","finish");
  document.getElementById("hide").setAttribute("id","slide");
}

function placeText() {
  nt_element.innerHTML = nt_text;
}

if (location.pathname.startsWith("/nerd")) {
  defaultSet();
  setTimeout(nerdText,1200);
} else {
  placeText();
}
</script>
