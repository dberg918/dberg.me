---
layout: page
title: About
---

<h2 class="about">About</h2>

I'm Dave Berg.

This is my digital journal for a few topics that I like to write about.

### Acknowledgments

Thanks to [@mdo][] for the [Hyde theme][hyde], upon which this blog is based.

[@mdo]: https://twitter.com/mdo
   "Mark Otto's Twitter account"

[hyde]: https://github.com/poole/hyde
   "Hyde Jekyll Theme on GitHub"
