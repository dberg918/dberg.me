---
layout: page
title:  A Fresh Start
subtitle: My Peace Corps Story
collection: tz
---

![A Fresh Start](/assets/tz/blog-title.png)

From September 22, 2009 to December 20, 2012, I served as a Peace Corps Education volunteer in Tanzania, East Africa. Below is the archive of the online blog I kept during my experience, edited slightly for posterity (and removal of dead links).

<table>
  <tbody>
    {% for post in site.tz %}
    <tr>
      <td class="archive-post language"><a href="{{ post.url }}">{{ post.title }}</a></td>
      <td class="archive-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%B %-d, %Y" }}</time></td>
    </tr>
    {% endfor %}
  </tbody>
</table>

[bl]: https://peacecorpsdmb.blogspot.com
