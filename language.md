---
layout: default
title:  Language
---

<h2 class="language">Language archives</h2>
<table>
  <tbody>
  {% for post in site.categories.language %}
  <tr>
    <td class="archive-post language"><a href="{{ post.url }}">{{ post.title }}</a></td>
    <td class="archive-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%B %-d, %Y" }}</time></td>
  </tr>
  {% endfor %}
  </tbody>
</table>

