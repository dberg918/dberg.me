---
layout: page
title:  Hello, I'm in 兵庫
subtitle: Living as a JET in Japan
collection: jp
---

![blog title](/assets/jp/hh-title.png)

On August 4th, 2013, I boarded a plane from Dulles International Airport bound for Japan. After my experience of living in a foreign country as a Peace Corps Volunteer, I decided I wanted more. Archived below is the blog I wrote during my first of three years in a small mountain town of Hyogo Prefecture.

<table>
  <tbody>
    {% for post in site.jp %}
    <tr>
      <td class="archive-post language"><a href="{{ post.url }}">{{ post.title }}</a></td>
      <td class="archive-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%B %-d, %Y" }}</time></td>
    </tr>
    {% endfor %}
  </tbody>
</table>
