---
layout: post
title: git commit -m pass it on
category: nerd
date: 2016-01-16 19:00:00 +0900
---

I think I'm pretty careful with my digital life. I do what I 
can to keep my identity and my personal information protected, like 
using strong, unique passwords on my online accounts and 
changing them on a fairly regular basis. I feel confident in my ability to spot 
phishing attempts (learn proper grammar, guys!). I guess the one weak point is 
the fact that I have social media accounts with my real name attached to them, 
but I suppose I'm not exactly a state-sponsored cracker that needs to cover his 
tracks at every turn.

All that said, I am also a relic of the boom of personal internet connections. 
Remember when you had to tell your family to get off the phone if you wanted 
to use the internet? Those were the days of my youth. Back then, things were 
much simpler when it came to protecting your privacy. AOL still controlled 
everything, so you had a password for your AOL account, but that was pretty 
much it---almost everything else was just static HTML (at least in my world). 
No NSA boogeymen to worry about, and the only person trying to crack your email 
password probably shared a roof with you...I mean, the chat rooms were 
certainly not an area you wanted to be, and you might accidentally download a 
virus that gave you popups for gambling websites (or perhaps something less 
palatable), but it certainly felt like a less scary place to explore.

### Getting more secure
I guess it's fitting that I write this now, after having just retired the 
oldest password in my collection, dating back well over a decade (maybe even to 
my last AOL account). What's worse is that it was my LastPass master password 
ever since I started using the service. It's a miracle that 10-character 2-word 
gem never got excavated. It certainly wasn't the lone exception in a history of 
pristine password hygiene of course, as I suspect is the case with many of us. 
I broke the cardinal rule of passwords for a long time by using one password 
for all of my online accounts for quite a while---even through most of college.

Although I've been using LastPass happily for a number of years already, I've 
recently decided to evaluate some less cloud-dependent options. 

 I knew there'd be some learning involved, but 
I wasn't afraid to get my hands dirty.

### pass vs. LastPass
I think there are a few criteria where these two password managers can be 
judged against each other to see which one comes out on top.

#### Out of the cloud
Depending on who you are, this could be a pro or a con. Keeping your passwords 
out of the cloud is a plus security-wise, as hackers are much more likely to go 
after LastPass than you personally (unless you've made a lot of nerdy enemies). 
Yeah, I know it's basically the "security through obscurity" argument, but I 
would have to believe that the probabilities are in my favor here.

That said, it's a con if you need to access your accounts from a computer that 
can't run `pass` (like Windows), or from shared/public machines. Considering I 
almost always have a personal computer in my pocket, I couldn't care less about 
access from shared computers. And while I do have a work machine that runs 
Windows, the need to access my accounts on that machine is debatable. For the 
most part, my work can be done without them, save for maybe one or two accounts 
in certain edge cases. Should I decide to ditch LastPass, a possible solution 
is to change the passwords for accounts I need "roaming" access to from 
LastPass' randomness to something I can memorize using Diceware, which is 
what I use for passwords I need to remember.

### PGP
