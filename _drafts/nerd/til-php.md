---
layout: post
title: "git commit -m today i learned: php sql queries"
category: nerd
date: 2016-06-15 08:30:00 +0900
---

I have studied computer languages off and on for a long time, but given my 
desire to enter the IT career field, I took it upon myself to start learning-by-
doing in earnest. Since I have experience building websites already, I figured 
PHP was a good place to start. Of course, PHP by itself can do some fun things, 
but using it in concert with a database like MySQL opens up some use cases that 
are immediately useful to me.

One project I had already started was a church database for a website I 
maintain in my spare time. The current workflow for the database looks like 
this:

 * Community members fill out a Google Form with their church information
 * I retrieve this info manually from the associated Google Spreadsheet
 * I paste it into an HTML template I've developed
 * I paste the filled-in HTML template into the database webpage

Needless to say, this is nearly impossible to maintain and clearly an 
unsustainable way to create a webpage. As a result, I've decided to use this as 
an opportunity to build my previously non-existent skills in PHP and SQL.

## w3schools

I've been following some of the tutorials on w3schools, which has been an 
excellent resource and reference for all of my previous web dev work. That 
said, the PHP tutorials--especially those involving SQL queries--have left me a 
bit dumbfounded. Perhaps this is because their approach is to simply toss you 
in the deep end for the sake of website security; they don't teach you the 
simple way to query a database in PHP, but go straight to the abstraction 
layers such as MySQLi and PDO. I know they do this to provide the current best 
practices for those wanting to learn, but surely they could do a better job of 
explaining what their code does.

Thus, the reason for this post. I'll be using this space as an exercise to 
attempt an explanation at a certain slice of code that w3schools has generously 
provided.

## spitting out your database

The only church database that is useful is one that you can see, so I want to 
create a page where the full database is visible and formatted nicely. In SQL, 
this means using a `SELECT` command. In the interest of security, w3schools has 
provided the following code, which uses the PDO abstraction layer combined with 
a prepared statement (plus a few modifications by me):

~~~
<?php
class TableRows extends RecursiveIteratorIterator {
	function __construct($it) {
		parent::__construct($it, self::LEAVES_ONLY);
	}

	function current() {
		return "<td>" . parent::current() . "</td>";
	}

	function beginChildren() {
		echo "<tr>";
	}

	function endChildren() {
		echo "</tr>" . "\n";
	}
}

$servername = "localhost";
$dbusername = "username";
$dbpassword = "password";
$dbname = "church_database";
$dbtable = "church_table";

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $dbusername, $dbpassword);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = $conn->prepare("SELECT * FROM $dbtable");
	$sql->execute();

	$result = $sql->setFetchMode(PDO::FETCH_ASSOC);
	foreach(new TableRows(new RecursiveArrayIterator($sql->fetchAll())) as $k=>$v) {
		echo $v;
	}
}
catch(PDOException $e) {
	echo "Error: " . $e->getMessage();
}
$conn = null;
?>
~~~
{: .language-php}

Now, I have a tiny bit of experience with classes and such, but this went way 
over my head.


