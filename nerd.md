---
layout: default
title:  Nerd
---

<h2 class="nerd">Nerd archives</h2>
<table>
  <tbody>
  {% for post in site.categories.nerd %}
  <tr>
    <td class="archive-post nerd"><a href="{{ post.url }}">{{ post.title }}</a></td>
    <td class="archive-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%B %-d, %Y" }}</time></td>
  </tr>
  {% endfor %}
  </tbody>
</table>

