---
layout: post
title: "arrival in tokyo"
date:   2013-08-04 12:00:00 +0900
---

I'm going to try to write as much as I can before I succumb to jet lag and forget everything tomorrow morning.

Yesterday may have begun with some bittersweet goodbyes, but today ended with some wonderful new friendships. Despite being the longest part of the journey so far, I have very little to say about the plane ride. I've done long plane rides before. No, they aren't fun, but they aren't necessarily that intolerable. I spent most of my time trying to fall asleep, hoping to offset the time jump; I figured I only needed a few hours to keep me awake until bedtime in Tokyo. Considering it's 9:30pm now and I'm still awake (but dragging pretty heavy), I did as well as I could have.
 
After disembarking, going through immigration, picking up checked luggage, and proceeding through customs, we were greeted by a long line of green t-shirts, herding us like sheep and leading the way out of the airport. Once outside, we boarded buses that took us to the hotel, which was about 2 hours away. The sights were somehow ordinary and mesmerizing all at once; acres and acres of rice paddies, giant skyscrapers towering over the expressway, and of course, the written language that I couldn't read. Arriving at the hotel, we were whisked up some escalators to a room set up just for checking in the new JETs. We all received a bag full of information and our room keys in very swift order.

Let me stop here to say thanks to all the current JETs who came into Tokyo to welcome us and help us out. It made getting from the plane to our hotel rooms a breeze. We haven't even started the orientation yet, but I can already honestly say you all are fantastic!

After getting settled in the rooms, a few of us decided to go out to find something to eat. First, we had to navigate the ATMs at the local post office. Thankfully, they have a "tourist mode" that guides you through the process in English with big colorful buttons to help you make the right decision. Money in tow, we tried our luck at an udon noodle bar. Despite not knowing any Japanese to order food, we were able to point at what we wanted on the menu. As we ate, I could tell the jet lag was wrecking havoc on all of us. Only a few of us finished what we ordered, and one or two looked visibly ill. I enjoyed my udon as much as I could have, but I felt pretty sick too.

Thankfully, one good bowel evacuation back at the hotel cleared me right up and I'm feeling great now! I hope the other JETs like talking about their poops, because the Peace Corps sure got me into some weird conversational habits.
