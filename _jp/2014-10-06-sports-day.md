---
layout: post
title: "sports day"
date:   2014-10-06 12:00:00 +0900
---

<div id="taiikutaikai" class="popup-translation" data-role="popup">
<h1>体育大会</h1>
<h2>たいいく　たいかい</h2>
<h3>taiiku taikai</h3>
<p>Sports Rally</p>
</div>

<div id="rajiotaisou" class="popup-translation" data-role="popup">
<h1>ラジオ体操</h1>
<h2>ラジオ　たいそう</h2>
<h3>rajio taisou</h3>
<p>Radio Calisthenics</p>
</div>

<div id="mukade" class="popup-translation" data-role="popup">
<h2>１００人百足競争</h2>
<h3>ひゃくにん　むかで　きょうそう</h3>
<h4>hyakunin mukade kyousou</h4>
<p>centipede race</p>
</div>

<div id="shougaibutsukyousou" class="popup-translation" data-role="popup">
<h1>障害物競走</h1>
<h3>しょうがいぶつ　きょうそう</h3>
<h4>shougaibutsu kyousou</h4>
<p>obstacle race</p>
</div>

<div id="rurounikenshin" class="popup-translation" data-role="popup">
<h1>るろうに剣心</h1>
<h2>るろうに　けんしん</h2>
<p>Rurouni Kenshin</p>
</div>

<div id="youkaiuocchi" class="popup-translation" data-role="popup">
<h1>妖怪ウオッチ</h1>
<h2>ようかい　ウオッチ</h2>
<h3>youkai uocchi</h3>
<p>Ghost Watch</p>
</div>

<div id="jibanyan" class="popup-translation" data-role="popup">
<h1>ジバニャン</h1>
<h2>jibanyan</h2>
<p>two-tailed ghost cat from Ghost Watch</p>
</div>

<div id="nyaa" class="popup-translation" data-role="popup">
<h1>ニャ~</h1>
<h2>nyaa</h2>
<p>meow</p>
</div>

<div id="taiikusai" class="popup-translation" data-role="popup">
<h1>体育祭</h1>
<h2>たいいく　さい</h2>
<h3>taiiku sai</h3>
<p>Sports Festival</p>
</div>

<div id="sohranbushi" class="popup-translation" data-role="popup">
<h1>ソーラン節</h1>
<h2>ソーラン　ぶし</h2>
<h3>so-ran bushi</h3>
<p>old Japanese sea shanty</p>
</div>

<div id="okurahomamikusa" class="popup-translation" data-role="popup">
<h2>オクラホマミクサー</h2>
<h3>okurahoma mikusa-</h3>
<p>Oklahoma Mixer</p>
</div>

<div id="kyoutousensei" class="popup-translation" data-role="popup">
<h1>教頭先生</h1>
<h2>きょうとう　せんせい</h2>
<h3>kyoutou sensei</h3>
<p>vice-principal</p>
</div>

Everyone stands at attention looking west. As the sun begins its descent from zenith, the flags flying at our school grounds follow suit and make their way down the poles. Japan's national anthem plays solemnly over the PA system. When the last note finishes, it echoes off the mountains and reverberates through town, hanging in the air for one last moment. Our <a class="translation" href="#taiikutaikai" data-rel="popup" data-transition="pop" title="たいいくたいかい, taiiku taikai, Sports Rally">体育大会</a> has come to a close.

Sports Day is an annual mainstay of Japanese school life, held at every elementary, middle, and high school in Japan. Some of the details vary from school to school, such as the time of year the ceremony takes place (some areas observe it in the midst of spring, while the majority celebrate at the dawn of autumn). But others are rather common, such as group presentations or the obstacle course relay. Finally are the events nearly universal in their practice across Japan: there will be marching, there will be foot races, and there will be <a class="translation" href="#rajiotaisou" data-rel="popup" data-transition="pop" title="ラジオたいそう, rajio taisou, Radio Calisthenics">ラジオ体操</a>.

### The Opening March

<div class="halfie">
![Sports Day 1][sd1]
![Sports Day 17][sd17]
</div>

At the sound of a starting gun, the ceremony kicks off with a massive student parade across the school grounds. The march playing in the background is the [NHK Sports Theme][nhk] which, judging by its name, sounds like it was written for the title cards of an NHK news segment (as it turns out, it was written by Yuji Koseki, the same composer that wrote the march for the 1964 Tokyo Olympics Parade of Nations and, amusingly, the fight song for Hyogo's professional baseball team, the [Hanshin Tigers][ht]). Parents and folks from the community clap along, cheering on their high-stepping, sharply-synced young counterparts. After making their rounds, the students march back into block form. With military-like precision, they turn 90 degrees on two counts and await the National Anthem. The flags slowly raise; on the left, the Prefectural flag; on the right, our high school's flag; in the center on the highest pole, the flag of Japan, reaching their pinnacle as the anthem reaches its climax. Remarks are given by the school administration, then the opening ceremony comes to a close and it's time to start the festivities.

### Radio Calisthenics

<div class="halfie">
![Sports Day 2][sd2]
![Sports Day 18][sd18]
</div>

<a class="translation" href="#rajiotaisou" data-rel="popup" data-transition="pop" title="ラジオたいそう, rajio taisou, Radio Calisthenics">ラジオ体操</a> is performed first to warm up the students before the races begin. This exercise routine has deep roots in Japan, though it was apparently inspired by the American insurance company MetLife, which sponsored similar radio calisthenics programs back in the 1920s in major U.S. cities. The sequence as it is known today was created in 1951; you can read a rough translation of all the exercises on the [JP Insurance][jpin] website. It is still broadcast early every morning in Japan, and there are many groups of people that get together to perform it.

### The Track Events

<div class="halfie">
![Sports Day 3][sd3]
![Sports Day 4][sd4]
</div>

At our school, most of the events are of the running variety. The boys and girls both have their 100m and 200m races, while only the girls run a 4x100m relay and the boys likewise only run a 4x200m relay. The last foot race is the boys' 1500m, just a bit short of the standard 1600m "mile" race. It is awe-inspiring to watch as participants leave everything on the track, running full bore all the way to the bitter end. Thinking back to field days in my early years, I don't recall ever seeing such a show of effort.

### The Class Contests

<div class="halfie">
![Sports Day 5][sd5]
![Sports Day 19][sd19]
</div>

In addition to the foot races, there are a few contests that pit each class of students (1st, 2nd, and 3rd years) against each other, including the <a class="translation" href="#mukade" data-rel="popup" data-transition="pop" title="ひゃくにん　むかで　きょうそう, hyakunin mukade kyousou">１００人百足競争</a>, tug of war, group jump rope, and something called <a class="translation" href="#shougaibutsukyousou" data-rel="popup" data-transition="pop" title="しょうがいぶつ きょうそう, shougaibutsu kyousou, obstacle race">障害物競走</a>. The １００人百足競争 is so named because it consists of groups of nine students tied together at the ankle running around the track together---closely resembling a centipede---with a tenth student calling out the march like a drill sergeant. If you thought 3-legged races were difficult, just imagine trying to do it with 8 other people. Of all the class contests, the tug of war and the group jump rope are the most familiar events to an American. The tug of war is decided by a flag tied to the middle of the rope, which falls in the direction of the winning team when the match is over. The winner of group jump rope is the class that completes the most successful revolutions of the rope. This contest is actually split between two teams for each class. At the end, the totals of each class' teams are added together to get the class total. The last class contest, 障害物競走, is best translated as an obstacle course relay. Each leg of the relay is paired with a parent or member of the community with which to run around the track. In order to advance, they must complete both physical challenges and mental challenges (trivia questions). Physical challenges include the classic dizzy derby bat spin, in which the runner holds their forehead to a baseball bat and spins a number of times to induce dizziness; bouncing a ball into a basket worn on the runner's back; trying to pop a balloon back-to-back with your partner; and my personal favorite, the flour pan, where the runner must stick his/her face into a huge container of white baking flour.

### The Class Presentations

<div class="halfie">
![Sports Day 6][sd6]
![Sports Day 20][sd20]
</div>

With the athletic events all decided, we move to the showcase of our high school's sports day, in which each class presents a skit featuring shortened plotlines from a beloved TV show or movie. Unsurprisingly, the movie *Frozen* makes an appearance thanks to our 1st years, a few cans of compressed air, and our computer teacher starring as Olaf the snowman. As Olaf begins to melt in the heat, the students turn their canisters on him to bring him back to life. Next, the 2nd years stage a dramatic sword battle depicting a scene in the life of <a class="translation" href="#rurounikenshin" data-rel="popup" data-transition="pop" title="るろうに　けんしん, Rurouni Kenshin">るろうに剣心</a>, the wandering samurai. One of our gym teachers dons the famous scar on her cheek and draws her blade against all enemies to save the damsel in distress. Finally, our 3rd years choose a relatively new anime cartoon called <a class="translation" href="#youkaiuocchi" data-rel="popup" data-transition="pop" title="ようかい　ウオッチ, youkai uocchi, Ghost Watch">妖怪ウオッチ</a>. One student straps on the ghost watch and brings out popular ghosts from the show, including our math teacher as <a class="translation" href="#jibanyan" data-rel="popup" data-transition="pop" title="jibanyan, two-tailed ghost cat from Ghost Watch">ジバニャン</a>, a cat that ends most of his sentences with 「<a class="translation" href="#nyaa" data-rel="popup" data-transition="pop" title="nyaa, meow">ニャ~</a>」, which is the sound that cats make in Japan. They end by performing the dance to the insanely catchy [ending song][yw], which has become quite popular among young and old anime fans alike.

<div class="halfie">
![Sports Day 21][sd21]
![Sports Day 7][sd7]
</div>

### The Club Parade

When lunchtime is over, students from our local junior high school come down the hill to join the fun. Technically speaking, this is when the actual <a class="translation" href="#taiikutaikai" data-rel="popup" data-transition="pop" title="たいいくたいかい, taiiku taikai, Sports Rally">体育大会</a> begins. Everything up to this point is considered our high school's <a class="translation" href="#taiikusai" data-rel="popup" data-transition="pop" title="たいいくさい, taiiku sai, Sports Festival">体育祭</a>. The students line up for a second time, dressed in their sports uniforms, and march around the field to show off their school pride. The principal of the junior high school makes a short speech to the students and then it's time for more activities.
 
### The Demonstrations

<div class="halfie">
![Sports Day 8][sd8]
![Sports Day 9][sd9]
</div>

First, the junior high boys take the field to show off some rather impressive team exercises. They hoist each other up on their shoulders, create human pyramids, and finish off with an impressive three-person tall tower---one boy standing on the shoulders of three students, who are standing on the shoulders of another five. Everyone is impressed by their strength and balance. Next, the junior high girls come on to perform a few dance numbers. Standing in a triangle formation, they hit their marks well and their moves pop with energy. Everyone cheers as they march off, waving goodbye to everyone. Then it's our high school girls' turn, who put on black robes and bring to life the famous <a class="translation" href="#sohranbushi" data-rel="popup" data-transition="pop" title="ソーラン　ぶし, so-ran bushi, old Japanese sea shanty">ソーラン節</a>. The ground seemingly moves under them as they heave to and fro, dancing with incredible leg strength and knees of steel. As soon as they're finished, on come the high school boys to finish this part of the program with their karate demonstration. Clad only in their gym pants, they begin with punches and kicks, stepping closer and closer to the audience before they stop and turn back. Then comes sparring, where they turn to a partner and show off defensive attacking techniques. Cheers emanate from the crowd as they run off the field, signaling the beginning of the finale of our Sports Day.

<div class="halfie">
![Sports Day 10][sd10]
![Sports Day 11][sd11]
</div>

### Mixed-school Relay

The culmination of the joint junior/senior high school festival is the final relays, where both schools send out two relay teams to find out who's the fastest in town. First is the girls' 4x100m relay, which the junior high girls win placing 1st and 2nd in a close race. The last race is the boys 4x200m relay, which is a nail-biter. Team A from the high school starts out in the lead, but Team B claws back in the second leg and, thanks to a crucial third leg run by one of our 2nd years to hold Team A at bay, the anchor from Team B breaks it open at the end to win it by a few lengths.

<div class="halfie">
![Sports Day 12][sd12]
![Sports Day 13][sd13]
</div>

### Folk Dances

Before the closing ceremony ensues, all of the students engage in a few folk dances with the help of some community members (mostly women because we're short a number of girls). The first is called <a class="translation" href="#okurahomamikusa" data-rel="popup" data-transition="pop" title="okurahoma mikusa-, Oklahoma Mixer">オクラホマミクサー</a>, which is performed to the American folk tune *Turkey in the Straw*. The second dance is done to the famous Russian folk song *Korobeiniki*, otherwise known as the song from *Tetris*.

<div class="halfie">
![Sports Day 14][sd14]
![Sports Day 15][sd15]
</div>

### The Closing Ceremony

The students make their way back into block formation, separated by school. They turn to each other and bow, expressing thanks for a great day of performances and competition. Once the junior high students are gone, the points from the day's earlier activities are tallied and one class is declared the winner of Sports Day. Even though the 2nd years impressed everyone with their presentation, the 3rd years take the trophy for their second year in a row thanks to their strength in the relays, the tug of war, and the group jump rope. Once the hugs and cheering is over, we find ourselves where we began, standing at attention, looking west, feeling proud.

![Sports Day 16][sd16]

*Immeasurable thanks is due to our <a class="translation" href="#kyoutousensei" data-rel="popup" data-transition="pop" title="きょうとう　せんせい, kyoutou sensei, vice-principal">教頭先生</a>, by whose camera most of these pictures were taken.*

[sd1]: http://4.bp.blogspot.com/-DZ8_zOAfOKg/VDHXuxX2g1I/AAAAAAAAHy4/RMVZiVnHMmY/s1600/DSC_0015.JPG
[sd2]: http://4.bp.blogspot.com/-EzZkF6Oh71w/VDHZXFTDgDI/AAAAAAAAHzI/BuAGpEbBZZE/s1600/DSC_0042.JPG
[sd3]: http://3.bp.blogspot.com/-wgCJiEiIXag/VC-bGU-t-xI/AAAAAAAAHyg/xRWmkM7IsQ8/s1600/IMG_1965.JPG
[sd4]: http://2.bp.blogspot.com/-E6aHSClKSqE/VC-bGa_ArUI/AAAAAAAAHyg/KZKfn8MZ9IY/s1600/IMG_1969.JPG
[sd5]: http://4.bp.blogspot.com/-pnuTyJSZMz8/VDHcceMIZ8I/AAAAAAAAHzg/k3DUtXJGjRI/s1600/DSC_0062.JPG
[sd6]: http://1.bp.blogspot.com/-Rz4fPXTPx98/VDHeTJ9aDDI/AAAAAAAAHz4/R3DWCZBIUP0/s1600/DSC_0235.JPG
[sd7]: http://2.bp.blogspot.com/-lGJosK8gVvg/VDHeUnx8hdI/AAAAAAAAH0Q/JOdBFqIyXcQ/s1600/DSC_0298.JPG
[sd8]: http://2.bp.blogspot.com/-ETLpBS6ItWc/VDHgQg0eFuI/AAAAAAAAH0c/xKe-qEhkBX8/s1600/DSC_0348.JPG
[sd9]: http://3.bp.blogspot.com/-Hk6Z22U2K6c/VDHgQ5_GRYI/AAAAAAAAH0g/5KI_FCF968U/s1600/DSC_0367.JPG
[sd10]: http://1.bp.blogspot.com/-8XWiqriNoV0/VDHgdktZ_9I/AAAAAAAAH0s/voCsiaPqdmY/s1600/DSC_0392.JPG
[sd11]: http://3.bp.blogspot.com/-LeRA1W49iW0/VDHgd51UDKI/AAAAAAAAH0w/1MuwVHSDlyM/s1600/DSC_0437.JPG
[sd12]: http://2.bp.blogspot.com/-oyfQY6tExzU/VDHjkEMPKJI/AAAAAAAAH1A/i2QlbYkHEQE/s1600/DSC_0455.JPG
[sd13]: http://1.bp.blogspot.com/-PHt8bTDrBYE/VDHjkUmODWI/AAAAAAAAH1E/J9E-Dx2iFFs/s1600/DSC_0464.JPG
[sd14]: http://2.bp.blogspot.com/-tJWAR-oaiwY/VDHlcQ2e-yI/AAAAAAAAH1U/STR_zkDPF6Q/s1600/DSC_0484.JPG
[sd15]: http://4.bp.blogspot.com/-C_jfhXewWUs/VDHlcyrMArI/AAAAAAAAH1Y/YPXGFKRoL5o/s1600/DSC_0486.JPG
[sd16]: http://4.bp.blogspot.com/-66adcQiXnks/VDHmTqXHi8I/AAAAAAAAH1w/zbHHbRFvFf4/s640/DSC_0023%282%29.jpg
[sd17]: http://2.bp.blogspot.com/-zy7wtqJm48s/VDHXuQv53XI/AAAAAAAAHy0/Vd6Z8RocUeg/s1600/DSC_0022.JPG
[sd18]: http://1.bp.blogspot.com/-ceU0G_ImMDQ/VDHZXsgdw3I/AAAAAAAAHzM/4qRKdTVodPo/s1600/IMG_6171.JPG
[sd19]: http://1.bp.blogspot.com/-9lKvHrLmJmg/VDHcwOjAtBI/AAAAAAAAHzs/Xi0KrbVNBzE/s1600/DSC_0103.JPG
[sd20]: http://4.bp.blogspot.com/-WjFSX_is-qo/VDHeUGkWudI/AAAAAAAAH0A/jy50JeRCu4U/s1600/DSC_0254.JPG
[sd21]: http://2.bp.blogspot.com/-lsEMqTAoyXQ/VDHeTj_TG2I/AAAAAAAAHz8/dSSukLEZ9rg/s1600/DSC_0278.JPG

[nhk]: https://www.youtube.com/watch?v=xwxmB-nZY68
[ht]: https://www.youtube.com/watch?v=um-BH7hPp4w
[jpin]: http://translate.googleusercontent.com/translate_c?act=url&depth=1&hl=en&ie=UTF8&prev=_t&rurl=translate.google.com&sl=ja&tl=en&u=http://www.jp-life.japanpost.jp/aboutus/csr/radio/abt_csr_rdo_dai1.html&usg=ALkJrhi8ZTnhkCUFfIp2ibhLlAd1OIgdqA
[yw]: https://www.youtube.com/watch?v=szGb41hHUHs
