---
layout: post
title: "hadaka matsuri"
date:   2014-02-17 12:00:00 +0900
---

<div id="fundoshi" class="popup-translation" data-role="popup">
<h1>褌</h1>
<h2>ふんどし</h2>
<h3>fundoshi</h3>
<p>traditional Japanese loincloth undergarment</p>
</div>

<div id="saidaiji" class="popup-translation" data-role="popup">
<h1>西大寺</h1>
<h2>さいだいじ</h2>
<h3>Saidai-ji</h3>
<p>temple in Okayama</p>
</div>

<div id="hadaka" class="popup-translation" data-role="popup">
<h1>裸祭り</h1>
<h2>はだか まつり</h2>
<h3>hadaka matsuri</h3>
<p>Naked Man Festival</p>
</div>

Some of you may have caught a strange post I made on Facebook over the weekend:

[![Hadaka Matsuri][hadaka]][hadaka]

I was in Okayama prefecture this past Saturday attending a very special, if unusual, Japanese festival that happens every year in Okayama City. At this festival, countless men, young and old, drop trou and hike a <a class="translation" href="#fundoshi" data-rel="popup" data-transition="pop" title="ふんどし, fundoshi, traditional Japanese loincloth undergarment">褌</a> up their rear end, and then run around the streets surrounding <a class="translation" href="#saidaiji" data-rel="popup" data-transition="pop" title="さいだいじ, Saidai-ji, temple in Okayama">西大寺</a>, one of Okayama's main temples. Why such an event takes place during the coldest month of the year says something about the people who choose to participate, though it is quite hilarious to watch a bunch of grown men clutching each other, jogging to the beat of taiko drums while screaming at the top of their lungs.

The event is called <a class="translation" href="#hadaka" data-rel="popup" data-transition="pop" title="はだか　まつり, hadaka matsuri, Naked Man Festival">裸祭り</a>, or the Naked Man Festival.

[![Running in the streets][streets]][streets]

The festival features hundreds upon hundreds of men every year, with many Japanese companies sponsoring "teams" to run in the event. After a few hours of running around to stay warm, everyone gathers on the temple grounds. Slowly but surely, the temple fills up with men until they spill down the steps and onto the surrounding stoney gravel. The area inside the temple building gets jammed so tight with diaper-wearing lads that they have to stick their arms up in the air so they aren't trapped below. It sounds tiring (I'm sure it is), but they need them up so they can grab at the sticks, called "shingi," dropped from above by the temple priests. These sticks are the prized possessions of the event, especially the incense-filled "daishingi."

In the 45 minutes leading up to the stick drop, the crowd of men typically experiences a number of [violent mass shifts][1]. It is awe-inspiring to watch, until the wave hits those unfortunate enough to be stuck near the steps of the temple, where the uneven ground and heaving momentum tosses men around like rag dolls, leading to what we aptly named "manvalanches." Despite a number of appearances by medics and the police pushing into the crowd, nobody died Saturday night that I know of (there have been fatalities in the past).

Around 10PM, the lights in the temple went out and the sticks were dropped. The ensuing 10-15 minutes turned into a veritable rugby scrum of a thousand men grabbing and clawing at each other. After a number of smaller skirmishes occurred below the steps, all of the sticks had quickly made their exit from the temple grounds. Game over.

Even though the actual event was short, the fun continued outside where food stands continued to offer all kinds of fantastic snacks!

[![Food stands][food]][food]

I'm already being urged to participate next year. It might be fun, though the looming specter of being crushed to death at an event called "Naked Man" is a little off-putting.

[hadaka]: http://4.bp.blogspot.com/-1uDuucXxHu0/UyKpCHrCT9I/AAAAAAAADaM/E-BRMIxswYg/s1600/IMG_0736.JPG
[streets]: http://3.bp.blogspot.com/-737mIDooEy8/UyKp6OypkgI/AAAAAAAADaU/g-TAqElh3eg/s1600/IMG_0730.JPG
[food]: http://2.bp.blogspot.com/-FqZ8dKF2za8/UwGsyr4MQKI/AAAAAAAADAE/FXRZ7J_85SM/s1600/IMG_0737.JPG

[1]: http://www.youtube.com/watch?v=HSINatmlG4c
