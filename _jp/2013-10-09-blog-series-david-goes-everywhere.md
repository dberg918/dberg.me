---
layout: post
title: "blog series: david goes everywhere!"
date:   2013-10-09 12:00:00 +0900
published: false
---

Just to make everyone aware, I have been writing short essays for my school's English blog. If you want to hear about some of the larger events and extracurricular things I do at the school, keep an eye on the "Chikusa High School" blog link in the right-hand sidebar under "Blogs you should follow." I will still post here on my personal blog about school, but it will be less formal in manner and more personal with regards to the topic. For the big stuff, refer to the school's website. Also don't be afraid to check out the Japanese entries that show up; most of them have a host of pictures of the activities going on here!
