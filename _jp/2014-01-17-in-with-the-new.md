---
layout: post
title: "in with the new"
date:   2014-01-17 12:00:00 +0900
---

Wow! That holiday break was a real whirlwind of activity. The hardest part was keeping the majority of it secret---unbeknownst to my parents, I sneaked onto a bullet train to Tokyo on December 22nd, then boarded a triple-7 bound for IAD the next day to surprise them for Christmas. For the better part of a month, my brother and I plotted and schemed in the dark to guarantee maximum shock value. Did it work? I'd say so.
 
<div style="text-align: center;">
  <iframe class="vine-embed" src="https://vine.co/v/hE9xmiKEtUE/embed/postcard" width="320" height="320" frameborder="0"></iframe>
  <script async src="//platform.vine.co/static/scripts/embed.js" charset="utf-8"></script>
  <a href="http://3.bp.blogspot.com/-nPAvz1mA0ic/UtjQL74fPFI/AAAAAAAACw4/r9fYuyXsHQU/s1600/IMG_0645.JPG" imageanchor="1">
    <img id="half" border="0" src="http://3.bp.blogspot.com/-nPAvz1mA0ic/UtjQL74fPFI/AAAAAAAACw4/r9fYuyXsHQU/s320/IMG_0645.JPG" />
  </a>
</div>

After a few days of visiting family and watching bowl games, I hopped on my return flight to touch down in Tokyo just a few hours before Japan rang in the New Year. Despite an errant co-pilot who never showed up, we were only an hour late and I had sufficient time to find my group of friends before the countdown. We chose to attend a small party happening around Tokyo Tower (not the Skytree), which featured a big band playing Latin music. At the stroke of midnight, attendees that were given balloons let them go and watched them rise in a wild chorus of shouts and cheers.

[![Family][fam]][fam]

We went to a nightclub to continue the celebration until about 3:30 in the morning, at which time we decided to return to our capsule hotel. If you don't know what a capsule hotel is, it's akin to a dormitory with bunk beds, only with slightly more privacy. Our hotel, however, had a community bath, so no private showers. I didn't take any pictures, but you can look at [this video][1] to get a basic idea.

Considering the 4 hours of sleep I got that night, plus the jet lag from being on a plane for 14 hours and crossing the International Dateline, travel on New Years Day was going to be difficult for me no matter which way you look at it, but I decided to go ahead and make my way back home. It took well over 10 hours, an entire circuit of Tokyo's Yamanote Line, and a loan from a friend to get there (note to self: your Japanese account is not available for withdrawal on holidays), but I did it. Thankfully, the snow that was piled up over a foot high when I left had melted away by the time I returned, easing the stress I felt about getting stuck just minutes away from my apartment.

Now, I'm back into the swing of school. Our active class has officially switched from golfing to skiing, so every Thursday now features me around the bunny slope, looking goofy as I try to master the art of gliding. I've also added another extra-curricular activity; right before the winter break, I became a co-Webmaster on the [National AJET Council][2]! This is in addition to my web maintenance duties for JET Christian Fellowship, which I neglected to mention earlier when I took on that responsibility in October. I'm busy, but it feels good!

[fam]: http://4.bp.blogspot.com/-YA_rky2ooo8/UypKXx-_yXI/AAAAAAAADfk/IFfLUBI1BOM/s1600/IMG_0674.JPG
[1]: http://www.youtube.com/watch?v=J1TbiOI7RGs
[2]: http://ajet.net
