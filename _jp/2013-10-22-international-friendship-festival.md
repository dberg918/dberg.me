---
layout: post
title: "international friendship festival"
date:   2013-10-22 12:00:00 +0900
---

<div id="pa" class="popup-translation" data-role="popup">
<h1>PA</h1>
<h3>Prefectural Advisor</h3>
<p>Being a Prefectural Advisor is an added responsibility on top of whatever your first job is, whether you're an ALT or a CIR. Japanese employees that work for the local government can also serve as PAs to support the JETs in these positions. I dont' know how they are selected, but I assume that CLAIR has something to do with it. PAs provide support to the JETs in their prefecture, mostly in the form of counselling should they become disillusioned or downtrodden during their time in Japan.</p>
</div>

[![IFF][festa]][festa]

After my travels in Kyoto over the weekend, our city held its 9th annual International Friendship Festival on the outskirts of Yamasaki. Though it rained throughout the day, it drew a good crowd, including a number of foreign residents I hadn't met before and our <a class="translation" href="#pa" data-rel="popup" data-transition="pop">PAs</a> in Hyogo!

As an international festival, many different nationalities were represented in the people that attended, and a host of them set up booths where they shared a prized dish from their local cuisine. Also, since we're currently approaching Halloween at the end of the month, the festival was an opportunity for everyone to wear a costume and teach the children about trick-or-treating. A few of us ALTs decided to extend this Halloween theme and set up the festival's first-ever haunted house in the adjoining parking lot. Underneath 3 tents, there was an outdoor face-painting and games section, a murder room with a bloody table and creepy pictures on the wall, and a blacklight room rendered dark with rolls of black, trashbag-like plastic.

The blacklight room was by far the most labor intensive, not just in set up but also in preparation. It featured spooky trees, which we did on large black sheets of paper and taped to the walls. Since there wasn't much time for painting the weekend of the event, we did it the weekend before at my apartment, as I had a nice back patio area that was the most conducive to spray painting.
 
[![Spray Painting][paint]][paint]

During operation, I was on "scare duty," dressed in dark colors with a Jason Voorhees mask to frighten the patrons that walked through. While we were supposed to tone it down for the officially designated "children's time," I routinely made small kids burst into tears without doing anything remotely frightening, besides looking at them and waving timidly. A select, rambunctious few were apparently ashamed of themselves for being scared the first time, so they came through repeatedly over a 5-minute window to prove I wasn't scary. On their last trip through, they decided to dip the weapons associated with their costumes in the red paint we used for the bloody table to try and kill me. They were subsequently kicked out and not allowed back in the rest of the day. It took me a while to figure out why they were smearing their swords and guns all over my sweatshirt since I couldn't see anything under the blacklight, but after I emerged for our first break, I discovered I was covered in red paint. The joke was on them, however, as it only made me look scarier.

[![Jason][jason]][jason]

Although the haunted house was a great success, the jury is out on whether we will repeat it next year. For starters, we're waiting to see if the giant red paint stain where the table was comes off in the parking lot. Hopefully our Japanese counterparts aren't too upset about that. But now that we've done it, we're aware how much work it takes to pull it off. Maybe if we get a few more people on board next time it'll be easier to manage. I, for one, would love to see it happen again.

[festa]: http://3.bp.blogspot.com/-CFVNwn2GjOQ/UypQYcPDE3I/AAAAAAAADg0/Q6b8pprakAs/s1600/IMG_0423.JPG
[paint]: http://1.bp.blogspot.com/-g-68bOZ6IRY/UypQzJFmILI/AAAAAAAADg8/Mz_xLYHy-wo/s1600/IMG_0429.JPG
[jason]: http://3.bp.blogspot.com/-DpF3zyAVQLg/UypREi39Y5I/AAAAAAAADhE/GY6TJN872kA/s1600/IMG_0425.JPG
