---
layout: post
title: "preparing for winter"
date:   2013-11-19 12:00:00 +0900
---

<div id="kotatsu" class="popup-translation" data-role="popup">
<h1>炬燵</h1>
<h2>こたつ</h2>
<h3>kotatsu</h3>
<p>small table with an electric heating element on the underside</p>
</div>

[![Fall Colors][fall]][fall]

It's been a long, long time since I've had the full autumn experience—mourning the waning daylight, feeling the brisk chill in the air, and most of all, watching the color-shifting landscape. The cold has been rapidly approaching since the end of October, and with it, a rapid change in the temperature in my apartment. I suppose the nostalgia of the autumn seasons of my youth are keeping me afloat at the moment, but there's no doubt that the thought of winter, when the temperature isn't likely to climb much above freezing, has me a bit worried. Fortunately, I'm no longer living on a Peace Corps salary and I can actually afford to make my life a bit more comfortable. In this entry, I'm going to introduce some of the things that will hopefully get me through the harsh winter of Japan.

The first thing you need to know about Japan is that the concept of central heating and air conditioning largely does not exist, especially when it comes to the types of housing situations JETs find themselves in. There's no magic knob that controls the temperature inside your home, no gas heater that is well-integrated into your building. The only things that protect you from the bracing, frigid outdoors are your thin windows and even thinner walls, both which are often poorly sealed and insulated. As a result, your apartment turns into nothing more than a wind-breaking ice box as November wears on.

|:------------------------------------------------:|
|        [![Ceramic Heater][heater]][heater]       |
|:------------------------------------------------:|
| My ceramic heater, a champion of generating heat |

I felt the chill early in late October, especially as I slept at night, so the first thing I decided to buy was a small ceramic heater I could use to heat my bedroom. It's a strange-looking machine, reminiscent of a computer monitor, that uses electricity to heat coils embedded in ceramic tiles and then blows the heat out through vents on the bottom. This thing works incredibly well, and I'm rather surprised that the machine itself never gets hot; even after it's been running for a few hours, the entire thing remains cool to the touch.

|:------------------------------------------------:|
|        [![Heated Table][kotatsu]][kotatsu]       |
|:------------------------------------------------:|
|  Nothing like snuggling up next to a cute table  |

Slowly but surely, the cold started creeping into my living room earlier and earlier in the evening, so I was having to move my heater between rooms fairly often. To mitigate this problem, I bought a <a class="translation" href="#kotatsu" data-rel="popup" data-transition="pop" title="こたつ, kotatsu">炬燵</a>, a wonderful invention that is a quintessential piece of winter furniture in every Japanese home. It is a table that has an electric heating element on the underside, with a detachable tabletop which allows you to drape a heavy blanket over it to trap the heat underneath. Sticking your legs under one of these on a cold day is pure happiness.
 
|:------------------------------------------------:|
|         [![Uniqlo HeatTech][legs]][legs]         |
|:------------------------------------------------:|
|                 My first leggings!               |

In terms of clothing, I took a trip into Himeji a couple weeks back to check out Uniqlo, a very popular clothing store chain in Japan (think Target without all the other stuff). Around this time of year, they sell a clothing line called <a href="http://www.uniqlo.com/ht_w/us/" title="Uniqlo's HEATTECH">HEATTECH</a>, which is essentially what it sounds like—clothes designed to keep you warm. I bought a long sleeve shirt and a pair of pants. It's all form-fitting, so they basically amount to a leotard when worn together. It certainly does its job though; I've started wearing the pants to school---under actual pants, of course---on a regular basis because they haven't turned on the heat in the building yet. I'm slightly afraid of going without them now. I plan to go back to buy matching socks, and perhaps a turtleneck once the cold really settles in.

Finally, the November weather is starting to get the best of the heater in my large bedroom, so I decided to upgrade my bedclothes. I replaced my thin cover sheet with a thick fleece pad and spent nearly $100 on a goose down comforter to improve on the light-as-air bed cover I was using previously. I thought it was a little expensive, but after spending a few nights under it, this is probably the best investment I've made in winter preparations thus far. The thing traps heat so well, I don't need to run the heater at night anymore, which is astonishing considering the air temperature in my bedroom is probably in the low 50s these days. In fact, I was sweating the first night because I was wearing pants and socks. If there are any JETs reading this and you don't have one of these guys, do yourself a favor and buy one at your local Aeon or similar department store. It's well worth the money.

|:------------------------------------------------:|
|          [![Warm Futon][futon]][futon]           |
|:------------------------------------------------:|
|     Futons don't get much warmer than this       |

Aside from these things, I have done a little work on improving the seals on the doors and windows, and my main line of defense has been staying layered and taking hot showers in the morning. Otherwise, I'm just bracing myself as the temperature continues to drop. While I'm not looking forward to afternoon highs in the single digits, I am excited for the fun activities that winter is sure to bring!

[fall]: http://3.bp.blogspot.com/-b5Bz12KQ1Z0/UypNrddMxpI/AAAAAAAADgE/xKlGknN0oF8/s1600/IMG_0507.JPG
[heater]: http://1.bp.blogspot.com/-sT9PIiGQsTk/UypN5U6EcjI/AAAAAAAADgM/fIwiGLaaz6c/s1600/IMG_0526.JPG
[kotatsu]: http://4.bp.blogspot.com/-DawBBLgymbY/UypOUrrORvI/AAAAAAAADgU/fOKML8Fgeac/s1600/IMG_0527.JPG
[legs]: http://1.bp.blogspot.com/-q7NGDLHZfSw/UypOuErfpTI/AAAAAAAADgc/gP1TgX73iPM/s1600/IMG_0528.JPG
[futon]: http://1.bp.blogspot.com/-07hpYgl8McQ/UypPED8zmLI/AAAAAAAADgo/fcgd3ni5A9c/s1600/IMG_0529.JPG
