---
layout: post
title: "springtime is here!"
date:   2014-05-19 12:00:00 +0900
---

<div id="sayounara" class="popup-translation" data-role="popup">
<h1>さようなら</h1>
<h2>sayounara</h2>
<p>good-bye</p>
</div>

<div id="ume" class="popup-translation" data-role="popup">
<h1>梅</h1>
<h2>うめ</h2>
<h3>ume</h3>
<p>Japanese plum</p>
</div>

<div id="zauo" class="popup-translation" data-role="popup">
<h1>ざうお</h1>
<h2>zauo</h2>
<p>Zauo, a <a target="_blank" href="http://www.zauo.com/">restaurant</a> in Osaka</p>
</div>

<div id="kyocera" class="popup-translation" data-role="popup">
<h1>京セラドーム</h1>
<h2>きょうせらどーむ</h2>
<h3>kyousera do-mu</h3>
<p>Kyocera Dome</p>
</div>

<div id="buko" class="popup-translation" data-role="popup">
<h1>Buko</h1>
<p>young, drinkable coconut</p>
</div>

<div id="khanga" class="popup-translation" data-role="popup">
<h1>Khanga</h1>
<p>A multi-purpose, towel-sized sheet of fabric sold in Tanzania, often having bold patterns and a Swahili message on the bottom.</p>
</div>

<div id="lola" class="popup-translation" data-role="popup">
<h1>Lola</h1>
<p>grandmother</p>
</div>

<div id="butanding" class="popup-translation" data-role="popup">
<h1>Butanding</h1>
<p>whale shark</p>
</div>

<div id="hanami" class="popup-translation" data-role="popup">
<h1>花見</h1>
<h2>はなみ</h2>
<h3>hanami</h3>
<p>cherry blossom viewing party</p>
</div>

<div id="sakura" class="popup-translation" data-role="popup">
<h1>桜</h1>
<h2>さくら</h2>
<h3>sakura</h3>
<p>cherry blossom</p>
</div>

In just a blink of an eye, we've gone from saying <a class="translation" href="#sayounara" data-rel="popup" data-transition="pop" title="sayounara, good-bye">さようなら</a> to the previous school year to holding mid-term exams here at Chikusa High School. Where did all the time go? A lot has certainly happened in the past 2 months, and it's about time to catch you up! Here's some of what's happened in my life since I last wrote.

### #gentsweekend

As school began to wind down at the end of March, a few of my close friends and I took Kobe and Osaka by storm. The fun started at Hatsukuru Brewery, where we watched a 15-minute introductory video about a guy that devoted his life to getting hammered drunk on sake every day while simultaneously documenting his experiences in poetry. This is otherwise known as "living the sake lifestyle." Unfortunately, it's apparently not one you can live for very long, as the guy died when he was 47 (if I had to guess, most likely from something liver-related). From there, we walked through the different stages of sake production, then had the opportunity to taste a few varieties of Hatsukuru's own brews. Before we left, we picked up a bottle of <a class="translation" href="#ume" data-rel="popup" data-transition="pop" title="うめ, ume, Japanese plum">梅</a>-flavored sake to lubricate on our train ride to Osaka, where we spent a few hours relaxing in our capsule hotel before we hit the streets.
 
[![Gents Weekend 1][gents1]][gents1]

That night, we went to <a class="translation" href="#zauo" data-rel="popup" data-transition="pop" title="Zauo">ざうお</a>, a seafood restaurant recommended in the Lonely Planet guide to Osaka for its unusual style of dining. The main room has a dining area shaped like the bow of a large wooden boat, where you sit in the traditional Japanese style at tables that are situated port and starboard. Along the sides are giant fish tanks filled with a variety of specimens...I think you can see where I'm going with this. It's a restaurant where you literally *fish* for your dinner. A very cool premise, but much more frustrating in practice. Despite our valiant attempts, only one of us actually managed to hook something---probably because of our continued consumption of adult beverages. #sakelifestyle

[![Gents Weekend 2][gents2]][gents2]

The next day, we took a train down to Nara in the morning to get felt up by roaming deer looking for snacks and to see the largest wooden structure in the world. It contains a giant buddha, in case you were wondering (among other statues). We returned to Osaka in the afternoon to take in a pre-season baseball game at the <a class="translation" href="#kyocera" data-rel="popup" data-transition="pop" title="きょうせらどーむ, kyousera do-mu, Kyocera Dome">京セラドーム</a>. We were able to sit pretty close to the action since exhibition games aren't nearly as popular here than they are in the States. The evening continued with ramen in [Shinsaibashi][shin], karaoke near Dotonburi, and clubbing at PURE, where despite the high cover charge, drinks were free all night!

<div class="halfie">
[![Gents Weekend 3][gents3]][gents3]
[![Gents Weekend 4][gents4]][gents4]
[![Gents Weekend 5][gents5]][gents5]
[![Gents Weekend 6][gents6]][gents6]
</div>

[![Gents Weekend 7][gents7]][gents7]

### Spring Break in the Philippines

Juxtaposed by a weekend of nightlife, I spent the rest of my spring break in the Philippines with members of JET Christian Fellowship (JCF). We worked at an elementary school in Tolosa, a rural village about an hour outside of Tacloban city. The Tacloban area was the hardest hit by Typhoon Haiyan, but despite all of the relief efforts, very little made it outside the city. Tolosa in particular hadn't seen any sort of help, so they were singled out by our friends at the Department of Social Welfare and Development as a place we should visit. For two days, we taught English lessons, told Bible stories, and played games and activities with the kids. For such young children that have survived such an awful natural disaster, they radiated strength and happiness, by which many of us were shocked. They truly captured our hearts, so much that we are looking into ways to maintain contact with them, and to possibly visit them again next year!

<div class="halfie">
[![PH 1][ph1]][ph1]
[![PH 2][ph2]][ph2]
[![PH 3][ph3]][ph3]
[![PH 4][ph4]][ph4]
[![PH 5][ph5]][ph5]
[![PH 6][ph6]][ph6]
</div>

Our next stop was in Biliran, a place famous for its beaches. We stayed at a church there, whose pastor is the brother of one of our trip members. They nearly fed us to death with countless signature Filipino dishes and fresh <a class="translation" href="#buko" data-rel="popup" data-transition="pop" title="young, drinkable coconut">buko</a>---something I was definitely jonesing for. It was like a trip back to my life in the Peace Corps; I took bucket baths, washed some clothes by hand, and made very good use of the <a class="translation" href="#khanga" data-rel="popup" data-transition="pop" title="A multi-purpose, towel-sized sheet of fabric sold in Tanzania, often having bold patterns and a Swahili message on the bottom.">khanga</a> I brought with me. While in Biliran, we performed a small show for the local village, in which we sang songs, told stories, and performed a one-scene play all in Tagalog (I played the <a class="translation" href="#lola" data-rel="popup" data-transition="pop" title="grandmother">lola</a>). Afterwards, we fed them and gave away some donations. Despite being exhausted by all the teaching, traveling, and performing we had done over those 5 days, we all came away from the trip incredibly blessed and extremely grateful to the Filipino people that welcomed us and made us feel at home wherever we went.

<div class="halfie">
[![PH 7][ph7]][ph7]
[![PH 8][ph8]][ph8]
</div>

[![PH 9][ph9]][ph9]

### Swimming with whale sharks

Rather than go home with the rest of our group, I got on a plane with a fellow JCF member and headed to Bicol, a small town outside of Legaspi City in another part of the Philippines. It was the beginning of peak season for <a class="translation" href="#butanding" data-rel="popup" data-transition="pop" title="whale shark">butanding</a> watching, and since I had missed my opportunity to see them back in Tanzania, I jumped on the one presented to me in Donsol Bay. We were out on the boat for 3 hours and managed to spot 6 of them! I swam with the last one for a good minute before our guide told us it was time to go. Seeing pictures and videos of them is one thing, but swimming above one gives you a much better sense of just how massive these guys are. I tried to take pictures with my phone through a waterproof pouch, but the water pressure when submerged made any kind of touch gestures essentially impossible.

[![Butanding 1][butan1]][butan1]
<div class="halfie">
[![Butanding 2][butan2]][butan2]
[![Butanding 3][butan3]][butan3]
</div>
[![Butanding 4][butan4]][butan4]
 
### Osaka Castle <a class="translation" href="#hanami" data-rel="popup" data-transition="pop" title="はなみ, hanami, cherry blossom viewing party">花見</a>

Shortly after my trip to the Philippines, I was back in Osaka for a massive JET hanami party. Much like the Americans that flock to Washington DC to see the cherry blossoms in April, there is a long-standing Japanese tradition of having picnics when the <a class="translation" href="#sakura" data-rel="popup" data-transition="pop" title="さくら, sakura, cherry blossom">桜</a> are in full bloom. AJET organized a massive party inside the Osaka Castle grounds and well over 100 people showed up! The highlight for me was leading a few drunken sing-a-longs to popular tunes by Avicii, Kesha (no more dollar sign!), Outkast, and Oasis.

<div class="halfie">
[![Hanami 1][hanami1]][hanami1]
[![Hanami 2][hanami2]][hanami2]
[![Hanami 3][hanami3]][hanami3]
[![Hanami 4][hanami4]][hanami4]
</div>

### The Philippines bite back

Despite feeling perfectly fine for 3 days after returning from my spring break trip, I soon fell ill the next week and missed a few days at work including, unfortunately, my school's opening ceremony. It took me a little while to level out after my fever went away, which led to a few return trips to my local clinic to have some blood tests done. We're still not sure what it was, but by the end of April I was all back to normal.

### Golden Week Peace Corps Reunion

By the time I had fully recovered from illness, it was Golden Week in Japan, so called because of the chain of holidays that line up to create a nice long break at the beginning of May. It is generally considered to be the busiest travel time of the year in Japan, which sounds like a nightmare, but it actually wasn't too bad. I spent the first night up on Mount Rokko outside of Kobe where JCF was holding their spring retreat. I got to reconnect with friends from the trip, as well as meet some new people I hadn't had the chance to meet yet. I was put on firepit duty, which turned out to be a much more laborious job than I bargained for. All the smoke I inhaled that evening led to a cold, in spite of my copious intake of vitamin C afterwards.

[![Golden Week 1][gw1]][gw1]

The next day I came down off the mountain and headed for Osaka, where I was planning to check-in to my capsule hotel and head to Kyoto. A few friends I served with in the Peace Corps were making their way through east Asia and ended up in the Kansai area for Golden Week! As luck would have it, my phone stopped picking up a signal after I came down from Mount Rokko, so I had to stop in Osaka longer than I had planned to get it fixed (perhaps my phone inhaled some smoke, too). We were able to meet for dinner that evening and then went to an Orix Buffaloes game in Osaka the following day. It was great to catch up and to hear about what the future holds for them.
 
[![Golden Week 2][gw2]][gw2]

### Lake Biwa Boat Cruise

Just this past Saturday, [AJET Block 6][block] held its last event of the JET year (it ends in June), a cruise on Lake Biwa aboard the Michigan. Like the hanami party at Osaka Castle, there was a great turnout of JETs---again over 100 of us attended and set sail in the evening. As is customary at AJET events, there was lots of socializing and drinking. In fact, the vast majority had decided to pre-game before the cruise and boarded with a pretty heavy buzz, which only made the experience that much more hilarious. We got to watch the sunset while on the water before nighttime fell and the city lights lit up the coast. I got to reconnect with a few friends I hadn't seen in a while, so I had a great time!

<div class="halfie">
[![Boat 1][boat1]][boat1]
[![Boat 2][boat2]][boat2]
</div>

### Next week, Fukuoka

I've finally reached present day, but there's another excursion coming up. This coming Saturday, I'll be taking the bullet train out to Fukuoka to participate in another AJET Block finale. Not only will I be crossing off another prefecture on the visit list, I'll have the opportunity to see a new baseball stadium and another baseball game! I don't know any of the JETs out west, but with baseball and beer in the mix, it shouldn't be too hard to make new friends.

[gents1]: http://3.bp.blogspot.com/-HQumd9JDjBw/U3l1EJMoWcI/AAAAAAAAFPc/KtFagzXg0rE/s1600/1052182_10103676574517013_207412366_o.jpg
[gents2]: http://1.bp.blogspot.com/-tmZsfQuzD2E/U3l0hy7OTBI/AAAAAAAAFPU/UF2B36BLCy0/s1600/IMG_0802.JPG
[gents3]: http://2.bp.blogspot.com/-3ZChTQSBkUY/U3l3VjJ1NEI/AAAAAAAAFQA/Owa424oNm44/s1600/IMG_0830.JPG
[gents4]: http://2.bp.blogspot.com/-3toorLc-rRo/U3l1p1GAeNI/AAAAAAAAFPk/XKJY6F79deo/s1600/IMG_0819.JPG
[gents5]: http://3.bp.blogspot.com/-xiaej3Q4EHg/U3l2DRfpS1I/AAAAAAAAFPw/iPQ7hljEAvs/s1600/IMG_0822.JPG
[gents6]: http://1.bp.blogspot.com/--1aXhn_XyP0/U3l2DRJql6I/AAAAAAAAFPw/OgqKyZjulaA/s1600/IMG_0826.JPG
[gents7]: http://1.bp.blogspot.com/-k0yIkcwzmCQ/U3l1p2XBgXI/AAAAAAAAFPk/YqIWxfK1J_s/s1600/IMG_0833.JPG

[ph1]: http://1.bp.blogspot.com/-GtPUz7lLyCc/U3l62R3mcqI/AAAAAAAAFQM/olzUDO0gPjU/s1600/IMG_1021.JPG
[ph2]: http://3.bp.blogspot.com/-aCsANoPesY8/U3l62WruTJI/AAAAAAAAFQM/35WV7iYOC38/s1600/IMG_1131.JPG
[ph3]: http://3.bp.blogspot.com/-FcEi3GWkZs4/U3l62ZIZgaI/AAAAAAAAFQM/WjEzamfbiEk/s1600/IMG_1133.JPG
[ph4]: http://3.bp.blogspot.com/-AB_6_SuHkCs/U3l62W3A5gI/AAAAAAAAFQM/SKKaAZqjJeM/s1600/IMG_0950.JPG
[ph5]: http://3.bp.blogspot.com/-wt6bRO3uSnk/U3l62f1nzJI/AAAAAAAAFQM/BgYPCyLONmg/s1600/IMG_0860.JPG
[ph6]: http://1.bp.blogspot.com/-3aolzt_xU6c/U3l9N6LvUlI/AAAAAAAAFQg/autKc99HjZs/s1600/IMG_1109.JPG
[ph7]: http://3.bp.blogspot.com/-R-f9vggkSCc/U3l-0Z_bsRI/AAAAAAAAFRI/xhSnjePdDh0/s1600/IMG_1142.JPG
[ph8]: http://3.bp.blogspot.com/-5p9z4h-Z4XE/U3l-0aCiESI/AAAAAAAAFRI/oFSr83Ggyic/s1600/IMG_1139.JPG
[ph9]: http://3.bp.blogspot.com/-SLZcFxYHv8I/U3l_FCB3Q_I/AAAAAAAAFRQ/BwSCPWrMhrQ/s1600/IMG_1136.JPG

[butan1]: http://4.bp.blogspot.com/-JG6Pt8TdV3M/U3l_iipY6tI/AAAAAAAAFRY/nSPn5ielRSo/s1600/IMG_1169.JPG
[butan2]: http://2.bp.blogspot.com/-vLVY1DXEfXE/U3mADJeDzxI/AAAAAAAAFRk/iHylGEdOl2g/s1600/IMG_1177.JPG
[butan3]: http://1.bp.blogspot.com/-hYlakBiK-Fg/U3mADLPSD2I/AAAAAAAAFRk/bFwLMEaipjk/s1600/IMG_1185.JPG
[butan4]: http://2.bp.blogspot.com/-GcND4X5Z1do/U3l_inabHrI/AAAAAAAAFRY/xCSMJGLwUX4/s1600/IMG_1167.JPG

[hanami1]: http://1.bp.blogspot.com/-ivIoXWJru_s/U3mAeBX94qI/AAAAAAAAFRs/HUsnl7qCuSk/s1600/IMG_1199.JPG
[hanami2]: http://2.bp.blogspot.com/-HGGGTXHdsNg/U3mAeJiikqI/AAAAAAAAFRs/uegbnj3gu2U/s1600/IMG_1202.JPG
[hanami3]: http://2.bp.blogspot.com/-mQI5TiurK4E/U3mA478-fkI/AAAAAAAAFR0/BldCUjcxwr8/s1600/IMG_1208.JPG
[hanami4]: http://2.bp.blogspot.com/-hMMSfY9J4XE/U3mA46THbcI/AAAAAAAAFR0/yBskpC1d5eM/s1600/IMG_1201.JPG

[gw1]: http://4.bp.blogspot.com/-Qh9yH6nL1dQ/U3mCSs2P2bI/AAAAAAAAFUI/Ai6sViYfwTU/s1600/10264281_10201998723306372_1800795441675202874_n.jpg
[gw2]: http://4.bp.blogspot.com/-zS6fYnmvt_4/U3mG_NXDasI/AAAAAAAAFY4/gSI1sekhfaI/s1600/10339368_10102654025937598_7907960078507417525_o.jpg

[boat1]: http://4.bp.blogspot.com/-PKOaYGtzS0k/U3mD1dObDwI/AAAAAAAAFYk/Un4UNvc_zIE/s1600/IMG_1289.JPG
[boat2]: http://1.bp.blogspot.com/-MVCBD3Urk54/U3mD1cdAQ-I/AAAAAAAAFYk/ev2NHZKK_Gk/s1600/IMG_1295.JPG

[block]: https://ajet.net/regional-ajet-chapters#block-6-kansai
[shin]: https://en.wikipedia.org/wiki/Shinsaibashi
