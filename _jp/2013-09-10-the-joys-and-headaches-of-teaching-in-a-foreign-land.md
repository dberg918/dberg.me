---
layout: post
title: "the joys and headaches of teaching in a foreign land"
date:   2013-09-10 12:00:00 +0900
---

<div id="sensei" class="popup-translation" data-role="popup">
<h1>先生</h1>
<h2>せんせい</h2>
<h3>sensei</h3>
<p>teacher</p>
</div>

<div id="mext" class="popup-translation" data-role="popup">
<h1>MEXT</h1>
<h3>Ministry of Education, Culture, Sports, Science, and Technology</h3>
<p>This is the top of the food chain in Japan regarding education policy. They set guidelines for how the education system is structured, and the prefectures of Japan implement them accordingly. I can only assume the X in MEXT came from trying to combine the sounds made by the C-S-S letters in the middle of the title.</p>
</div>

[comment]: # (Please read <a href="http://my2senseiworth.blogspot.jp/2013/09/stream-of-consciousness.html"><em>Learning How to Teach</em></a>, a wonderful blog entry by my friend Adam, who is currently teaching up in Fukushima Prefecture. We went to college together at the University of Maryland and were a part of the same Christian fellowship on campus, and now we've both ended up in Japan at the same time, rather coincidentally. Although he works for a different organization, and teaches at a junior high school rather than a senior high school, our jobs are just about identical. In his post, he talks about some of the differences between Japanese and American culture, and also has some great insights on what Japanese students are like. I'm finding lots of this to be true in my own situation. I'm writing this as a sort of reflection on his entry.)

Teaching is a difficult job. It requires intricate planning, a keen sense of time, a demeanor that commands respect and attention, and a spirit that invites participation and engagement. On top of that, you have to be able to improvise when things don't go as planned, deal with classrooms that spiral into chaos, and identify students that are falling behind and help them catch up. Luckily, I am teaching in Japan, where the <a class="translation" href="#sensei" data-rel="popup" data-transition="pop" title="せんせい, sensei, teacher">先生</a> is revered universally as a very important position in society, so the work I do is greatly appreciated by others in the community. I'm also technically a government employee, another position that is well-respected here. You could just about say the opposite of the same kinds of positions in America these days, although I think many of us have tremendous respect for our teachers, on condition that they are good at what they do.

For Japanese students, English is a very difficult subject. As you might suspect, it is difficult for the same reasons we as English speakers would consider Japanese to be a difficult subject; the grammar and sentence structure are very different between the languages, and each respective writing system uses completely different alphabets and/or characters. My experience learning Swahili, in contrast, was much more pleasant because these two things were relatively the same with respect to English. While it feels like you have to switch your brain into a different mode to use a foreign language at first, trying to go between Japanese and English feels more like making your brain do a back flip half-gainer off the high dive. Sometimes it's a challenge just to get the words to come out of your mouth (although maybe I should [take my own advice][1] in regards to this matter).

As of 2012, English as a school subject in high schools has become, or now should be, even harder, as <a class="translation" href="#mext" data-rel="popup" data-transition="pop">MEXT</a> has decreed it should be taught *in English*, as opposed to conducting the class in Japanese while offering opportunities to speak English. This not only puts more stress on the students, but the teachers as well. I can't speak of how widely this is the case, but the teachers I work with still use Japanese in their classes, although the higher the level, the more English they tend to use.

I agree with the Ministry that English class should be taught in English, but it can't be done by decree. Having an Assistant Language Teacher from the JET Program at your school certainly helps to make it possible, and perhaps this was the Ministry's intention, but not every school has a JET, and I don't think the problem can be solved by asking the JET Program for more people to fill in the gaps. What I hope to see is MEXT seeking to educate and support its JTEs, perhaps through organizing professional development seminars, or providing tools for continued language learning. This is certainly something that JETs can get involved in, as they are a valuable resource for language knowledge and teaching strategies. And in my opinion, this is how JETs should be thought of---resources, not solutions to a problem.

Despite the challenges, I think teaching English in Japan will be incredibly rewarding, just as I believe learning Japanese will be in my own time. Language is one of the things that makes us feel like humans should be split up into several species or groups, despite the scientific evidence that we are all much more alike than different. In my mind, it isn't until we personally break one of our own language barriers that this really becomes apparent, and being a vessel for this realization in another is nothing short of life-affirming.

[1]: /tz/2009-11-07-a-little-tusker-loosens-the-tongue
[2]: /jp/abbreviations#mext
