---
layout: post
title: "kyoto travels: in photos"
date:   2013-10-21 12:00:00 +0900
---

<div id="kyouto" class="popup-translation" data-role="popup">
<h1>京都市</h1>
<h2>きょうとし</h2>
<h3>Kyouto-shi</h3>
<p>Kyoto City</p>
</div>

This past Friday and Saturday I made an overnight visit to Japan's former capital, <a class="translation" href="#kyouto" data-rel="popup" data-transition="pop" title="きょうとし, Kyouto-shi, Kyoto City">京都市</a>, to see my uncles and take in the sights around town. Rather than bore you with a laundry list of places I went and things I saw, I will let the pictures speak for me. Needless to say, Kyoto is a gorgeous city with a ton of history.

|:-----------------------------------------------------------:|
|          [![Golden Temple][kinkakuji]][kinkakuji]           |
|:-----------------------------------------------------------:|
| <a title="きんかくじ, Kinkaku-ji">金閣寺</a>, the Golden Temple |

|:-----------------------------------------------------------:|
|          [![Rock Temple][ryoanji]][ryoanji]           |
|:-----------------------------------------------------------:|
| <a title="りょうあんじ, Ryōan-ji">竜安寺</a>, temple featuring a zen rock garden |

|:-----------------------------------------------------------:|
|          [![Silver Temple][ginkakuji]][ginkakuji]           |
|:-----------------------------------------------------------:|
| <a title="ぎんかくじ, Ginkaku-ji">銀閣寺</a>, the Silver Temple |

|:-----------------------------------------------------------:|
|          [![Kyoto Taste][taste]][taste]           |
|:-----------------------------------------------------------:|
| A seasonal taste of Kyoto, featuring Matcha Sorbet with roasted soy flour |

|:-----------------------------------------------------------:|
|          [![Mountain Path][fushimi]][fushimi]           |
|:-----------------------------------------------------------:|
| <a title="ふしみいなりたいしゃ, Fushimi Inari Taisha">伏見稲荷大社</a>, mountain path with thousands of red <a title="とりい, torii">鳥居</a> gates |

|:-----------------------------------------------------------:|
|          [![Clear Water Temple][kiyomizu]][kiyomizu]           |
|:-----------------------------------------------------------:|
| <a title="きよみずでら, Kiyomizu-dera">清水寺</a>, large temple built on a hillside and supported with giant pillars |

|:-----------------------------------------------------------:|
|          [![Tall Platform Temple][kodaiji]][kodaiji]           |
|:-----------------------------------------------------------:|
| <a title="こうだいじ, Kōdai-ji">高台寺</a>, Buddhist temple established by a widow in honor of her husband |

|:-----------------------------------------------------------:|
|          [![Rickshaw][jinrikisha]][jinrikisha]           |
|:-----------------------------------------------------------:|
| <a title="じんりきしゃ, jinrikisha">人力車</a>, a popular mode of transport in certain parts of Kyoto |

[kinkakuji]: http://2.bp.blogspot.com/-PDQZQPinQrg/UypUI_XTN9I/AAAAAAAADhQ/Z8gcWRVSQ08/s1600/IMG_0427.JPG
[ryoanji]: http://4.bp.blogspot.com/-sBaq4qv3SzY/UypUViNM38I/AAAAAAAADhY/-q9CjURO3rQ/s1600/IMG_0435.JPG
[ginkakuji]: http://4.bp.blogspot.com/-r26EvCNIHUw/UypUmFquYjI/AAAAAAAADhg/qPR3d4N_SZo/s1600/IMG_0428.JPG
[taste]: http://4.bp.blogspot.com/-rNjcmYkcCIE/UypUw67DARI/AAAAAAAADho/1elVUhsj1c0/s1600/IMG_0430.JPG
[fushimi]: http://3.bp.blogspot.com/-8NzrnlcRuDA/UypU7K79gTI/AAAAAAAADhw/QqqO8ciHrik/s1600/IMG_0431.JPG
[kiyomizu]: http://1.bp.blogspot.com/-zNpYeMIAstI/UypVE08Ex2I/AAAAAAAADh4/JctKTBUkkXU/s1600/IMG_0432.JPG
[kodaiji]: http://3.bp.blogspot.com/-Mjenz-sJ6Sw/UypVQTArwYI/AAAAAAAADiA/4D80_YOumcw/s1600/IMG_0433.JPG
[jinrikisha]: http://1.bp.blogspot.com/-1LuV3Bs5A8c/UypVa0tnkKI/AAAAAAAADiI/OJt8qLnYwqk/s1600/IMG_0434.JPG
