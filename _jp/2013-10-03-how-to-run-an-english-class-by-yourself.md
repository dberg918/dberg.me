---
layout: post
title: "how to run an english class by yourself"
date:   2013-10-03 12:00:00 +0900
---

<div id="ess" class="popup-translation" data-role="popup">
<h1>ESS</h1>
<h3>English Speaking Society</h3>
<p>This is an after-school activity that most JETs are often obliged to take part in. The club can be run in a number of different ways—you can give a presentation on your home country's culture or even have them listen to popular music—but the objective is to expose the club members to another side of English they may not get to experience in the classroom, which is usually about learning grammar points and vocabulary for their exams.</p>
</div>

If you had asked me how to do this yesterday, I would've had no idea. If you ask me now, I still wouldn't have any idea. The only difference is that I was thrust into this position during 1st period this morning. My JTE had to deal with some school administrative issues, so he asked me to cover his 3rd Year (seniors) Writing class. My only direction was to perhaps try something involving listening. Hmm...

What I ended up doing was playing some games I tried out last night in my <a class="translation" href="#ess" data-rel="popup" data-transition="pop">ESS</a> club meeting, which was a rag-tag bunch of 1st and 2nd Year kids that only came in because I enticed them with chocolate. The first was a warm-up; I just call it Word Association, but I should probably think of a cooler name. It is exactly what it sounds like---give the students a word, then go around the room and have them say a related word. It can be anything from synonyms to antonyms, or nouns to an adjective, or an adjective to a noun, or a verb to a noun, whatever. It takes a little while at the beginning, but the idea is to get them responding quickly enough that they begin to think in English.
 
Then we moved on to the main event: Hot Seat. In this game, you split the class into two teams and set up two chairs in the front of the room, facing away from the chalkboard. Each team picks a student to put in the Hot Seat, and you write a word on the board. The rest of the team has to get the student in the Hot Seat to say the word. Whoever says the word first gets a point for their team. The idea here is for them to harness the quick thinking they acquired in the warm-up, saying words or phrases related to the word on the board. Needless to say, things can get pretty heated in the classroom. In fact, halfway through our game of Hot Seat, another teacher peeked in and told us to quiet down a little.

You do have to lay down a couple ground rules, though. They have to get their student to guess using only their English---that means no gestures, *or Japanese*. That last one is important, and you have to be pretty vigilant each round to make sure they aren't sneaking any words in. If you hear a student use Japanese, or see one using gestures, you can penalize him/her in one of two ways; give the other team a point, or he/she is not allowed to speak for the rest of the round.

After a few rounds with some easy words, you can try harder ones, but make sure it is in their vocabulary. If you see puzzled looks, or everyone reaches for their electronic dictionary, you might want to pick another word. For example, I tried the word "Beijing" and neither student could get it. I even helped them out with "Olympics, 2008, China, city." I was pretty surprised, but I guess they don't know much Chinese geography, and perhaps they were too young to be interested in the Olympics in 2008. The other way to make it slightly more difficult is to have them guess short phrases, like "I love (something)," or "I hate (something)." You can make it interesting, so I did "I love chopsticks" and "I hate Japanese food." The sillier the better. It was a lot of fun to watch their strategies for these phrases, breaking it down by word and using related words to fill in each part of the phrase.

It worked pretty well. Well, considering we had to be told to quiet down because everyone was so riled up, I'd say it worked really well! That said, I have no idea how to teach an *actual* English class by myself.

