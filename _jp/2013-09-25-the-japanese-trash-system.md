---
layout: post
title: "the japanese trash system"
date:   2013-09-25 12:00:00 +0900
---

<div id="konnichiwa" class="popup-translation" data-role="popup">
<h1>こんにちわ！</h1>
<h2>konnichiwa!</h2>
<p>good afternoon!</p>
</div>
 
<div id="doko" class="popup-translation" data-role="popup">
<h1>どこ</h1>
<h2>doko</h2>
<p>where</p>
</div>

<div id="gomi" class="popup-translation" data-role="popup">
<h1>ごみ</h1>
<h2>gomi</h2>
<p>trash</p>
</div>

Pretend you are me for a moment. Having just arrived in Japan, there is so much you need to learn. Things you don't consciously think about until they become a physical problem, taking up space on your kitchen counter, piling up on your dining table. You went to the store to buy some important items, like a bed, some utensils, and food, but of course you forgot the all-important, often forgotten receptacle, a trash can. "Wait!" you think. "Perhaps there is a dumpster somewhere around my apartment complex. I can just go straight there and skip the middleman!" So you look around. Nothing in the backyard, and nothing around the side. Confused, you decide to ask the teacher who lives in the apartment next to yours. He's an English teacher, surely he'll be able to tell you where things are. So you ring the door bell...and his wife answers. 「<a class="translation" href="#konnichiwa" data-rel="popup" data-transition="pop" title="konnichiwa!, good afternoon!">こんにちわ！</a>」, she says. You ask for her husband by name, and she replies in Japanese and shakes her head. You assume she said something like, "he's not here right now." Figuring it shouldn't be too hard to ask "where do you put trash?" using gestures, you make silly, exaggerated motions with the empty bag of potato chips in your hand, along with one of the question words in Japanese that you know, <a class="translation" href="#doko" data-rel="popup" data-transition="pop" title="doko, where">どこ</a>. She doesn't understand, so you keep repeating the same motion over and over, until you extract the Japanese word for trash (<a class="translation" href="#gomi" data-rel="popup" data-transition="pop" title="gomi, trash">ごみ</a>) out of her. She catches on, then blurts out a string of sentences that make absolutely no sense. After she shows you her trash bins, and you try to tell her you don't have any, she resolves to interrupt her husband's baseball practice to have him come explain it to you.
 
Whether you've lived in the same place your whole life, or you've never lived in the same place for more than a year, culture shock is something we all experience in our lifetime. It doesn't even necessarily have to be in a different country; just visiting another area within your own country can have you reeling for "home." Being in Japan, it is an inevitable fact of life—it's going to happen sooner or later. People tend to think of it with a negative connotation, but that isn't always the case. Although rare, things can be different in a way you think is an improvement on what you're used to.

<div class="halfie">
[![Trash bins][bins]][bins] 
[![Trash bags][bags]][bags] 
</div>

With regards to the trash system in Japan, which is the topic of this entry, I have mixed feelings, but mostly due to the fact that I don't fully understand it yet. This misunderstanding is, in fact, partially why I'm experiencing the culture shock in the first place. The system is rather complex, and since I don't know how to read Japanese, I am at a considerable learning disadvantage. Even so, I know enough to keep trash from piling up in my house...that is, except for the mass of cardboard boxes underneath my kitchen table. I still need to figure out how to recycle those.

What's strange is I've been told some of this doesn't necessarily apply to my small mountain town. I hear that other more populated areas are more strict about their trash, whereas ours is slightly more lenient towards mistakes. For the most part, in any case, this is how the trash system works throughout the country.

First, let's talk trash bins. In my house, I have three. No, they aren't just bins for different rooms, they are for keeping the different types of trash separate. The black lid on the left is for burnable trash. In America, this is basically your average trash can. You throw food waste, dust, etc. in this guy. The green lid on the right is for plastic, although plastic bottles are a sort of exception. You put the caps and labels from plastic bottles in the green bin, but the bottles themselves are separated from the rest of the plastic trash, as they are recycled separately. Plastic bottles go in one of the compartments in the small bin. The other side is for cans. Cans come in two varieties—aluminum and steel. This is one of the areas I'm unclear on; I don't know if all cans are recycled together, or if they have to be separated. The recycling bags (explained below) seem to suggest they are separated, but the guide book (also below) suggests you can put them together. What I don't have is a bin for glass, since I haven't produced much waste on the glass front. Should I start drinking copious amounts of Asahi Dry out of glass bottles—available at my local supermarket—I might consider making an addition to my trash nook, although I will probably just stick to aluminum cans, which are more widely available (vending machines even).

Now let's look at what trash actually goes in. The trash system uses its own set of bags. You can buy them at most supermarkets, convenience stores, and home improvement stores, at about &yen;300 for 20 bags. Yellow bags are for burnable trash, while the clear bags are for recycling of various kinds. There is a table printed on the clear bags that lists the different types of recycling. On these bags, you have to mark with a circle what type of recycling is going in that particular bag. There are also blue bags, which are for large items that don't fit into the standard bags, as well as stickers and paper bags for paper recycling, but I haven't seen any of these in any store I've been to so far. Before you start filling it with trash, you write your name on the bag. From what I understand, putting your name on the bag allows the trash men to notify you when you've done something wrong, e.g. you put a plastic bottle in your yellow trash bag. Privacy advocates would probably cry foul on this, as it clearly labels what was once in your possession, including documents that may contain sensitive information. I think as long as you follow standard safe practices for such things, like shredding, it is no different than America, where your trash is linked to you because it sits out in front of your house.

Another small thing that is slightly different here—people put their trash out in the morning, not the night before. This is because your trash bag doesn't sit in a dumpster waiting to be picked up. It sits on the curb without anything around it. As it isn't contained very well, the smell can attract animals who may be enticed to rip open the bag and browse its contents. To prevent this, the convention is to have your bag on the curb by 8:00am, so that the trash men can pick it up at 8:30am.﻿﻿

<div class="halfie">
[![Writing my name on the bag][write]][write]
[![Trash guidebook][book]][book]
</div>

Inevitably, there are cases where you have trash to throw away and it isn't entirely clear what bag it goes in because you don't know what it's made of. Most prepackaged foods and vending machine items are well-labelled; you can usually look for a symbol that tells you how to dispose of it. For anything that isn't properly labelled, you whip out your handy-dandy trash guide! It has a large index of items in the back which identifies how everything should be disposed. Given I don't know the names of most things in Japanese yet, it has worked pretty well as a vocab list of words to learn. It also has some pictures and diagrams in the front for some of the more common trash, including how to properly "clean" things before you put it in the bag.

Finally, there is the color-coded trash schedule. Like most developed countries, trash is picked up at scheduled times throughout the month, and depending on the type of waste, the day of the week and frequency of pick-up is slightly different. Burnable trash is picked up every Monday, without exception. Last Monday was a holiday here (Respect for the Aged Day), and there was a typhoon no less, and the trash was still picked up on time. Plastic recycling is picked up twice a month, usually on Wednesdays. Bottles and cans are only picked up once a month, also on Wednesday. Other stuff, like big items and paper, I'm not sure. The calendar doesn't match up entirely with what's in the guidebook, so I'm a little confused at the moment, but since I haven't had any big things to deal with yet, it hasn't been much of an issue.

[![Trash schedule][schedule]][schedule]

Despite not knowing the language, and never having to navigate such a system on my own yet (we just burned everything ourselves in Tanzania), I really like how organized it is. I also appreciate that this is a public service throughout the country, not a private enterprise, and that they take recycling very seriously. Overall, I approve, and I'm sure the more I learn about it, the more I'll like it.

[bins]: http://2.bp.blogspot.com/-qJJSUOVJWbc/UjbLDKL7aUI/AAAAAAAAAkI/aEo6jRQ8UMU/s1600/IMG_0152.JPG
[bags]: http://2.bp.blogspot.com/-zSQywlvUwOI/UjbRoZ38_LI/AAAAAAAAAkY/-8VCMaoRBe4/s1600/IMG_0145.JPG
[write]: http://3.bp.blogspot.com/-_K1Xj_NWAMI/UjqXf2y3JrI/AAAAAAAAA_U/Ir-bHMjxVF0/s1600/IMG_0160.JPG
[book]: http://3.bp.blogspot.com/-orK_2meVnXU/UjcV1Y-UfBI/AAAAAAAAAk8/0iDqnaabszA/s1600/IMG_0148.JPG
[schedule]: http://3.bp.blogspot.com/-nnna6bwHrrw/UypW_BXfKlI/AAAAAAAADiU/murrZzEt4W8/s1600/IMG_0149.JPG
