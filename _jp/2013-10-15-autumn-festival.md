---
layout: post
title: "autumn festival"
date:   2013-10-15 12:00:00 +0900
---

<div id="akimatsuri" class="popup-translation" data-role="popup">
<h1>秋祭り</h1>
<h2>あき まつり</h2>
<h3>aki matsuri</h3>
<p>Autumn Festival</p>
</div>

<div id="mikoshi" class="popup-translation" data-role="popup">
<h1>神輿</h1>
<h2>みこし</h2>
<h3>mikoshi</h3>
<p>portable shrine</p>
</div>

<div id="oomori" class="popup-translation" data-role="popup">
<h1>大森</h1>
<h2>おおもり</h2>
<p>oomori</p>
</div>

This weekend was the <a class="translation" href="#akimatsuri" data-rel="popup" data-transition="pop" title="あきまつり, akimatsuri, Autumn Festival">秋祭り</a> in my town. I had only heard about it from the other ALT that lives in my area earlier in the week, so I had no idea what it entailed. Turns out it was mostly just townspeople walking through the streets carrying a <a class="translation" href="#mikoshi" data-rel="popup" data-transition="pop" title="みこし, mikoshi, portable shrine">神輿</a>, while somenone hit a drum to cue everyone to yell something. What's interesting is that there were a few different festivals happening at the same time, and the one you went to depended on where you lived in town. This is because there are several different shrines in my town, and each one had their own festival.

[![Autumn Festival][matsuri]][matsuri]

My shrine is basically right across the street from where I live. It's called the <a class="translation" href="#oomori" data-rel="popup" data-transition="pop" title="おおもり, ōmori">大森</a> shrine, which translates literally to "big forest." I attended the festivities there on Saturday night. I didn't stay for very long because I didn't know most of the people that were there (note to self: get more involved in my community), but there was food, a fire, and people performing some kind of ceremony inside the shrine. I watched a little bit of it from a distance. I didn't hear what they were saying, but there was lots of bowing and kneeling. An old man that was standing next to me participated, even though he wasn't inside with everyone else.

Though my visit was short, it was a good look into one aspect of Japanese culture. This festival is the first of many to come!

[matsuri]: http://2.bp.blogspot.com/-xK8QA0gMJlI/UypZjs5pfXI/AAAAAAAADig/r37BtYL1ibU/s1600/IMG_0315.JPG
