---
layout: post
title: "teaching english beyond the classroom"
date:   2014-09-24 12:00:00 +0900
---

<div id="eikaiwa" class="popup-translation" data-role="popup">
<h1>英会話</h1>
<h2>えいかいわ</h2>
<h3>eikaiwa</h3>
<p>English conversation class</p>
</div>

<div id="shi" class="popup-translation" data-role="popup">
<h1>し</h1>
<h2>shi</h2>
<p>hiragana character, part of the hiragana syllabary</p>
</div>

This past Tuesday marked an important moment in my work as a JET here in Japan. Along with a fellow ALT, I started an <a class="translation" href="#eikaiwa" data-rel="popup" data-transition="pop" title="えいかいわ, eikaiwa, English conversation class">英会話</a> in my hometown. While most ALTs will find this rather mundane (many JETs host classes of their own), it's exciting for me because to my knowledge, my town has never had one before. Having something I can point at and say "I started that!" is nice, as opposed to taking over responsibility for a project somebody else had a vision for. This is also one of the rare opportunities I get to meet new people and make friends in my community; unlike Tanzania, people aren't too keen about random visitors coming to their home for no particular reason but to see what they're up to. Unfortunately, I've had to restrain myself from sharing that particular part of Tanzanian culture with my town, as I've told many of my Japanese friends. 

[![Class 1][cl1]][cl1]

Our first class went very well. We had 4 students come, which is a pretty good turnout considering how little and last-minute we advertised. I hope we can expand the number a bit and garner more interest locally; two of our students made long drives from out of town. As you might expect, we started our inaugural class with self-introductions, which was probably my favorite part. I got to learn so many interesting things about our students! One runs a mushroom farm (and supplies our local supermarket!), another trains rescued dogs, and another was a JICA volunteer in Chile. As an added bonus, they were speaking English so I was easily able to understand it! After self-introductions, we played a quick game and my fellow teacher gave a nice pronunciation explanation about the difference between <a class="translation" href="#shi" data-rel="popup" data-transition="pop" title="shi, part of the hiragana syllabary">し</a> in Japanese and "si" in English. Before we knew it, 90 minutes had vanished and class was over.

[![Class 2][cl2]][cl2]

For now, we've decided to teach this class once a month and simply plan the dates one month at a time. We don't have any particular objective yet, but once we get to know our students better, we will probably consider helping them set goals for their language learning.
 
[cl1]: http://3.bp.blogspot.com/-Zo8tBIdJDC0/VCIY72Ok2rI/AAAAAAAAHf0/R4T3Q5gnXk4/s1600/DSC_0111.JPG
[cl2]: http://4.bp.blogspot.com/-voEbdUZTXg0/VCIZd5k48bI/AAAAAAAAHf8/jofaJYi60mo/s1600/DSC_0123.JPG
