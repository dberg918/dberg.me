---
layout: post
title: "yep, it's winter"
date:   2013-12-21 12:00:00 +0900
---

Today, I was supposed to participate in a golf tournament with a couple of teachers and some of the students from our school's golf team, but I decided last-minute to back out and stay home. I know, missing an opportunity to play golf?? What's wrong with me? I must be crazy! In my defense, this is what it looks like in my town today:

[![Snowy Day][snow]][snow]

Frankly, the fact that they're still going to play is pretty bonkers, but apparently, it's not raining or snowing in the neighboring district where the golf course is. That's still something I have to wrap my head around living up in these mountains; weather is extremely local and follows certain trends, but is very much subject to change at a moment's notice. It will be sunny one minute and then all of a sudden, the sky will open up. In fact, over the past month or so, I've seen both at the same time on numerous occasions---yesterday being the most recent.

Either way, it's not going to be pleasant on the golf course today. I have no regrets.

[snow]: http://3.bp.blogspot.com/-qq5Kkj2V6xQ/UypLPWTtQ8I/AAAAAAAADfw/Rw8vh9FvSi0/s1600/IMG_0604.JPG
