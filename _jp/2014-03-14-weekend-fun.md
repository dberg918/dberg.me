---
layout: post
title: "weekend fun"
date:   2014-03-14 12:00:00 +0900
---

<div id="naita" class="popup-translation" data-role="popup">
<h1>ナイター</h1>
<h2>naita-</h2>
<p>nighttime skiing</p>
</div>

<div id="kotatsu" class="popup-translation" data-role="popup">
<h1>炬燵</h1>
<h2>こたつ</h2>
<h3>kotatsu</h3>
<p>small table with an electric heating element on the underside</p>
</div>

<div id="sig" class="popup-translation" data-role="popup">
<h1>SIG</h1>
<h3>Special Interest Group</h3>
<p>You may already know this acronym from some other context. With respect to the JET Programme, these are groups formed for the JET community, by the JET community. Many of them revolve around religious beliefs, but there are others that relate to personal ancestry as well as sexual orientation. Still others are formed for the purpose of organizing sporting events and tournaments.</p>
</div>

<div id="npb" class="popup-translation" data-role="popup">
<h1>NPB</h1>
<h3>Nippon Professional Baseball</h3>
<p>(the MLB of Japan)</p>
</div>

Sorry I've been a little off the blog a while. I've been pretty busy and haven't had as much time to sit down and write as I'd like. Thankfully, my school schedule is starting to slow down as we near the end of the final term, so let me catch you up on what's been happening in my neck of the woods.

### White nights on the slopes

<div class="halfie">
[![Slopes 1][slope1]][slope1]
[![Slopes 2][slope2]][slope2]
</div>

Since the start of January, I have been making regular trips to the local ski slope to indulge in a new-found hobby. To add to the cool factor, most of the skiing I do is at night! The place where I ski holds a weekly <a class="translation" href="#naita" data-rel="popup" data-transition="pop" title="naita-, nighttime skiing">ナイター</a> on Friday nights, where they illuminate the beginner hills with high-powered multicolored flood lights and blast pop music over their PA system. It's a very popular event amongst the younger generation, so it's quite common to see lots of high school and college-aged kids out having a good time. The night ski also tends to attract very talented snowboarders, many which you see doing ridiculous twist moves and crazy jumps, since the beginner hills are otherwise pretty boring for them.

### Bi-polar weather in the mountains

<div class="halfie">
[![Weather 1][weather1]][weather1]
[![Weather 2][weather2]][weather2]
</div>

It's been quite snowy at the base of the mountains, too, although it has been trying to warm up in recent weeks. We had a very warm end to January, but February ushered in another blast of winter. We've probably seen snow accumulate and melt at least 5 or 6 times this winter! Even this morning, after it rained all day yesterday and hovered around 10C, I woke up to snow-capped trees and a light layer of snow around my apartment. While I've enjoyed seeing the powder on a weekly basis, I'm looking forward to the day where I don't need to huddle under my <a class="translation" href="#kotatsu" data-rel="popup" data-transition="pop" title="こたつ, kotatsu, small table with an electric heating element on the underside">炬燵</a> to stay warm!

### Trip to Tottori

<div class="halfie">
[![Tottori 1][tottori1]][tottori1]
[![Tottori 2][tottori2]][tottori2]
</div>

In late February, I made an excursion up to Yonago in Tottori Prefecture to see my friend Pete make his DJ debut. It was a very intimate event, both in the size of the venue and how close the Tottori folks are with one another. I made a lot of new friends! It was also my first long drive alone in Japan, which turned out to be a beautiful trip through the countryside of western Hyogo. I also stopped by one of the famous lookouts in Chikusa on the way back (below right). Hopefully I'll be making that trip again sometime soon!
 
<div class="halfie">
[![Tottori 3][tottori3]][tottori3]
[![Tottori 4][tottori4]][tottori4]
</div>

### Graduation Day

<div class="halfie">
[![Graduation 1][grad1]][grad1]
[![Graduation 2][grad2]][grad2]
</div>

We said a bittersweet goodbye to our 3rd year students on the last day of February.

[comment]: # (You can read the piece I wrote on my school's <a href="http://www.chikusa-hs.jp/index.php?action=pages_view_main&amp;active_action=journal_view_main_detail&amp;post_id=518&amp;comment_flag=1&amp;block_id=177#_177">English blog</a> for a recap of the day's events.)
 
### Osaka Charity Concert

This past Saturday, I participated in a charity concert! JET Christian Fellowship, the <a class="translation" href="#sig" data-rel="popup" data-transition="pop">SIG</a> for which I maintain a website, is raising money for Typhoon Haiyan disaster relief in the Philippines. They're sending a group over at the end of March (including me!) to deliver supplies, help out with cleanup, and run some games and activities with the local kids. At the concert, I played a few tunes on guitar in front of about 50 people, plus I met a ton of talented musicians! I wasn't able to snap any pictures, but I'll try to share some on Facebook if I find any.
 
### Pre-season Baseball in Himeji

<div class="halfie">
[![Baseball 1][base1]][base1]
[![Baseball 2][base2]][base2]
</div>

After the charity concert on Saturday, I took a short drive to see an <a class="translation" href="#npb" data-rel="popup" data-transition="pop">NPB</a> pre-season game at the new baseball stadium in Himeji. The traffic was pretty terrible and I had to drive around a while to find parking, but I managed to get there before the end of the 2nd inning. The Orix Buffaloes, who normally play at the Osaka Dome, hosted the Nippon-Ham Fighters from Hokkaido. I ended up in the cheering section for the Fighters purely by accident.

Although the Buffaloes came back dramatically in the 6th to tie the game with a big 2-run double off the left field wall, their pitching came undone in the later innings. The winning run came home on a base on balls in the 8th. Orix did threaten in the bottom of the 9th, though. After the first batter was hit by a pitch, a sac bunt moved the runner to 2nd with 1 out. The Fighters' 3rd baseman then made a game-saving play on a sharp grounder in the hole, holding the runner on 2nd and making the out at 1st. The next batter struck out looking to end the game.

[![Baseball 3][base3]][base3]

Okay, you're all caught up now. Expect another post after the start of April at the very least. I'm sure I'll have some stories to tell about my time in the Philippines!

[slope1]: http://4.bp.blogspot.com/-_0egV7V22II/UyKxkwI6NxI/AAAAAAAADas/LRIOcYhjWxA/s1600/IMG_0687.JPG
[slope2]: http://1.bp.blogspot.com/-68kkKxHolwM/UyKxvea8Z2I/AAAAAAAADa0/dkx7VaQJufk/s1600/IMG_0699.JPG

[weather1]: http://1.bp.blogspot.com/-X5T9mrY0aDo/UyKyOTO9tZI/AAAAAAAADa8/Ly1jFvgPPGk/s1600/IMG_0709.JPG
[weather2]: http://3.bp.blogspot.com/-0DLUXidPXEE/UyKyU6eCiKI/AAAAAAAADbE/m2UPZBLfmGc/s1600/IMG_0782.JPG

[tottori1]: http://2.bp.blogspot.com/-2p4WrZHtZXk/UyKys6AnMxI/AAAAAAAADbU/0-PYonKflG8/s1600/IMG_0746.JPG
[tottori2]: http://3.bp.blogspot.com/-GdDBD_HPv6Q/UyKy0TqFOeI/AAAAAAAADbc/oPet17plfqI/s1600/IMG_0754.JPG
[tottori3]: http://1.bp.blogspot.com/-fuY9vm2-tyQ/UyKz0rndrvI/AAAAAAAADbo/Xl9s7ZgoCUY/s1600/IMG_0748.JPG
[tottori4]: http://2.bp.blogspot.com/-wmgrfmnqsL0/UyKz8-YRMiI/AAAAAAAADbw/_yvHxyaMSMw/s1600/IMG_0756.JPG

[grad1]: http://2.bp.blogspot.com/-rkkRH2ZaqD0/UyK0j4zBRVI/AAAAAAAADb4/rESIlfCe3CA/s1600/IMG_0759.JPG
[grad2]: http://1.bp.blogspot.com/-bAQCXM1YegI/UyK0p7QVwrI/AAAAAAAADcA/mn9OEWKO6nM/s1600/IMG_0761.JPG

[base1]: http://1.bp.blogspot.com/-xdPSIzDwCn8/UyL732Kp43I/AAAAAAAADc0/Kc8KorpWOCk/s1600/IMG_0772.JPG
[base2]: http://3.bp.blogspot.com/-26GdtKnNLt0/UyL7xPyf3HI/AAAAAAAADcs/oKDfVC65_dM/s1600/IMG_0773.JPG
[base3]: http://1.bp.blogspot.com/-55hz_SQEfsQ/UyL9-xVG8CI/AAAAAAAADdA/s5NXEbVedNE/s640/IMG_0781.JPG
