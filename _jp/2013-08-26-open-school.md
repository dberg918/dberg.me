---
layout: post
title: "open school"
date:   2013-08-26 12:00:00 +0900
---

This past Tuesday, we held an event called "Open School," where soon-to-graduate students from some of the neighboring junior high schools come to see what their life will be like next year. There didn't seem to be a whole lot of preparation for this---though I did only start working here about 2 weeks ago---but there were a considerable number of activities taking place throughout the school during the 6 hours the students were here.

I took part in a sort of English seminar with my counterpart Matsui-sensei. If you had any doubt that I work at a small school, this should convey it pretty well: We only had 3 students! Partly because it's a rather unpopular subject amongst youngsters, but also because there were only about 30-some junior high students here to begin with. Ten percent isn't a bad pull in the grand scheme of things. We taught them a short self-introduction, including their favorite foods and hobbies, and then we did a role play to show them how to order food at a restaurant.

We also hosted some short sports camps, where students could pick their favorite sport or activity and learn a little bit about the clubs at the high school. Our golf team helped organize a small chipping and putting expo in the gym for about 1 hour, which was quite entertaining to watch. They first divided the gym in half to separate putting and chipping. On the putting side, they rolled out some putting mats they use to practice when it's raining or too cold outside, and the junior high students could take turns and try their luck. The chipping side was much more raucous. There were small chipping mats set up on each end of the court with circular basket nets sitting in the middle as targets. Instead of using real golf balls, which would undoubtedly be hazardous and cause tons of damage to the floors and walls, they used small, whiffle-style golf balls to try and chip into the nets. After the golf team demonstrated the proper technique for chipping, the junior high kids were given wedges and told to swing away. It was apparent that some weren't paying attention during the demo, because a few were swinging as hard as they could, sending low-streaking bullets across the gym. A number of balls went through open windows, and others had students ducking for cover, adding an interesting dodgeball component to the activity.

Later in the day, we took 3 junior high students who expressed interest in joining the golf team to the local country club for some extracurricular practice. We got to go to the driving range, which was actually just a small, net-enclosed area no longer than about 50 or 60 yards. After we finished there, we were required to pick up after ourselves and return the balls to the shed for future use. Then we spent some time on the putting green, before packing up and returning to the school.

All in all, the day was a success! It was my first day with the golf team, and my first time in a classroom setting in almost two years. Feels good to be back!

[comment]: # (In other news, my vice-principal just posted my <a href="http://www.chikusa-hs.jp/jorglsphr-177/#_177">self-introduction</a> on the high school website, if you'd like to take a look. He has asked me to write regularly for the school blog, so bookmark the English version of the site if you're interested!)
