---
layout: post
title: "another adventure, another blog"
date:   2013-07-23 12:00:00 +0900
---

Rather than figuring out the far more important "what I need to pack", I set up a new blog so I can write a complete nonsense post. Now this feels [vaguely familiar][1]...

My flight into Narita International leaves in little more than a week, and I haven't even begun to consider what's going into my bags (maybe just bag?) yet. I suppose this is a sign that packing for a long trip has become something of a challenge for me. A challenge to see how much time I can spend doing other things before all the pent-up nervous energy forces me to take action. Sort of like playing chicken with the departure date. Unfortunately, I don't anticipate Time swerving out of the way at the last second to protect me from being unprepared. I hope something happens soon because I'm supposedly responsible for buying gifts for all my new friends. Note to self: shop for gifts.
 
I guess this is as good a time for an introduction as any. The title of this blog; the "adventure" alluded to in this entry's title; they are all references to my impending departure from these United States of America to pursue a position as an Assistant Language Teacher with the JET Program. I just can't seem to keep my feet on American soil these days. Prior to this engagement, I served as a Peace Corps Volunteer in Tanzania---two years as a math and physics teacher in a secondary school, and one year on extension as a computer "guru" with a high-profile USAID project. I quote "guru" because the title is only relative. I don't really consider myself a guru with computers. I just know how they work better than the folks that grew up with typewriters. Before Peace Corps, I was in college, getting my degree in Aerospace Engineering. While the prospect of a high-paying math and science-based career still eludes me at the moment, I am enjoying the ride that my other, somewhat new-found, interests are providing me.

And so begins another adventure. A new land to explore. A new language to learn. A new journey...for which I can procrastinate another week.

[1]: /tz/2009-08-25-preparing-for-the-journey-i
