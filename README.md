# bergnerd
I’m Dave Berg.  
This is my digital journal for a few topics that I like to write about.

### Acknowledgments
Thanks to [@mdo][1] for the [Hyde theme][2], upon which this blog is based.

[1]: https://twitter.com/mdo
   "Mark Otto on Twitter"
[2]: https://github.com/poole/hyde
   "Hyde - Jekyll theme on GitHub"
