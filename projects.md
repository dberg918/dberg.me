---
layout: page
title: Projects
published: false
---

<h2 class="about">Things I do in my spare time</h2>
Here are some projects I currently work on.

<a href="http://loveydummies.com">
  ![Lovey Dummies](/assets/ld-logo.png)
</a>

### Lovey Dummies
_A podcast about relationships._

Producer, co-host, and website designer.
<hr />

<a href="http://jetchristianfellowship.com">
  ![JET Christian Fellowship](/assets/jcf-logo.png)
</a>

### JET Christian Fellowship
_Christian connections and resources in Japan._

Webmaster and logo designer.
