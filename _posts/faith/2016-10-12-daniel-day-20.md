---
layout: post
title: "(not) daniel fast: day 20"
category: faith
date: 2016-10-12 23:31:00 +0900
---

I'm already starting to lose track of the days, haha. Spent most of my time at 
home, working on personal finances and checking into getting a new phone. Went 
light on food again today, but never really felt hungry. While I am weighing in 
a little lean currently, I seem to have plateaued in the past few days, just 
below the 67kg mark. Not a terrible place to be for me, as this seems to be 
where I feel fit and light. 

Hard to believe tomorrow is Day 21! Even though I've been off the strict Daniel 
fast since the second week, my eating habits have been drastically different. 

*God, give us the strength to go one more day. You're awesome. Amen.*
