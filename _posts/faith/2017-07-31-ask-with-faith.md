---
layout: post
title: "ask with faith"
category: faith
date: 2017-07-31 12:28:00 +0900
---

> あなたがたのうち、知恵に不足している者があれば、その人は、とがめもせずに惜しみなくすべての人に
> 与える神に、願い求めるが良い。そうすれば、与えられるであろう。
> 
> If you need wisdom, ask our generous God, and He will give it to you. He will 
> not rebuke you for asking.  
> &ndash; James 1:5 (NLT)

Just ask. It doesn't matter if it's something you feel you should already know. 
God wants to equip you for the task at hand. All He asks is that we don't treat 
Him like some kind of second opinion (James 1:6). Otherwise, we'll just choose 
whatever option we like the most instead of letting God guide us.

This has been a difficult lesson to learn. It's important to examine the way you 
are asking God for advice because if your heart isn't in the right place, He may 
stay silent.

Consider for a moment if a friend came to you asking for your help 
in a matter. However, based on the way they asked you, it seemed like they 
already had some other idea in mind, or they were playing you against some other 
third party. You probably wouldn't be too inclined to tell them anything, or you 
might respond with, "it sounds like you already know what you want to do, what
are you asking me for?" However, if someone came to you humbly to ask for your 
advice on a matter, you would be happy to help them, since you could sense their 
trust in you and that your opinion matters to them.

So trust in God when you ask for His advice and put your inclinations on the 
altar. He is trustworthy, and He'll give you exactly what you need. And who 
knows, He may ask you to [pick up what you put down][g22]!

[g22]: https://www.biblegateway.com/passage/?search=genesis+22&version=NLT
