---
layout: post
title: "daniel fast: day 5"
category: faith
date: 2016-09-27 21:36:00 +0900
---

Starting to settle in a bit. It was an easy day at work today, so that helped. 
Feeling the stirrings of the Holy Spirit, which is a good sign. Hoping to hear 
from God about our prayers soon.

We made a soba dish for dinner tonight that turned out pretty nice. The normal 
soba broth or dipping sauce usually has some type of fish sauce or, at the very 
least, added sugar, which makes it no good for the Daniel fast, so we made our 
own dressing from peanut butter, garlic, apple cider vinegar, and lemon juice. 

![Soba](https://goo.gl/WnhLil)

Sounded like a weird combination to me, but it actually had a nice flavor. I 
might suggest trying garlic powder for those who'd like to try it though...raw 
garlic was a little too powerful and left a rather long aftertaste.

*Thanks for helping me to settle into this fast, God. And thanks for speaking 
to me and through me in the midst of my hunger. Please continue to give me 
strength and stay close as I close in on the end of week 1. Amen.*
