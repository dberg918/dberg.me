---
layout: post
title: "(not) daniel fast: day 18"
category: faith
date: 2016-10-10 23:59:58 +0900
---

Another lazy day. Since we didn't do much, we didn't eat much either! Just some 
standard faire for breakfast and lunch. Strangely, it was a holiday here in 
Japan as well as in America, for different reasons (Sports Day in Japan).

*Thanks for another day to rest and relax, God. We are gearing up for the last 
push to 21 days. Give us the energy to get through it! Amen.*
