---
layout: post
title: philippians 4:10-18
category: faith
date: 2016-06-08 00:10:00 +0900
---

It is not the generous gift<br />
that God desires,<br />
but the generous heart.

