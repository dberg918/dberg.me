---
layout: post
title: the little johns
category: faith
date: 2016-01-28 13:48:00 +0900
---

I find something quaint and adorable about 2nd and 3rd John. They're both 
one-chapter books in the back of the Bible, less than 15 verses. It makes you 
wonder---whoever included these in the Bible---what they were thinking, 
especially when you're reading through the first few verses. "This guy's just 
writing a personal note to a friend of his...what's this doing in here?" Once 
your read them through, though, you realize they have tremendous purpose. Both 
letters, while bookended mostly by salutations and greetings, contain little 
nuggets of amazing truths within them.

> ...I am not writing you a new command but one we have had from the beginning. 
> I ask that we love one another. And this is love: that we walk in obedience 
> to his commands. As you have heard from the beginning, his command is that 
> you walk in love.
> -- 2 John 1:5-6

Our love for the Lord is demonstrated through obedience. If the Lord hasn't 
commanded it, it isn't love. There's an inextricable link between our obedience 
and the love we demonstrate. In other words, the love we show others through 
obedience to the Lord is the love we receive from God. Is our obedience always 
outwardly demonstrated? Not necessarily, but the effects of our obedience are. 
Certainly when God tells us to be nice to people or help others, we are acting 
out the love He has given us and the effect is visible. But I think even in our 
own personal circumstances---perhaps when we make a decision to trust God's 
promise in something---when we decide to be obedient to the Lord's voice, it 
will ultimately manifest itself through us and our actions.

> Dear friend, do not imitate what is evil but what is good. Anyone who does 
> what is good is from God. Anyone who does what is evil has not seen God.
> -- 3 John 1:11

Revenge is not justice, no matter how sweet the thought of it is. In times when 
we feel persecuted, we must remember that we are irrelevant to the resolution 
of wrongdoing. God is the Judge, and He alone has the right to exact justice, 
which is exactly what He did on the cross. It's already done. All that's left 
for us is to accept God's sacrifice and let it transform us. As a result, we 
will become more like the person "who does what is good."

<em>copied from my [previous blog][haven]</em>

[haven]: http://thethoughthaven.blogspot.jp/2014/05/the-little-johns.html
   "The Little Johns - thought haven"
