---
layout: post
title: "(not) daniel fast: day 9"
category: faith
date: 2016-10-01 23:59:58 +0900
---

What did we do today?

Well, pretty nice little Saturday. First, we went up to the Asahi Sports Center 
and played some badminton with a group of friends. Then, we went to Big Boy 
(didn't know those existed in Japan) for the all-you-can-eat salad bar. After 
that, we did a little shopping at the pharmacy before coming home.

Relaxed and rested at home. Darling Pearl made some oatmeal-banana-walnut 
cookies! Daniel-friendly too!

![Oatmeal Cookies](https://goo.gl/phyB1G)

*God, thanks for a wonderful companion. She plays sports with me, she watches 
TV with me, and she makes cookies for me. Please keep us tightly knitted 
together as we begin to establish the foundations of our family, and keep me 
right-minded so that I can lead it well. Amen.*
