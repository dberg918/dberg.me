---
layout: post
title:  git commit -m initial commit
category: nerd
date:   2016-01-10 16:00:00 +0900
---
Hello world! This is my blog for writing all the computer things I learn about. 
If you can learn along with me, then that's great! If you can laugh at all the 
mistakes I make, then that's also great! This is ultimately for me, but I won't 
be upset if you get something from it, too.
