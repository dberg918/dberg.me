---
layout: post
title: git commit -m how i got hyde running on arch
category: nerd
date: 2016-01-11 18:30:00 +0900
---

It didn't take me a long time to decide that I liked 
[Jekyll](https://jekyllrb.com/). A blog run almost 
entirely on plaintext files? No weird SQL databases to worry about? Man, that
sounds sweet! Where do I sign up?

All the better when I learned that GitHub uses Jekyll to create its websites
([GitHub Pages](https://pages.github.com))---a site for which I already held 
an account. Sounds like a great opportunity to build a new blog and learn some 
new things!

Looking at all the documentation for GitHub Pages and Jekyll made it seem like
I'd be up and running in about 8 seconds. There were a number of prefabricated
Jekyll themes set up in GitHub repos just waiting to be forked; all I had to do 
is pick one I liked!

Of course, I didn't want to publish a website without adding some content and 
personalizing it a bit, 
so once I picked one ([Hyde](https://github.com/poole/hyde)), I took some time 
to dig into the Jekyll docs and configs to 
see where I could customize and tweak stuff. I had read that I could actually 
run the site offline using `jekyll serve`, so I thought "maybe I'll get 
everything set up locally first, write a few posts, then push it live later!"

Sometimes, running an OS like [Arch Linux](https://archlinux.org) is a real 
blessing. Other times, you'll be 
wringing your hands, contemplating how much force it would take to break your 
computer monitor. Still others, you sit back in your chair, eyes glazed over 
from reading the Arch Wiki and countless guides on the web---your brain has 
decided to short-circuit. It's not uncommon for me to experience a mixture of 
these emotions when learning something new, and getting Jekyll to work with 
Hyde (not trying to be funny) on Arch certainly fell into the "a bit of 
everything" category.

Getting Jekyll installed on Arch was pretty simple, as outlined in the Arch 
Wiki. Just install the `ruby` package, then run `gem install jekyll`. Finally, 
edit the `$PATH` variable to include any gems you install with ruby:

~~~
PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"
~~~
{: .language-bash}

After that, you can run the `jekyll` command without using the full path 
(something like `$HOME/.gem/ruby/$version/bin/jekyll`). Very simple.

I wasn't exactly sure what to do after that, though. Where do I put the files 
for the website? How do I run the `jekyll serve` command? Turns out this is 
quite simple as well. Grab the site files from your favorite theme, dump them 
in a folder, then run `jekyll serve` _in that folder_. Nice! Let's grab that 
Hyde theme and get that server running!

~~~
mkdir .site && cd .site
git clone https://github.com/poole/hyde.git
cd hyde
jekyll serve
~~~
{: .language-bash}

This is where I encountered some problems.

<p class="error">
Deprecation: You appear to have pagination turned on, but you haven't included 
the `jekyll-paginate` gem. Ensure you have `gems: [jekyll-paginate]` 
in your configuration file.
</p>

and

<p class="error">
Since v3.0, permalinks for pages in subfolders must be relative to the site 
source directory, not the parent directory. Check 
http://jekyllrb.com/docs/upgrading/ for more info.
</p>

It appears the Hyde theme I wanted to use wasn't working. After a little 
research, I discovered the theme was outdated enough that a new version of 
Jekyll came out and deprecated a few of the config options that it tries to 
set. And of course Arch always has the latest and greatest of everything. 

>Note: Had I simply gone to the [website](https://jekyllrb.com/docs/upgrading/)
>the second error linked to, I would have found the answers I was looking for.

I went digging into the GitHub Issues and Pull Requests for the Hyde 
repository thinking I'd find an answer there. I eventually did find the fixes I 
needed, which were quite trivial.

The first error is because Jekyll v3 no longer handles paginating pages (the 
older/newer links you see that the bottom of this page) on its own. That 
functionality got split out into its own plugin, which needed to be downloaded 
and installed, then activated in the config file.

~~~
gem install jekyll-paginate
~~~

then, in `_config.yml`:

~~~
gems: [jekyll-paginate]
~~~

The second error is because relative permalink support was removed entirely. 
So, we can just get rid of this line while we're at it:

~~~
relative permalinks: true
~~~

Okay! Let's get this sucker up and running! `jekyll-serve`:

<p class="error">
Liquid Exception: Liquid syntax error: Unknown tag 'gist' in [$HOME]/...<br />
Liquid Exception: Liquid syntax error: Unknown tag 'gist' in _layouts/...
</p>

Hmm. What's gist? Maybe I can do `gem install gist`?

~~~
Fetching: gist-4.5.0.gem (100%)
Successfully installed gist-4.5.0
Parsing documentation for gist-4.5.0
Installing ri documentation for gist-4.5.0
Done installing documentation for gist after 0 seconds
1 gem installed
~~~

Sweet! Gotta add that to the gems:

~~~
gems: [jekyll-paginate, gist]
~~~

Surely it'll work now! `jekyll serve`:

<p class="error">
Liquid Exception: Liquid syntax error: Unknown tag 'gist' in [$HOME]/...
Liquid Exception: Liquid syntax error: Unknown tag 'gist' in _layouts/...
</p>

Hrm. After some more searching, it turns out the syntax for that gem isn't just 
`gist`, it's `jekyll-gist`. So back to `_config.yml`:

~~~
gems: [jekyll-paginate, jekyll-gist]
~~~

`jekyll-serve`...

~~~
Server running... press ctrl-c to stop.
~~~

Gangbusters! It's working! All in all, it only took about 10 hours of my time. 
Of course, if I had just looked [here](https://jekyllrb.com/docs/upgrading/2-to-
3/ "Upgrading from Jekyll 2.x to 3.x"), I would have found the solutions a LOT 
faster (and you wouldn't have had to read my blog post).
