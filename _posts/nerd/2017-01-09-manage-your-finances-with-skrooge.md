---
layout: post
title: git commit -m manage your finances with skrooge
subtitle: "Managing money is a difficult task, but it doesn't have to be."
image: "assets/nerd/units.png"
category: nerd
date: 2017-01-09 00:05:00 +0900
---

Managing money is a difficult task for most, but it doesn't have to be. 
[Skrooge][skg] is a powerful application that, when set up correctly, can help 
you make smart financial decisions based on real data.

## Installation
Most distributions will have Skrooge in their default set of repositories, but
if you run Ubuntu or any distribution based on it, I recommend using
[Skrooge's PPA][ppa] to get the latest version. Run the following to get the
latest version from the PPA:

    sudo add-apt-repository ppa:s-mankowski/ppa-kf5
    sudo apt-get update
    sudo apt-get install skrooge-kf5

The first command installs the archive in your list of repositories, the
second pulls down the latest information for packages, and the third will
install Skrooge on your system.

## Setting up Skrooge
After firing up the application, you might be tempted to jump right in and 
start logging receipts. Some consideration before doing this, however, will 
ensure Skrooge will work for you and not against you!

### Configure your currencies
Depending on your installation, you may not have to worry about this. Skrooge 
is usually smart enough to pick the right currency for you if your operating 
system is configured correctly. If it doesn't, it's not a problem! The Units tab 
allows you to add currencies to your file so that you can use them in your 
transactions. Just find the currency you want in the drop down menu at the 
bottom and click Add.

![Units](/assets/nerd/units.png)

If you're using two currencies, you can configure them so that one is the 
primary currency and the other is the secondary currency. The primary currency 
will be your default currency when entering transactions and will be used for 
graphs and reports, and the secondary currency is visible if you hover over 
amounts in the Operations tab or over charts in the Reports tab. To set up 
primary and secondary currencies, select the currency you want to change and 
click the "Manual" button at the bottom.

### Create accounts for your bank accounts
Go to the Accounts tab and create an account for each bank account you have. You 
can safely ignore the drop down with all the bank names, as it doesn't do much 
except give you a tiny icon, and possibly fill out some of the bank's 
information. The important fields to select or fill in here are:

* Bank Name
* Account Name
* Initial Balance
* Currency Type
* Account Type

Consult your financial records if you need help filling out this information.

These accounts will essentially match the transaction history your bank has 
provided you, whether through bank statements or a bank book. For tracking your 
cash spending we're going to create a separate account.

### Create a wallet account
In the Accounts tab, select Wallet from the Account Type drop down. This is a 
special type of account that is not affiliated with a bank, as it is supposed to 
reflect what's going on in your actual wallet. This is the account where you 
will track the majority of your spending (assuming most of your transactions are 
done with cash). If you spend in cash in multiple currencies, you can create 
multiple wallets that have different currencies. Very handy for international 
traveling!

I recommend naming your wallet based on the country, so if you live in Japan, 
you'd have a Japanese Wallet. If you live in America, you'd have an American 
Wallet.


### Consider your categories
By default, Skrooge gives you a number of spending categories to choose from. 
Ultimately, it is up to you how you want to organize these. You can keep things 
at 30,000 feet and just have one tier of categories, or you can create 
subsubcategories of your subsubcategories to track every last minute detail of 
your spending.

![Categories](/assets/nerd/categories.png)

I've found that 2 tiers of categories (CATEGORY > SUBCATEGORY) provide 
sufficiently detailed information for most people. In the picture above, you can 
see my categories, which are based on how I set my [budgets](#budget) every 
month. If you follow this system, I recommend 3 tiers.

Don't worry if you don't know where to start! Categories can be flexible. If I 
ever feel the need to track something in particular, such as the money I spend 
on beer, I can set up a category and use it for a while. After 
a month or two, I get a pretty clear picture of those spending habits, then 
carry on using my usual set of categories. This is extremely useful if you're 
looking for areas of your spending where you can save money!

If you still need help setting up categories, most money help bloggers recommend 
at least the following:

* Bills
* Car (if you have one)
* Food
* Healthcare
* Household
* Leisure
* Travel
* Utilities
* Wages (use this for your income)

The above categories should be enough to get you started. As you spend money, 
you'll likely discover other categories and subcategories you need to add. All 
part of the process!

## Transactions
Now that you have Skrooge all set up, you're ready to start entering receipts! 
Skrooge calls transactions "operations", so use the Operations tab for this.

### Bank account transactions
![Transaction](/assets/nerd/transaction.png)
As I mentioned when setting up accounts, the transactions for your bank accounts 
in Skrooge should simply match the transaction history your bank has provided 
you. Enter in each transaction and the appropriate information associated with 
it. Make sure your amounts for deposits are positive and withdrawals are 
negative!

> #### Note for transactions
> If you have set up Skrooge to use multiple currencies, make sure you use the 
> correct currency type in your transactions! Since each account has a currency 
> type attached to it, Skrooge will be confused if you enter a transaction into 
> an account with a conflicting currency.

### Wallet account transactions
Somewhere along the line, you've probably withdrawn cash from your bank account 
for spending in shops and grocery stores. How do you track this spending if it 
isn't recorded in your bank book? This is where your Wallet account comes into 
play. The only problem is your wallet technically has no income, as opposed to 
your bank accounts, which likely receive direct deposits from your employer. How 
can we reconcile an account that only has expenditures and no income? The way to 
do it is with Transfers.

#### Creating "income" for your wallet
Basically, anytime you withdrawal cash from your bank account, you're 
"transferring" money from your bank account to your wallet account. Skrooge 
allows you to transfer money between your various accounts in the Operations tab 
by selecting the Transfer type of transaction at the bottom of the screen. Click 
the Transfer button and another set of boxes will show up, allowing you to set 
up the transfer.

![Transfer](/assets/nerd/transfer.png)
For a cash withdrawal from your bank account, enter the following information:

* Account: [the bank account you're taking money out of]
* To Account: [the wallet you're using]

Enter the other information as you would normally (you can see what I normally 
use for payee and category above). When you add the operation, Skrooge will 
automatically create two operations; one that adds the amount to your wallet and 
another that deducts the amount from your bank account. Just like in real life!

Now that your wallet has money in it, you can track your cash spending using the 
wallet account. Enter any and all cash transactions originating from your wallet 
here; receipts are your best friend when tracking cash spending, so be sure to 
keep them until you've had a chance to enter them into Skrooge!

> #### Note for traveling abroad
> You can use this same method of Transfers if you travel abroad and exchange 
> some money into a foreign currency.
> 
> For example, if you travel to the Philippines, create a Filipino Wallet 
> account and set it to use Philippine Pesos (make sure you add the PHP in your 
> Units tab!), then transfer money into it. If you exchange cash at the airport, 
> the transfer would be from your normal wallet account to the new wallet 
> account. If you order money from your bank, the transfer would be from your 
> bank to the new wallet.
> 
> Skrooge will see that the two accounts use different units and ask you for an 
> amount in the foreign currency. Just enter the amount you received from the 
> foreign exchange bureau or your bank and Skrooge will put that amount in your 
> foreign wallet!

## Reconciliation
After you've entered in your transactions, you need to check the totals you have 
in Skrooge with what you actually have. This process is called reconciliation. 
Skrooge handles reconciliation through a two-step process. First, you point the 
transactions in the account you want to reconcile in the Operations tab by 
clicking the check boxes in the check column. Then, you enter your actual total 
for that account into the Final Balance box. If your actual total matches the 
total in Skrooge, a big check mark will show up that you can click to reconcile 
the account.
![Reconcile](/assets/nerd/reconcile.png)

The easiest way to start the process of reconciliation is to go into the 
Accounts tab, right-click the account your want to reconcile, and select 
Reconcile (or just type ALT+R).

### Pointing transactions for reconciliation
If you are using Skrooge's default settings, it will automatically point your 
transactions so you don't need to point them yourself; you should see a 
partially checked box or a check box that looks filled in, but without a check 
mark. If you aren't sure, just try clicking the boxes to see the different 
states.

### Reconciling your bank accounts
For bank accounts, you should have records from your bank that you can check 
against the records you have entered into Skrooge. Find the current balance of 
your account and type it into the Final Balance box.

### Reconciling your wallet
Reconciling your wallet account is typically harder than your bank accounts 
since your bank usually keeps very good records of what's going in and out, 
whereas your wallet account depends entirely on the paper trail you create for 
it. In any case, reconciling your wallet account is very similar to reconciling 
your bank account. After you've entered in your transactions, make sure they are 
pointed. Then count the money in your wallet and enter the number into the Final 
Balance box. If you were vigilant in your tracking, the numbers will match and 
your wallet will be reconciled. If not, think back through the time you're 
trying to reconcile and try to remember any places where you may have spent 
money, but you forgot, lost, or didn't receive a receipt.

If you still have trouble, you have options. If it's a small amount, you could 
push it into one of your pointed transactions (or split it into a bunch of them) 
and it won't affect your spending summary too much. If you're a stickler for 
accuracy, or the amount is rather large and you don't want to skew your spending 
data, you can create a fake transaction for that exact amount and assign it a 
category like “Unknown” or “Missing.” That way there's at least a record of that 
money so you can reconcile it. Also, if you remember where you spent it later, 
you can go back and replace the fake transaction.

#### Tips for reconciliation
__Reconcile often.__ The best way to keep accurate data is to enter in your 
transactions as frequently as possible. You should be reconciling at least every 
other day, if not every day. The more often your reconcile, the less you'll have 
to think back and retrace your spending history (especially for your wallet).

__Keep your receipts.__ This one's pretty obvious, but many people neglect this 
practice. Always keep receipts from your cash spending; ask for one if your 
payee doesn't offer one to you. Once you've entered the data into Skrooge, you 
can safely throw away most receipts, though it may be smart to keep certain ones 
around for a while, e.g. big purchases (TV, refrigerator), pay stubs, monthly 
bills (gas, electricity), etc. Receipts for big purchases are useful for 
warranties and returns, pay stubs can be helpful for taxes, and monthly bills 
can prove your residence in certain situations (job hunting, taxes, applying for 
a visa, etc.).

__Always carry a pen.__ If you're serious about tracking your finances 
(specifically your cash spending), a pen is your best friend. In the event you 
spend money where no receipt is available, like a vending machine or a small 
shop that doesn't offer receipts, you can write down the details you need right 
on the spot and save your brain the headache later. You can write on the back of 
another receipt, you can bring a small sheet of paper with you, or you can just 
write it on your arm. Don't skimp on details either! Be sure to write the date, 
payee, and category in addition to the amount. This protects you in case of 
extenuating circumstances, like a computer crash, or against your own laziness.

__Import your bank transactions.__ If you have access to online banking and your 
bank allows you to download your transaction history, try importing it into 
Skrooge. It handles a number of different file formats, including QFX and plain 
CSV, among others. This is especially helpful if your transaction history is 
very long. You might be able to save yourself hours (or days) of manual input!

## Creating a budget
This may be what you wanted all along. You wanted to reign in your spending 
somehow, but that's hard to do when you don't know where your money is going! 
It's possible to do it blindly---take out some percentage of your monthly income 
for savings, then divide the rest by 30 or 31 and you've got a daily budget. Or 
divide by 4 and you've got a weekly budget. Unfortunately, we hardly spend money 
in such a linear fashion. Sometimes we need to buy plane tickets or replace 
furniture. Daily and weekly budgets can't account for big expenditures like 
these and could lead you into financial trouble in the long run if you aren't 
careful.

### Skrooge's budget system
In case you haven't noticed, Skrooge has a Budget tab. It is quite powerful, as 
you can create a variety of different budgets with it. Personally, I choose to 
create customized budgets manually every month, since it allows me to adjust 
amounts as I plan around big expenditures and it forces me to consider each 
month separately. However, Skrooge offers the ability to create budgets over the 
period of a year, or monthly budgets that simply repeat the same amount every 
month. It also allows you to set rules on budgets to carry over surpluses or 
deficits to subsequent months. If you've been using Skrooge for a while, it can 
even create budgets for you automatically based on your spending habits. I 
haven't used these features because, as I mentioned, I prefer to budget my money 
myself.

### <a name="budget"></a>Making your own budget
![Budget](/assets/nerd/budget.png)
Figure out your after-tax income (net). The first step is pretty simple. How 
much money gets deposited into your bank account every month? This is the number 
you'll have to work with when creating your budget.

If you have no idea where to get started though, try looking at your spending 
through the lens of the popular 50/30/20 plan:

__50% for fixed costs.__ Chances are you have some monthly payments that don't 
flucuate very much. These are things like rent for your apartment, utilities, 
etc. This also includes things like cell phone bills and yoga studio 
memberships, since these are things you're committed to paying every month. 
These costs should generally be no more than 50% of your after-tax income.

__30% for flexible spending.__ This is stuff that can change month to month, 
such as dining out, groceries, shopping, etc. You would think food stuff like 
dining out and groceries would be in the fixed costs budget, but since you can 
change how much you spend on either, they tend to vary. These expenses should be 
no more than 30% of your after-tax income.

__20% for financial goals.__ This budget is mostly about saving money or 
repaying any debts you might have, and it's arguably the most important. If 
you're wondering why you need to save any money (if you have no debts), there's 
plenty of reasons it's a good idea: creating an emergency fund; saving for 
retirement; saving for your future family's education costs; saving for a future 
vacation; or saving for a down payment on a home. The reason for your savings 
don't have to be 100% crystal clear, but just keeping in mind these things when 
you're saving is a good motivator to remember why you're doing it.

> #### Note for giving
> If you tithe or give money regularly to a religious institution or charity, 
> you might be wondering where that percentage should go. It all depends on your 
> own personal budget. Are your fixed costs already near or over 50%? Make 
> giving a financial goal. Have room in your fixed costs? Put it there. If 
> you're somewhere in the middle or you're just starting out, you can even split 
> it across both; put half in your fixed costs and commit to paying that and put 
> the other half in your financial goals.

If you've been having trouble in some area of your finances (saving, right?), 
hopefully this first look will give you an idea of where the problem lies. Maybe 
your fixed costs are too high, or you're spending too much on shopping, and as a 
result your savings are taking a hit. Seeing your monthly spending in this way 
will help you know where to make the most effective adjustments so you can get 
back on track.

## Other areas of Skrooge to explore
This article is just to give you an overview of how to track your spending and 
set up a budget with Skrooge. If you're willing to explore, there are even more 
features that are quite useful, including the Report tab, which you can set up 
to get a graphical look at your finances, and the Monthly Report tab, which 
gives you a rundown on your monthly spending. Also be sure to check out 
Trackers, which allow you to group certain transactions together (think 
vacations or loans), as well as Scheduled Operations, which automatically enter 
transactions into your accounts at a given cadence.

## Conclusion
While managing your money can be a complicated, if not Herculean task, Skrooge 
has the power to tame your wallet. For me personally, the killer feature is its 
ability to handle multiple currencies. I have lived abroad ever since I 
graduated from college, so it was the one feature I needed that almost no other 
app I found could match. In addition, I see other features that I may grow into 
at some point, such as the ability to track the value of assets such as stocks, 
a house, and a car. I'm sure you'll find it to be just as good, if not better, 
than any other financial application out there if you give it a try.

[skg]: http://skrooge.org "Skrooge, a personal finance app for Linux"
[ppa]: https://launchpad.net/~s-mankowski "Skrooge PPA"
