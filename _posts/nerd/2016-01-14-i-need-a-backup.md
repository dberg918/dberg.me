---
layout: post
title: git commit -m i need (a) backup
category: nerd
date: 2016-01-14 14:30:00 +0900
---

As someone who's been successfully breaking and maiming computers for around 
two decades, it's surprising that I have been waltzing around without proper 
backups until last year. I knew the mantra: back up often, back up securely, 
back up remotely. I just figured it was more trouble than it was worth; 
I didn't have anything really important to back up, anyway.

Now that I have started keeping track of my finances (more on that later) and 
feel that a computer crash would actually hurt in terms of losing documents, I 
decided to take a deeper look at making backups a part of my daily computing 
habits. Thankfully, Linux has a number of very useful tools for this, and with 
cloud storage more available than ever, backing up remotely is just as simple.

### Backup tool
I run GNOME on my machines, so the first thing I looked at was 
[D&eacute;j&agrave; Dup][dup]. It's perfect for what I need: easy, automated, 
and encrypted. For my purposes, all I need to backup is my Documents folder. 
My pictures and videos are all on my mobile device at this point, and I have 
them backed up to Google Photos already. Music I generally stream from the 
internet these days. Everything else is pretty much expendable.

    // Arch install:
    # pacman -S deja-dup

All you need to do with D&eacute;j&agrave; Dup is give it a list of directories 
to backup, a place to put the backups, and a password if you want to encrypt 
them. Then, simply set it to backup at one of the given intervals (I back up 
every day) and tell it how long to keep them.

[dup]: https://launchpad.net/deja-dup
   "D&eacute;j&agrave; Dup on Launchpad"

### Going remote
Backing up to a local machine or file server is helpful for restoring files you 
delete accidentally or when your hard drive crashes, but it doesn't help much 
if your house/apartment burns down. Remote backups are important! It's also 
important to keep them secure, as you don't want anyone snooping on your 
personal information.

#### Remote storage
D&eacute;j&agrave; Dup supports some of the common remotefs protocols (SSH, 
FTP, WebDAV, etc.) in case you have your own private server set up. For those 
of us that don't, it also supports a few hosting services, but most of them are 
services you have to pay for, which I'm not so interested in doing considering 
the plethora of free options that are out there. You get 15GB from Google just 
for having a Google account. Dropbox offers 2GB for free. Since I'm just 
backing up a bunch of documents, space is not a huge concern (although I would 
have to upgrade a Dropbox account for a year's worth of backups).

Although I could have gone the Google route, I decided to sign up for MEGA's 
[cloud storage service][store]. Not only do you get 50GB for free, they also 
offer a desktop client for automatic syncing (much like Dropbox). This allows 
me to automate my backups to the cloud without the need for D&eacute;j&agrave; 
Dup to support the service directly.

[store]: https://mega.nz/#privacycompany
   "MEGA: The Privacy Company"

#### Security
As for security, D&eacute;j&agrave; Dup already takes care of that, as 
everything is encrypted via GnuPG right off the bat. This is plenty of security
for my personal needs.

### Putting it all together
I have D&eacute;j&agrave; Dup set to backup my files into a hidden directory 
in my home folder called `.doc-sync`. Backups go in there automatically every 
day.

Then I set up a `Document Backups` folder in my Mega account and point Mega's 
desktop sync client at that `.doc-sync` folder. Whenever D&eacute;j&agrave; Dup 
adds something to that folder, it automatically gets uploaded to my Mega 
account. Voila! Automagic remote backups!

### Problems with my setup
Though I am quite happy to have nearly a year's worth of backups up to this 
point, there's still a few things I'm dissatisfied with.

#### Password is set in stone
First is that I can't change my password on the backups once I set it. The 
sanctioned workaround for this is to simply move your backup directory or 
rename it, then start afresh<sup>\[[bug][pass]\]</sup>. I was honestly confused 
by this because I thought D&eacute;j&agrave; Dup used GnuPG for "encryption," and
since I can change the password on a GnuPG key without changing the key itself,
surely this is possible. This turned out to be a fundamental misunderstanding 
of how it is being used in D&eacute;j&agrave; Dup.

D&eacute;j&agrave; Dup uses GnuPG through `duplicity` with a
"[symmetric cipher][sym]" for encryption<sup>\[[link][encrypt]\]</sup>, which 
is different from the asymmetric public-private keys you generate with the 
`gpg` command. The confusion was understandable, since both require
passphrases. I just assumed that D&eacute;j&agrave; Dup was hiding its use of a 
GnuPG master key or subkey, when in fact it's just using the password you
give it.

[pass]: https://bugs.launchpad.net/deja-dup/+bug/957857
   "No way to change password for a backup set"
[encrypt]: https://wiki.gnome.org/Apps/DejaDup/HowItWorks#Encryption
   "D&eacute;j&agrave; Dup: How It Works - Encryption"
[sym]: https://www.gnupg.org/gph/en/manual.html#AEN185
   "The GNU Privacy Handbook: Symmetric ciphers"

#### Can't use my GnuPG keys to encrypt/decrypt my backups
Piggy-backing on the previous thought, it'd be nice if I could use a GnuPG key
to encrypt my backups<sup>\[[bug][key]\]</sup>. Such a setup would fix the 
issue above; the key can stay the same, but I can change the password on the 
key to improve security. Also, as pointed out in the bug report, it would allow 
for automatic backups without the need to save the password on your machine or, 
if you decide not to save it, enter it every time you want to backup.

[key]: https://bugs.launchpad.net/deja-dup/+bug/415527
   "Provide a way to use public key encryption"

#### Solution?
Looking through the docs (or you may have noticed in the previously linked bug 
report), `duplicity` actually supports asymmetric GnuPG keys
<sup>\[[man][dupen]\]</sup>, so moving to `duplicity` would actually solve 
those problems rather elegantly. I would end up losing the simple interface for 
scheduling, doing periodic full backups, and deleting old ones that 
D&eacute;j&agrave; Dup provides in the process though, but I might be able to 
get that back with `cron` (something I've never used before) or a `systemd` 
unit file (something I've never written on my own). Seems like another 
opportunity to learn something new!

Another boon to this solution is that `duplicity` also supports sending files 
directly to MEGA without the need for a sync client<sup>\[[man][mega]\]</sup>, 
although the developer of D&eacute;j&agrave; Dup mentioned it may not be fully 
reliable as of April last year<sup>\[[list][reli]\]</sup>. That said, there's a 
[bug report][backend] open for integrating more of the backends that 
`duplicity` supports into D&eacute;j&agrave; Dup itself.

Check back with me later. I might decide to go commando with my backups!

[dupen]: http://duplicity.nongnu.org/duplicity.1.html#sect5
   "duplicity(1) manual page: Options"
[mega]: http://duplicity.nongnu.org/duplicity.1.html#sect7
   "duplicity(1) manual page, URL Format"
[reli]: https://mail.gnome.org/archives/deja-dup-list/2015-April/msg00009.html
   "D&eacute;j&agrave; Dup mailing list: How to develop back-ends"
[backend]: https://bugs.launchpad.net/deja-dup/+bug/1254659
   "Add other duplicity backends support"

