---
layout: post
title: git commit -m a few weeks with loki
category: nerd
date: 2016-10-03 11:00:00 +0900
---

I've been an Arch user for almost as long as I've been a Linux user. I started 
on Ubuntu, moved to Linux Mint, then finally landed on Arch and stayed there 
for a pretty long time. I was constantly frustrated that the Ubuntu repos 
didn't have the latest of *this* package, or *that* new app wasn't available at 
all. PPAs were a mess to deal with, and compiling from source was often a hunt 
for the magic `dev` package I was missing. After moving to Arch, all those 
complaints went away. Sure, I broke my system horribly a number of times and 
spent countless hours scouring the internet for answers, but that was all part 
of the fun!

Recently, since I started developing websites and [producing a podcast][1], I 
have come to appreciate a system that doesn't update major versions constantly 
and will work reliably when I need it to. I was also interested in dipping my 
toes back into Linux package systems that I might encounter in a production 
environment, should I ever finally land that elusive IT job. So, about 3 months 
ago, I decided to scrap Arch for a while and settle on two systems that were a 
little more stable. 

I moved to Fedora on my desktop rather quickly. It was a fairly straightforward 
process and it has worked wonderfully, even after an upgrade from 23 to 24. On 
my laptop, however, I decided to wait a while. I really wanted to give the 
latest release of [elementary OS][6] a run on a machine I depend on to see if it 
really was more than just beautiful design.

I've been using it for about 3 weeks at this point. It's been fine for the most 
part, but I have to say that I've encountered a number of bugs that, given 
elementary's reputation, left me a little disappointed.

#### [Asian Input Methods][im]
I live in Japan, so being able to type Japanese is important. I used GNOME with 
Arch, which had the ibus input method baked right into it. It looked beautiful, 
and it worked beautifully. On elementary Loki, however, no amount of tweaking 
and digging could get ibus to work, even though the package is *installed by 
default*. In fact, using ibus caused a lot of other things to *stop* working, 
like searching in Slingshot and, on occasion, keyboard shortcuts. There is an 
open bug for this issue, but it was not addressed in Loki. Let's hope Juno gains 
better support for input methods.

#### [Calendar issues][cl]
Loki comes with Maya by default, which seems like a nice first-party app. 
That is until you plug in a Google calendar. Not only do events show up 
incorrectly in the sidebar, there's also a display bug in the date & time 
indicator on the top bar where events leak into other days. Granted, I really 
haven't used a desktop calendar app before, but this seems like basic 
functionality for an app dedicated to keeping track of appointments.

#### [Screenshot destroys data][ss]
In the midst of reporting bugs for Maya and other parts of the operating system, 
I discovered a bug in Screenshot where, if you use an existing filename in the 
directory where you save the screenshot, the app will simply overwrite the file 
without a prompt or warning. Bye-bye data. Yikes.

#### [File manager crashes on trash restore][ps]
Another bug I discovered in the midst of reporting bugs and taking screenshots 
was that elementary's file manager would crash if you tried to restore a file 
from the system trash. Not just a once-in-a-while seg fault, but every single 
attempt I tried. I'd consider this a showstopper.

I can only assume that the elementary developer team is simply not big enough, 
or paid enough. It's hard already to put together a Linux distribution 
without a good team of full-time developers. The fact that they're doing the 
things they do with only 2 or so full-time staff is nothing short of amazing.

I don't want to be a complete Debby Downer here, because there are definitely a 
couple of positive things I feel compelled to mention. Here they are, in no 
particular order.

#### Scratch as a development environment
My needs in terms of a development environment are very simple as they are, but 
I was still a little surprised to find how well it works for me. I used Builder 
quite a bit when I was on Arch with GNOME, which had a nice sidebar that showed 
all the files in my Project directory, as well as a terminal at the bottom for 
running `jekyll serve` and `php -S` commands when doing my web dev stuff. Little 
did I know that Scratch can do these things, too. It also mimics Builder's 
split-screen ability (though Builder might have the nicer UI for that). The one 
feature I really appreciate though, and some may disagree, is that it 
auto-saves like I'm typing an email in Gmail or Google Docs, so I never have to 
hit Ctrl+S. Nice! 

#### Mail and Online Accounts
In GNOME, entering your Google credentials into the Online Accounts section of 
System Settings only allows you to use Evolution to manage your email. This is a 
real bummer, because Evolution never worked for me as a mail app. It was just 
too cluttered and tried to do too much. In elementary, they have forked Yorba's 
great Geary mail app into Pantheon Mail and hooked it into Online Accounts. It 
was a little finicky---Pantheon Mail gave me the first-run dialog the first few 
times I started it after entering my credentials into Online Accounts---but it 
does work! Being able to see all of my email accounts in one desktop app is 
pretty sweet.

#### Notifications
I think the elementary team really knocked it out of the park with notifications 
on the desktop. The controls in System Settings are gorgeous and very easy to 
understand, the indicator in the top bar is well-designed, and having the 
ability to turn them off with a single switch in the indicator is great. The 
notifications themselves are very pretty, although I question some of the 
default settings...playing sounds for music track changes doesn't make much 
sense to me.

#### Out with Midori, Hello Epiphany
While it may not be a Google Chrome or Mozilla Firefox, elementary's version of 
Epiphany works nicely for normal use. Unfortunately, it doesn't support 
extensions like LastPass, which I use for managing my passwords. However, I 
find that the [LastPass command line utility][2] works quite well and eliminates 
my annoying dependency on something strictly in-browser. The only thing I really 
need Chrome/Firefox for at this point is the web dev tools. Epiphany has nothing 
for tweaking stuff in-browser\*.

\*Actually, this is not true. Epiphany does have WebKit's Web Inspector 
built in, but there is no working keyboard shortcut to expose it. The only way 
to open it is to right-click on a webpage and select `Inspect Element`. In any 
case, it doesn't seem to work as well as the dev tools on Chrome or Firefox.

## [Loki Release Event][re]
One last thing I'll comment on is elementary's release event for Loki, which was 
made possible by [Jupiter Broadcasting][5]. I only comment because I thought the 
event was [a great idea][3]. Whether or not I was actually the detached 
mastermind of it, I'm not really sure. In any case, I think they did a nice job 
of showing off a few new parts of the desktop, but I had something different in 
mind. Perhaps if the guys end up reading this, it can be food for thought for 
future events.

One thing the elementary team prides itself in is moving users from 
closed-source operating systems like Mac and Windows. However, a lot of time was 
spent talking about Linux plumbing that wouldn't make any sense to a Windows or 
Mac user. Granted, the audience of Jupiter Broadcasting is not exactly an MS or 
Apple-heavy crowd, but I think a release event geared towards your mission as a 
project is the most effective way to advertise. Perhaps showing elementary with 
a target OS side by side, and how elementary does certain things better might 
make more sense to an end user. Another idea is to demonstrate new features 
live! Set up an email account in System Settings to show how easy it is; create 
some parental controls so that users can see how they work; pair a phone over 
Bluetooth and move some pictures into the Photos app; load up a few video files, 
turn on Sharing in System Settings and serve media to a TV. I think there are a 
lot of creative ways to show users how your software works, and invite them to 
give it a try themselves.

Overall, I see Loki as a great iteration on Freya. I don't have any plans to go 
back to Arch, so I'm looking forward to seeing Loki improve bit by bit. I'm also 
excited to see what happens with their developer platform for GitHub, 
[Houston][4]. If it really lowers the bar for getting software to their 
AppCenter, it could be a very interesting future for elementary!


[1]: http://loveydummies.com
[2]: https://github.com/lastpass/lastpass-cli
[3]: https://twitter.com/dberg918/status/769025971534069762
[4]: https://github.com/elementary/houston
[5]: http://jupiterbroadcasting.com
[6]: https://elementary.io
[im]: https://bugs.launchpad.net/switchboard-plug-keyboard/+bug/1397776
[ss]: https://bugs.launchpad.net/screenshot-tool/+bug/1624356
[ps]: https://bugs.launchpad.net/pantheon-files/+bug/1622053
[cl]: https://bugs.launchpad.net/maya/+bug/1443056
[re]: https://www.youtube.com/watch?v=MN8dVJ6_AL8

