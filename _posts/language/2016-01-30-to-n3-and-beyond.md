---
layout: post
title: to n3 and beyond
category: language
date: 2016-01-30 18:12:00 +0900
---

I am a certified [JLPT][] N4-level Japanese speaker. If you know what that means, 
you understand how very unimpressive that statement is. At the very least, it's 
a measureable achievement in my quest to learn Japanese, which has often been 
an exercise in frustration.

Japanese study time has largely eluded me since I started living in Japan about 
2 and half years ago, aside from a weekly Japanese class I attend on Thursday 
nights. In addition, many of my attempts to have a conversation with native 
speakers prove unfruitful. Part of the problem is that I still can't keep up 
with a conversation at a native pace (which isn't expected of an N4-level 
speaker). If that isn't enough, the grammar is a complete departure from 
English. For a native English speaker, there are a lot of initial mental 
gymnastics you must learn to really be fluent. It's quite a unique challenge, 
and I'm honestly surprised I passed N4 given how little time I've put into 
learning.

All that is to say that I've gathered a nice list of excuses, detailing the 
reasons why I'm not fluent in Japanese despite living in-country for over 2 
years. Those who have never lived overseas might balk at such a statement, but 
learning a language really isn't as hard as it seems, as long as you put in the 
time to practice, don't get discouraged from making mistakes, and stave off 
the developed instinct to pretend you understand something when you really 
don't.

Now that my first test is over and done with, I'm looking to keep pushing 
forward. Unfortunately, I feel like I've lost the momentum I had before I took 
the test back in December. I was jazzed enough to buy books for my next test, 
but I have yet to pick them up to study them. The reason I haven't moved on is 
I didn't feel very confident in my N4 grammar skills (or N5 for that 
matter), so I wanted to review those before I start in on N3. The real 
underlying issue is that I'm not getting enough practice in the language. In my 
default mindset, if I can fall back to English, I won't challenge myself with 
Japanese. The only time I've really forced myself into Japanese is during class 
once a week. Otherwise, I'm mostly using English[^eng]. All this review without 
any practical use is causing me to lose interest in studying. I need to get out 
of my comfort zone!

So, with the advent of February looming this weekend, I plan to begin studying 
in earnest. The ground rules are:

1.  **Stick my nose in a book every day.**
    
    It's important to establish a routine. Even though some days can be quite 
    busy, it shouldn't be impossible to set aside about 30 minutes a day to 
    take in a bit of vocabulary or grammar.
    
2.  **Practice new skills in a conversation for at least 10 minutes.**

    Given that I'll be learning something new every day, this rule means I'll 
    be practicing every day. The most important part of learning a language is 
    practicing and making mistakes.
    
The first JLPT test of 2016 will be held on July 3rd. With these rules, I feel 
confident that I'll be able to make measurable gains in my Japanese ability 
over the course of the next 5 months. I may write here every once in a while 
about my progress.

これから、頑張ります!

[^eng]: Granted, I am an English teacher, so speaking English is actually 
        important for me at work. That said, I'm surrounded by Japanese 
        speakers every day, so it shouldn't be that hard to make attempts 
        to practice.

[JLPT]: http://jlpt.jp/e/
   "Japanese-Language Proficiency Test"
