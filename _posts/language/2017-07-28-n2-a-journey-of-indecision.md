---
layout: post
title: "n2? a journey of indecision"
category: language
date: 2017-07-28 15:28:00 +0900
---

Despite my inability to form any discernable study habit last year, I managed to 
pass N3 in July. Surprising, considering I walked out at the end thinking I had 
utterly bombed the listening section. Thankfully, the JLPT gods that be were 
merciful, and I escaped with a shiny new certification.

Since then, a lot of life things have happened. I moved to Osaka, for one. And 
now that I'm getting older, I'm starting to think about building something that 
resembles a career. I've decided that my skills in tech are a thing I'd like to 
pursue further, so the last few months have been dedicated to job hunting in the 
IT field. Unfortunately, N3 just doesn't cut it for most Japanese companies, 
especially in the Osaka area (unless you're extremely lucky). The one interview 
I was invited to did not go well, and mostly because of my inability to express 
myself in good ol' 日本語. You'd think I'd become conversational after nearly 4 
years, but alas, English teachers in Japan rarely get to practice, as Japanese 
folks so eagerly want to speak English.

Thus, I press forward. Two opportunities to upgrade my certification have 
skated by since last July, mostly because I felt too intimidated by the gravity 
of N2. If I still barely understand what's going on around me day-to-day, how 
can I expect to even come close to passing that test? The indecision made me 
flinch, but no more. I will conquer this language, and I will conquer this test. 
How hard can it be? Learning like 2,000 kanji?

Speaking of kanji, this is where I am beginning my attack on N2. While the 
concept of drilling thousands of kanji into my brain sounds like a horrible way 
to learn Japanese, I've found it quite enjoyable so far. I must give a proper 
credit to [RTK][rtk] and more specifically [Nihongo Shark][ns], as they have 
convinced me that climbing the hardest hill first (learning kanji) will make the 
rest feel like a nice relaxing jog. The cool thing about the Heisig method is 
you learn kanji in an order that actually makes sense based on the primitive 
elements, and if you have a good understanding of stroke order, you instantly 
know how to write crazy-difficult kanji just by remembering what goes where. The 
weird thing is you learn kanji in a very wacky order indeed; I've 
already learned kanji that some of my Japanese friends didn't know, including 
kanji that aren't even on N1, but I still haven't studied characters like 人 
or 行 yet. Whatever, I'm up to about 740 kanji now, and I've only studied for 
a little over a month. By mid-October, I should reach 2200, including the entire 
[jouyou kanji][jk].

Once I get a little deeper into my kanji study, I plan to start incorporating a 
ton of Japanese reading into my study habits. I'm also listening to some 
podcasts to get my listening skills in shape. Grammar, while important, is 
generally the worst part of studying. Not because Japanese grammar is super-hard 
(it certainly can be), but it's boring, and nobody has really figured out a way 
to make it not boring. The jury is still out on what I'll do in that arena, but 
I am satisfied for the time being on tackling the most challenging areas of the 
test first.

２級、おまえを打ち克つ。

[rtk]: https://en.wikipedia.org/wiki/Remembering_the_Kanji_and_Remembering_the_Hanzi
[ns]: https://nihongoshark.com/
[jk]: https://en.wikipedia.org/wiki/J%C5%8Dy%C5%8D_kanji
